<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Banners</h3>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th>Imagem</th>
                <th>URL</th>
                <th>Tipo</th>
                <th>Ordem</th>
                <th>Remover</th>
            </tr>
            <?php foreach($this->viewItems['banners'] as $banner){?>
                <tr>
                    <td><img src="<?php echo(SITE_URL . 'Images/banners/' . $banner->image);?>" class="img-thumbnail" style="width: 280px"></td>
                    <td><?php echo($banner->url);?></td>
                    <td><?php
                        if($banner->type == 'main'){
                            echo('Principal');

                        }else if($banner->type == 'fixed-main'){
                            echo('Principal Fixo');

                        }else if($banner->type == 'product'){
                            echo('Produto (3 por linha)');
                        }

                        ?></td>
                    <td><?php echo($banner->order);?></td>
                    <td><a href="<?php echo(SITE_URL . 'mkt/banner/remove/' . $banner->id);?>">[remover]</a></td>
                </tr>
            <?php }?>
        </table>
    </div>

    <h3>Adicionar Banner</h3>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form rule="form" method="post" action="<?php echo(SITE_URL . 'mkt/banner/add');?>" enctype="multipart/form-data">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="image">Imagem</label>
                    <p>Utilize imagens com mesma proporção sempre!<br><br>
                        Recomendado:<br>
                        <?php echo(TXT_BANNER_MAIN_NAME);?> - <?php echo(TXT_BANNER_MAIN_SIZE_HELP);?><br>
                        <?php echo(TXT_BANNER_SECCONDARY_NAME);?> - <?php echo(TXT_BANNER_SECCONDARY_SIZE_HELP);?><br>
                        <?php echo(TXT_BANNER_PRODUCT_NAME);?> - <?php echo(TXT_BANNER_PRODUCT_SIZE_HELP);?><br>
                        <?php echo(TXT_BANNER_STRIPE_NAME);?> - <?php echo(TXT_BANNER_STRIPE_SIZE_HELP);?><br>
                    </p>
                    <input type="file" id="image" name="image" class="form-control">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="link">Link</label>
                    <input type="text" id="link" name="link" class="form-control">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="type">Tipo</label>
                    <select id="type" name="type" class="form-control">
                        <option value="main"><?php echo(TXT_BANNER_MAIN_NAME);?> - <?php echo(TXT_BANNER_MAIN_SIZE_HELP);?></option>
                        <option value="seccondary"><?php echo(TXT_BANNER_SECCONDARY_NAME);?> - <?php echo(TXT_BANNER_SECCONDARY_SIZE_HELP);?></option>
                        <option value="product"><?php echo(TXT_BANNER_PRODUCT_NAME);?> - <?php echo(TXT_BANNER_PRODUCT_SIZE_HELP);?></option>
                        <option value="stripe"><?php echo(TXT_BANNER_STRIPE_NAME);?> - <?php echo(TXT_BANNER_STRIPE_SIZE_HELP);?></option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="order">Ordem</label>
                    <input type="text" id="order" name="order" class="form-control">
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Adicionar" class="btn btn-danger">
                </div>
            </div>
        </form>
    </div>
</div>