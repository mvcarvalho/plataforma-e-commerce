<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Criar E-mail Marketing Padrão</h3>

    <?php if(isset($this->viewItems['emailhtml'])){?>
        <a href="<?php echo($this->viewItems['emailhtml']);?>" target="_blank" class="btn btn-success">Acessar HTML Gerado</a>

    <?php } else { ?>
        <form rule="form" method="post" action="<?php echo $this->viewItems['formaction'];?>">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid1">ID de produto 1</label>
                    <input type="text" id="productid1" name="productid1" class="form-control">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid2">ID de produto 2</label>
                    <input type="text" id="productid2" name="productid2" class="form-control">
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 30px"></div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid3">ID de produto 3</label>
                    <input type="text" id="productid3" name="productid3" class="form-control">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid4">ID de produto 4</label>
                    <input type="text" id="productid4" name="productid4" class="form-control">
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 30px"></div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid5">ID de produto 5</label>
                    <input type="text" id="productid5" name="productid5" class="form-control">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid6">ID de produto 6</label>
                    <input type="text" id="productid6" name="productid6" class="form-control">
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 30px"></div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid7">ID de produto 7</label>
                    <input type="text" id="productid7" name="productid7" class="form-control">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid8">ID de produto 8</label>
                    <input type="text" id="productid8" name="productid8" class="form-control">
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 30px"></div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid9">ID de produto 9</label>
                    <input type="text" id="productid9" name="productid9" class="form-control">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="productid10">ID de produto 10</label>
                    <input type="text" id="productid10" name="productid10" class="form-control">
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 30px"></div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Criar E-mail Marketing" class="btn btn-danger">
                </div>
            </div>
        </form>
    <?php }?>
</div>