<?php
$states = array("AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MG","MS","MT","PA","PB","PE","PI","PR","RJ","RN","RO","RR","RS","SC","SE","SP","TO");
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px">
    <div class="col-lg-3 col-md-2 col-sm-1 col-xs-12"></div>
    <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12 form">
        <div class="title">Meu cadastro:</div>
        <p style="font-size: 13px; color: #555555">As informações para cadastro no <?php echo(SITE_NAME)?> são simples e importantes. Nós não queremos que você fique digitando coisas desnecessárias.</p>
        <p style="font-size: 13px; color: #555555">Sendo assim, apenas as informações abaixo serão necessárias para poder adquirir nossos produtos.</p>
        <p style="font-size: 13px; color: #555555">Toda e qualquer informação aqui registrada será de uso exclusivo da <?php echo(SITE_NAME)?>. Suas informações estão seguras e protegidas com as mais altas tecnologias de segurança.</p>
        <form role="form" method="post">
            <div class="form-group">
                <label for="username">Nome Completo</label>
                <input class="form-control" type="text" name="username" id="username" placeholder="Nome Completo" required
                    <?php if(isset($this->pageItems['user']->name)){echo('value="' . $this->pageItems['user']->name . '"');}?>>
            </div>

            <div class="form-group">
                <label for="cpf">CPF / CNPJ</label>
                <input class="form-control" type="text" name="cpf" id="cpf" placeholder="999.999.999-99" required
                    <?php if(isset($this->pageItems['user']->cpf)){echo('value="' . $this->pageItems['user']->cpf . '"');}?>>
            </div>

            <div class="form-group" style="padding-top: 20px">
                <label for="address">Endereço</label>
                <p style="font-size: 13px; color: #555555">Este endereço será seu endereço de entrega. Verifique atentamente os dados inseridos para seu pedido ser entregue corretamente.</p>
                <p style="font-size: 13px; color: #555555">Caso tenha feito algum pedido e seu endereço de entrega esteja errado, entre em contato com nossa central o quanto antes.</p>
                <input class="form-control" type="text" name="address" id="address" placeholder="Rua. Primeira, 987, Ap. 12" required
                    <?php if(isset($this->pageItems['user']->address)){echo('value="' . $this->pageItems['user']->address . '"');}?>>
            </div>
            <div class="form-group" style="overflow: auto">
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-5" style="padding-left: 0">
                    <label for="city">Cidade</label>
                    <input class="form-control" type="text" name="city" id="city" placeholder="São Paulo" required
                        <?php if(isset($this->pageItems['user']->city)){echo('value="' . $this->pageItems['user']->city . '"');}?>>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding: 0">
                    <label for="state">Estado</label>
                    <select id="state" name="state" class="form-control" required>
                        <?php
                        foreach($states as $state){
                            if(isset($this->pageItems['user']->state)){
                                if($this->pageItems['user']->state == $state){
                                    echo('<option selected value="'.$state.'">'.$state.'</option>');
                                }else{
                                    echo('<option value="'.$state.'">'.$state.'</option>');
                                }
                            }else{
                                echo('<option value="'.$state.'">'.$state.'</option>');
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-4" style="padding-right: 0">
                    <label for="cep">CEP</label>
                    <input class="form-control" type="text" name="cep" id="cep" placeholder="00000-000" required
                        <?php if(isset($this->pageItems['user']->cep)){echo('value="' . $this->pageItems['user']->cep . '"');}?>>
                </div>
            </div>

            <div class="form-group" style="padding-top: 20px">
                <label for="phone">Telefone com DDD</label>
                <p style="font-size: 13px; color: #555555">Mantenha sempre seu telefone para contato atualizado. Ele será nosso principal meio de contato em caso de dúvidas urgentes.</p>
                <input class="form-control" type="text" name="phone" id="phone" placeholder="00 0000-0000" required
                    <?php if(isset($this->pageItems['user']->phone)){echo('value="' . $this->pageItems['user']->phone . '"');}?>>
            </div>

            <div class="form-group" style="padding-top: 20px">
                <label for="emailLogin">E-mail</label>
                <p style="font-size: 13px; color: #555555">Seu e-mail não pode ser alterado após o cadastro.</p>
                <input class="form-control" type="text" name="emailLogin" id="emailLogin" placeholder="seuemail@exemplo.com.br" disabled
                    <?php if(isset($this->pageItems['user']) && isset($this->pageItems['user']->email)){echo('value="' . $this->pageItems['user']->email . '"');}?>
                    <?php if(isset($this->pageItems['email'])){echo('value="' . $this->pageItems['email'] . '"');}?>>

                <input type="hidden" name="email"
                    <?php if(isset($this->pageItems['user']) && isset($this->pageItems['user']->email)){echo('value="' . $this->pageItems['user']->email . '"');}?>
                    <?php if(isset($this->pageItems['email'])){echo('value="' . $this->pageItems['email'] . '"');}?>>
            </div>

            <div class="form-group">
                <label for="password">Senha</label>
                <p style="font-size: 13px; color: #555555">Para alterar sua senha, apenas digite a senha desejada abaixo.</p>
                <input class="form-control" type="password" name="password" id="password" autocomplete="off" required
                    <?php if(isset($this->pageItems['user']->password)){echo('value="' . $this->pageItems['user']->password . '"');}?>>
            </div>
            <button type="submit" class="btn btn-danger" name="login">Salvar</button>
        </form>
    </div>
    <div class="col-lg-3 col-md-2 col-sm-1 col-xs-12"></div>
</div>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#cpf").mask("999.999.999-99");
        $("#cep").mask("99999-999");
    });
</script>