<!--CHECKOUT MERCADO PAGO-->
<div id="card-pay" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none">

    <div class="col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-10">
        <h2 style="padding-bottom: 30px">Dados de pagamento</h2>

        <form role="form" action="<?php echo(SITE_URL . 'cart' . DS . 'finalize' . DS . 'credit-card');?>" method="post" id="form-pagar-mp">
            <input id="amount" type="hidden" value="<?php echo($total);?>"/>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Número do Cartão</label>
                    <input class="form-control" data-checkout="cardNumber" type="text" id="cardNumber"/>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Código de Segurança</label>
                        <input class="form-control" data-checkout="securityCode" type="text" placeholder="123" id="securityCode"/>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Mês de Vencimento</label>
                        <input class="form-control" data-checkout="cardExpirationMonth" type="text" placeholder="12" id="cardExpirationMonth"/>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0">
                    <div class="form-group">
                        <label class="control-label">Ano de Vencimento</label>
                        <input class="form-control" data-checkout="cardExpirationYear" type="text" placeholder="2020" id="cardExpirationYear"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Nome impresso no cartão</label>
                    <input class="form-control" data-checkout="cardholderName" type="text" placeholder="JOAO SILVA SOUZA" id="cardholderName"/>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">CPF do titular (somente números)</label>
                    <input class="form-control" data-checkout="docNumber" type="text" placeholder="12345678910" id="cpf"/>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Parcelas</label>
                    <select class="form-control" id="installmentsOption" name="installmentsOption"></select>
                </div>
            </div>
            <input data-checkout="docType" type="hidden" value="CPF"/>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Concluir pagamento" class="btn buy">
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
</div>

<script type="text/javascript" src="https://secure.mlstatic.com/org-img/checkout/custom/1.0/checkout.js"></script>
<script type="text/javascript">
    Checkout.setPublishableKey("<?php echo(MP_PUBLIC_KEY);?>");

    $(document).ready(function(){
        $("#cardExpirationYear").mask("9999");
        $("#cardExpirationMonth").mask("99");
        $("#cpf").mask("99999999999");
    });

    function pagarComBoleto(){
        executePay(false);
    }

    function pagarComCartao(){
        var div = document.getElementById('card-pay');
        div.style.display = 'block';
    }

    $("#form-pagar-mp").submit(function( event ) {
        event.preventDefault();

        var re = new RegExp('_', 'g');

        var year = document.getElementById('cardExpirationYear').value.replace(re, "");
        var month = document.getElementById('cardExpirationMonth').value.replace(re, "");
        var cpf = document.getElementById('cpf').value.replace(re, "");
        var name = document.getElementById('cardholderName').value.replace(re, "");
        var card = document.getElementById('cardNumber').value.replace(re, "");
        var securityCode = document.getElementById('securityCode').value.replace(re, "");

        if(year < 2014){
            alert("Verifique o ano de validade digitado.");
            return false;

        }else if(month > 12 || month <= 0){
            alert("Verifique o mês de validade digitado.");
            return false;

        }else if(name.length < 4){
            alert("Verifique o nome digitado.");
            return false;

        }else if(card.length < 12){
            alert("Verifique o número do cartão digitado.");
            return false;

        }else if(securityCode.length < 3){
            alert("Verifique o código de segurança digitado.");
            return false;

        }else if(cpf.length < 11 || !isCPFValid(cpf)){
            alert("Verifique o CPF digitado.");
            return false;

        }else{
            var $form = $(this);
            Checkout.createToken($form, mpResponseHandler);
            return false;
        }
    });

    var mpResponseHandler = function(status, response) {
        var $form = $('#form-pagar-mp');

        if (response.error) {
            alert("Não conseguimos iniciar o pagamento.\nVerifique suas informações e tente novamente.");

        } else {
            var card_token_id = response.id;
            $form.append($('<input type="hidden" id="card_token_id" name="card_token_id"/>').val(card_token_id));
            //alert(response);
            //$form.get(0).submit();
            executePay(true);
        }
    };

    $("input[data-checkout='cardNumber']").bind("keyup",function(){
        var bin = $(this).val().replace(/ /g, '').replace(/-/g, '').replace(/\./g, '');
        if (bin.length >= 6){
            Checkout.getPaymentMethod(bin,setPaymentMethodInfo);
        }
    });

    function setPaymentMethodInfo(status, result){
        $.each(result, function(p, r){
            $.each(r.labels, function(pos, label){
                Checkout.getInstallments(r.id ,parseFloat($("#amount").val()), setInstallmentInfo);
                return;
            });
        });
    }

    function setInstallmentInfo(status, installments){
        var html_options = "";
        for(i=0; installments && i<installments.length; i++){
            html_options += "<option value='"+installments[i].installments+"'>"+installments[i].installments +" de "+installments[i].share_amount+" ("+installments[i].total_amount+") sem juros</option>";
        }
        $("#installmentsOption").html(html_options);
    }

    function executePay(isCard){
        var url = "";

        if(isCard == true){
            var select = document.getElementById('installmentsOption');
            var cardToken = document.getElementById('card_token_id').value;
            var installments = select.options[select.selectedIndex].value;
            url = "<?php echo(SITE_URL . 'APIs/mercadopago_execute_payment.php')?>?method=credit-card&cardTokenId=" + cardToken + "&installmentsOption=" + installments;

        }else{
            url = "<?php echo(SITE_URL . 'APIs/mercadopago_execute_payment.php')?>?method=ticket";
        }

        var result = httpGet(url + "");

        //var console = document.getElementById('console');
        //console.innerHTML = result;

        if(result == '1'){
            alert('Pagamento efetuado com sucesso.\nVocê será redirecionado ao seu histórico.');
            window.location = "<?php echo(SITE_URL . 'user/history');?>";

        }else if(result == '0'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, entre em contato com nosso atendimento.');

        }else if(result == '2'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, ligue para a administradora do seu cartão para autorizar o pagamento e tente novamente.');

        }else if(result == '3'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nSaldo insuficiente. Por favor, utilize outro cartão.');

        }else if(result == '4'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nCódigo de segurança inválido. Verifique e tente novamente.');

        }else if(result == '5'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nCartão com validade expirada. Por favor, verifique se a informação está correta ou utilize outro cartão.');

        }else if(result == '6'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, verifique suas informações e tente novamente.\nSe o problema persistir, entre em contato com nosso atendimento.');

        }else if(result == '7'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, verifique suas informações e tente novamente.\nSe o problema persistir, entre em contato com nosso atendimento.');

        }else{
            if(isCard){
                document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
                alert('Não conseguimos realizar o pagamento.\nPor favor, verifique suas informações e tente novamente.\nSe o problema persistir, entre em contato com nosso atendimento.');

            }else{
                var div = document.getElementById('pay');
                div.innerHTML = '<a target="_blank" onclick="redirectToHistory();" href="' + result + '"><button class="btn buy pull-right" style="margin-right: 10px">Imprimir Boleto</button></a>';
            }
        }
    }

    function redirectToHistory(){
        window.location = "<?php echo(SITE_URL . 'user/history');?>";
    }

    function isCPFValid(strCPF){
        var Soma;
        var Resto;

        if (strCPF == "00000000000"){
            return false;
        }

        Soma = 0;

        for (i = 1; i <= 10; i++){
            Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        }

        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11)){
            Resto = 0;
        }

        if (Resto != parseInt(strCPF.substring(10, 11) ) ){
            return false;
        }

        return true;
    }
</script>