<?php if(!isset($this->viewItems['category'])){?>
    <?php if(count($this->viewItems['main-banners']) > 0 && count($this->viewItems['seccondary-banners']) >= 2) {
        $bannersCount = count($this->viewItems['main-banners']);
        if($bannersCount > 5){
            $bannersCount = 5;
        }
        ?>
<div style="padding: 10px 0 25px 0;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<?php if(count($this->viewItems['stripe-banners']) > 0) {?>
<a href="<?php echo($this->viewItems['stripe-banners'][0]->url);?>">
    <img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['stripe-banners'][0]->image);?>" class="img-responsive" style="padding-bottom: 10px;">
</a>
<?php }?>
    <?//Monta e exibe os banners principais no carrossel do topo. ?>
<div style="overflow: hidden; padding: 0; padding-right: 15px" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="gallery autoplay items-<?php echo($bannersCount);?>">
                    <?php $count = 1;
                    foreach($this->viewItems['main-banners'] as $banner){?>
                        <div id="item-<?php echo($count);?>" class="control-operator"></div>
                        <?php
                        $count++;
                        if($count > $bannersCount){
                            break;
                        }
                    }?>
                    <?php $count = 1;
                    foreach($this->viewItems['main-banners'] as $banner){?>
                        <figure class="item">
                            <a href="<?php echo($banner->url);?>">
                                <img src="<?php echo(BANNERS_IMAGES_URL . $banner->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>">
                            </a>
                        </figure>
                        <?php
                        $count++;
                        if($count > $bannersCount){
                            break;
                        }
                    }?>
                    <div class="controls">
                        <?php $count = 1;
                        foreach($this->viewItems['main-banners'] as $banner){?>
                            <a href="#item-<?php echo($count);?>" class="control-button">•</a>
                            <?php
                            $count++;
                            if($count > $bannersCount){
                                break;
                            }
                        }?>
                    </div>
                </div>
            </div>

            <?//Exibe os banners secundários no topo. ?>
            <div style="overflow: hidden; padding-left: 15px; padding-bottom: 29px; padding-right: 0" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a href="<?php echo($this->viewItems['seccondary-banners'][0]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['seccondary-banners'][0]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
            </div>
            <div style="overflow: hidden; padding-left: 15px; padding-bottom: 0; padding-right: 0" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <a href="<?php echo($this->viewItems['seccondary-banners'][1]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['seccondary-banners'][1]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
            </div>
        </div>
    <?php }?>
<?php }else{?>
    <?php if(count($this->viewItems['seccondary-banners']) >= 2) {?>
        <div style="padding: 25px 0px 0px 0;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?//Exibe os banners secundários no topo. ?>
            <div style="overflow: hidden; padding-left: 15px; padding-bottom: 29px; padding-right: 0" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo($this->viewItems['seccondary-banners'][0]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['seccondary-banners'][0]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
            </div>
            <div style="overflow: hidden; padding-left: 15px; padding-bottom: 0; padding-right: 0" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo($this->viewItems['seccondary-banners'][1]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['seccondary-banners'][1]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
            </div>
        </div>
    <?php }?>
<?php }?>

<?php if(isset($this->viewItems['category'])){?>
    <h3 class="session-name"><img src="<?php echo(SITE_URL . 'Images/arrow-right.png');?>" class="img-responsive" style="display: inline-block; padding-right: 7px; margin-top: -4px;"><?php echo($this->viewItems['category']->name);?> :)</h3>
<?php }else{?>
    <h3 class="session-name"><img src="<?php echo(SITE_URL . 'Images/arrow-right.png');?>" class="img-responsive" style="display: inline-block; padding-right: 7px; margin-top: -4px;">Os mais acessados :)</h3>
<?php }?>

<div class="row">
<?php
$count = 0;
$countMain = 0;
$stripeBannerCount = 0;
$firstRow = true;

//Verifica se existe ao menos 3 banners de produtos para serem exibidos.
if(count($this->viewItems['product-banners']) < 3){
    $firstRow = false;
}

foreach($this->viewItems['products'] as $product){ ?>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="<?php
        if(isset($this->viewItems['category'])){
            echo(SITE_URL . 'produto' . DS . $product->id . '/' . strtolower(StringUtils::clean($this->viewItems['category']->name)) . '/' . strtolower(StringUtils::clean($product->name)));
        }else{
            echo(SITE_URL . 'produto' . DS . $product->id . '/' . strtolower(StringUtils::clean($product->name)));
        }
        ?>">
            <div class="product-grid transition">

                <img src="<?php
                if($product->image == "" || empty($product->image)){
                    echo(PRODUCTS_IMAGES_URL . 'no-image.png');
                }else{
                    echo(PRODUCTS_IMAGES_URL . $product->image);
                }
                ?>" class="img-responsive" alt="<?php echo($product->name);?>">

                <div class="product-name"><?php echo($product->name);?></div>

                <?php if($product->normalPrice > 0){?>
                    <div class="product-normal-price">de <?php echo(GeneralUtils::getFormattedValue($product->normalPrice));?> por</div>
                    <div class="product-offer-price"><?php echo(GeneralUtils::getFormattedValue($product->offerPrice));?></div>
                <?php }else{?>
                    <div class="product-offer-price"><?php echo(GeneralUtils::getFormattedValue($product->offerPrice));?></div>
                    <div class="product-normal-price">ou <?php
                        $inst = GeneralUtils::getMaxInstallments($product->offerPrice, MIN_INSTALLMENT_VALUE, MAX_INSTALLMENTS);
                        echo($inst['installments'] . 'x de ' . GeneralUtils::getFormattedValue($inst['value']) . ' no cartão');
                        ?></div>
                <?php }?>
            </div>
        </a>
    </div>

    <?php
    $count ++;
	$countMain ++;
    if($countMain == 4){
		$countMain = 0;
        echo('</div><div class="row">');
        

        if($count >= 7 && $firstRow == true){
			$count = 0;
            $firstRow = false;?>

            <div style="padding: 25px 0" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div style="overflow: hidden; padding: 0 10px;" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="<?php echo($this->viewItems['product-banners'][0]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['product-banners'][0]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
                </div>
                <div style="overflow: hidden; padding: 0 10px;" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="<?php echo($this->viewItems['product-banners'][1]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['product-banners'][1]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
                </div>
                <div style="overflow: hidden; padding: 0 10px;" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="<?php echo($this->viewItems['product-banners'][2]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['product-banners'][2]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
                </div>
            </div>

        <?php
		}
    }
    ?>

<?php }?>

<?php for($x = 1; $x < count($this->viewItems['stripe-banners']); $x++) {?>
		<a href="<?php echo($this->viewItems['stripe-banners'][$x]->url);?>">
			<img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['stripe-banners'][$x]->image);?>" class="img-responsive" style="padding-bottom: 10px;">
		</a>
<?php } ?>

</div>