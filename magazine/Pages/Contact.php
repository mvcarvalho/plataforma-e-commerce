<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12"></div>
<div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
    <h2>Fale Conosco!</h2>
    <p style="font-size: 13px; color: #555555">O <?php echo(SITE_NAME)?> entende que o bom relacionamento com o cliente é fundamental para a satisfação de todos.</p>
    <p style="font-size: 13px; color: #555555">Sendo assim, disponibilizamos as informações de contato abaixo para que você possa nos contactar sempre que julgar necessário. Vamos retornar o mais breve possível!</p>
    <p style="font-size: 13px; color: #555555">Críticas, sugestões ou qualquer outro assunto, sinta-se à vontade para se expressar.</p><br><br>
    <p style="font-size: 13px; color: #555555">E-mail: <?php echo(SITE_CONTACT_MAIL)?></p>

    <h3>Você também pode preencher este formulário</h3>
    <?php if(isset($this->viewItems['message'])){?>
        <h4>Mensagem enviada com sucesso. Em breve responderemos.</h4>
    <?php }?>
    <form role="form" method="post" style="padding-bottom: 20px">
        <div class="form-group">
            <label for="email">E-mail</label>
            <input class="form-control" type="text" name="email" id="email" placeholder="seuemail@exemplo.com.br" required>
        </div>
        <div class="form-group">
            <label for="name">Nome</label>
            <input class="form-control" type="text" name="name" id="name" required placeholder="Seu Nome Completo">
        </div>
        <div class="form-group">
            <label for="message">Mensagem</label>
            <textarea class="form-control" name="message" id="message" required style="resize: vertical; height: 100px" placeholder="Mensagem para nossa equipe..."></textarea>
        </div>
        <button type="submit" class="btn btn-success" name="login">Enviar</button>
    </form>

    <h3>Você ainda pode abrir um ticket.</h3>
    <p style="font-size: 13px; color: #555555">Os tickets são vistos com um problema mais sério. Quando algum produto não chega, algo foi enviado de forma errada ou algo que esteja lhe causando problemas.</p>
    <a href="<?php echo(SITE_URL)?>user/ticket-list"><button class="btn btn-success">Ir aos meus tickets</button></a>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 20px"></div>
</div>
<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12"></div>