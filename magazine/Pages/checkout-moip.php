<!--CHECKOUT MOIP-->
<script src="<?php echo(SITE_URL);?>js/MoipWidget-v2.js"></script>

<div id="cardInfo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none">
    <div class="col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-10">
        <form role="form" id="cardForm">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Bandeira</label>
                        <select class="form-control" id="cardFlag">
                            <option>AmericanExpress</option>
                            <option>Diners</option>
                            <option>Mastercard</option>
                            <option>Hipercard</option>
                            <option>Visa</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-right: 0">
                    <div class="form-group">
                        <label class="control-label">Parcelas</label>
                        <select class="form-control" id="installments">
                            <?php foreach($this->pageItems['installments'] as $k => $installment){?>
                                <option value="<?php echo($k);?>"><?php echo($k . ' x ' . GeneralUtils::getFormattedValue($installment['value']));?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Número do Cartão</label>
                    <input class="form-control" type="text" id="cardNumber" placeholder="Número Cartão">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-left: 0">
                    <div class="form-group">
                        <label class="control-label">Código de Segurança</label>
                        <input class="form-control" type="text" id="securityCode" placeholder="Código de Segurança">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-right: 0">
                    <div class="form-group">
                        <label class="control-label">Validade do Cartão</label>
                        <input class="form-control" type="text" id="cardExpiration" placeholder="Expiração (00/00)">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Nome do proprietário (Impresso no cartão)</label>
                    <input class="form-control" type="text" id="cardholderName" placeholder="Nome no Cartão">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">CPF do proprietário</label>
                    <input class="form-control" type="text" id="cpf" placeholder="CPF">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Data de Nascimento do proprietário</label>
                    <input class="form-control" type="text" id="birthdate" placeholder="Data Nascimento">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Telefone do proprietário</label>
                    <input class="form-control" type="text" id="phone" placeholder="Telefone">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Concluir pagamento" class="btn buy">
                </div>
            </div>
        </form>
    </div>
</div><br>

<div id="dataSet">
</div>

<script type="text/javascript">
    var paymentMethod = "";
    var usrId = "";
    var paymentToken = "";

    $(document).ready(function(){
        $("#cardExpiration").mask("99/99");
        $("#cpf").mask("999.999.999-99");
        $("#birthdate").mask("99/99/9999");
        $("#phone").mask("(99)9999-9999");
    });

    function pagarComCartao(){
        var div = document.getElementById('pay');
        div.innerHTML = '';
        var result = preparePayment(<?php echo($total + $this->pageItems['deliveryCost']);?>, 'Compra <?php echo(SITE_NAME);?>', <?php echo($this->pageItems['user']->id);?>);
        if(result != 'error_0' && result != 'error_1' && result != 'error_2' && result != 'error_4'  && result != 'error_5'){
            document.getElementById('cardInfo').style.display = "block";
            paymentMethod = "creditCard";
        }
    }

    $("#cardForm").submit(function( event ) {
        event.preventDefault();
        var re = new RegExp('_', 'g');
        var exp = document.getElementById('cardExpiration').value.replace(re, "");
        var cpf = document.getElementById('cpf').value.replace(re, "");
        var name = document.getElementById('cardholderName').value.replace(re, "");
        var card = document.getElementById('cardNumber').value.replace(re, "");
        var securityCode = document.getElementById('securityCode').value.replace(re, "");
        var birthdate = document.getElementById('birthdate').value.replace(re, "");
        var phone = document.getElementById('phone').value.replace(re, "");
        var cardFlag = document.getElementById('cardFlag');
        var installments = document.getElementById('installments');

        var settings = {
            "Forma": "CartaoCredito",
            "Instituicao": cardFlag.options[cardFlag.selectedIndex].text,
            "Parcelas": installments.options[installments.selectedIndex].value,
            "CartaoCredito": {
                "Numero": card,
                "Expiracao": exp,
                "CodigoSeguranca": securityCode,
                "Portador": {
                    "Nome": name,
                    "DataNascimento": birthdate,
                    "Telefone": phone,
                    "Identidade": cpf
                }
            }
        };
        MoipWidget(settings);
        return false;
    });

    function pagarComBoleto(){
        var div = document.getElementById('pay');
        div.innerHTML = '';
        paymentMethod = "ticket";
        var result = preparePayment(<?php echo($total + $this->pageItems['deliveryCost']);?>, 'Compra <?php echo(SITE_NAME);?>', <?php echo($this->pageItems['user']->id);?>);
        if(result != 'error_0' && result != 'error_1' && result != 'error_2' && result != 'error_4'  && result != 'error_5'){
            var settings = {"Forma": "BoletoBancario"};
            MoipWidget(settings);
        }
    }
    function preparePayment(value, reason, userId){
        usrId = userId;

        var re = new RegExp(' ', 'g');
        reason = reason.replace(re, '%20');

        var url = '<?php echo(SITE_URL);?>APIs/moip_prepare_payment.php?';
        url += 'value=' + value;
        url += '&reason=' + reason;
        url += '&userId=' + userId;
        url += '&token=' + '<?php echo('moip');?>_'+userId+'_<?php echo(time());?>';

        var result = httpGet(url);
        if(result != 'error_0' && result != 'error_1' && result != 'error_2' && result != 'error_4'  && result != 'error_5'){
            paymentToken = result;
            buildDataSet(result);
        }

        return result;
    }
    function finishPayment(status, userId){
        var url = '<?php echo(SITE_URL);?>APIs/moip_finish_payment.php?userId='+userId+'&status='+status+'&token=<?php echo('moip');?>_'+userId+'_<?php echo(time());?>';
        httpGet(url);
    }
    function buildDataSet(token){
        var dataSet = document.getElementById('dataSet');
        dataSet.innerHTML =
            '<div id="MoipWidget"' +
                'data-token="' + token + '"' +
                'callback-method-success="paySuccess"' +
                'callback-method-error="payError"></div>';
    }

    function paySuccess(data){
        if(paymentMethod == "ticket"){
            finishPayment(5, usrId);
            var div = document.getElementById('pay');
            div.innerHTML = '<a target="_blank" onclick="redirectToHistory();" href="' + data.url + '"><button class="btn buy pull-right" style="margin-right: 10px">Imprimir Boleto</button></a>';
        }else{
            if(data.Status == 'Autorizado'){
                finishPayment(3, usrId);
                alert("Pagamento realizado com sucesso.\nVocê será redirecionado ao seu histórico de pedidos.");
                redirectToHistory();

            }else if(data.Status == 'EmAnalise' || data.Status == 'Iniciado'){
                finishPayment(5, usrId);
                alert("Seu pagamento está em processamento.\nAssim que confirmado, vamos iniciar o processo de envio do produto.\nVocê será redirecionado ao seu histórico de pedidos.");
                redirectToHistory();

            }else{
                alert("Seu pagamento foi cancelado pela operadora do seu cartão.\nEntre em contato com nossa equipe para finalizar a compra.");
            }
        }
    }

    function payError(data){
        if(paymentMethod == "ticket"){
            alert("Falha ao gerar boleto.\nTente gerar novamente, pois muitos acessos ao site\npodem gerar tráfego muito alto e causar problemas.\nCaso o problema persista, entre em contato com nosso suporte." );
        }else{
            alert("Falha ao processar pagamento.\n" + data.Mensagem );
        }
    }

    function redirectToHistory(){
        window.location = "<?php echo(SITE_URL . 'user/history');?>";
    }
</script>