<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form">
        <div class="title">Digite seu e-mail:</div>
        <p>Você receberá um e-mail com instruções para resetar sua senha.</p>
        <?php if(isset($this->pageItems['msg'])){?>
            <div class="alert alert-warning" role="alert"><?php echo($this->pageItems['msg']);?></div>
        <?php }?>
        <form role="form" method="post">
            <div class="form-group">
                <label for="email">E-mail</label>
                <input class="form-control" type="email" name="email" id="email" required>
            </div>
            <button type="submit" class="btn btn-danger" name="login">Enviar</button>
        </form>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
</div>