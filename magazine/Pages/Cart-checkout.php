<?php
$total = $this->pageItems['final'];
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cart">
    <div class="row" style="padding: 20px 0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">Verifique sua compra!</div>

        <p style="padding-left: 10px; padding-right: 10px; font-size: 13px; color: #555555">Verifique as informações abaixo. Tenha certeza de que estão corretas.</p>
        <p style="padding-left: 10px; padding-right: 10px; font-size: 13px; color: #555555">Caso alguma de suas informações estiver errada, altere no seu cadastro e realize o checkut novamente.</p>
        <p style="padding-left: 10px; padding-right: 10px; padding-bottom: 20px; font-size: 13px; color: #555555">Queremos garantir que tudo esteja perfeito, para você ter a certeza de que estará tudo bem após a compra!</p>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">Estes os itens da compra:</div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cart-list" style="padding: 0">
            <?php foreach($this->pageItems['cart']->items as $item){ ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-checkout">
                    <div class="col-lg-4 col-md-5 col-sm-4 col-xs-12" style="padding-top: 9px;"><?php echo($item->product->name);?></div>
                    <div class="col-lg-8 col-md-7 col-sm-8 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 info"><div style="text-align: center; padding-top: 5px;">Prazo de Entrega:<br><?php echo($item->product->deliveryDays);?></div></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 info"><div style="text-align: center; padding-top: 5px;">Quantidade:<br><?php echo($item->quantity);?></div></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 info"><div style="text-align: center; font-weight: bold;font-size: 14px">Valor Unitário:<br>R$ <?php echo(number_format($item->price, 2, ',', '.'));?></div></div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 info"><div style="text-align: center; font-size: 14px;">Total:<br>R$ <?php echo(number_format($item->price * $item->quantity, 2, ',', '.'));?></div></div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title" style="padding: 20px">Estas são as informações de entrega:</div>
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <td >Telefone de contato</td>
                <td ><?php echo($this->pageItems['user']->phone);?></td>
            </tr>
            <tr>
                <td >Endereço de entrega</td>
                <td >
                    <?php
                    echo(
                        $this->pageItems['user']->address . ', ' .
                        $this->pageItems['user']->city. ' - ' .
                        $this->pageItems['user']->state . ', CEP ' .
                        $this->pageItems['user']->cep);
                    ?>
                </td>
            </tr>
            <tr>
                <td >Prazo de entrega</td>
                <td >Descrito em cada um dos items acima. Quando não descrito no item, o prazo é de 7 à 15 dias úteis.</td>
            </tr>
            <tr>
                <td >Valor do Frete</td>
                <td >R$ <?php echo(number_format($this->pageItems['deliveryCost'], 2, ',', '.'));?></td>
            </tr>

            <?php if(isset($this->pageItems['coupon'])){?>
                <tr>
                    <td >Desconto do cupom <?php echo($this->pageItems['coupon']);?></td>
                    <td >R$ <?php echo(number_format($this->pageItems['coupon_off'], 2, ',', '.'));?></td>
                </tr>
            <?php }?>

            <tr>
                <td >Total em produtos</td>
                <td >R$ <?php echo(number_format($this->pageItems['total'], 2, ',', '.'));?></td>
            </tr>
        </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 total" style="margin-bottom: 15px; text-align: right">Total: R$ <?php echo(number_format($this->pageItems['final'], 2, ',', '.'));?></div>

    <div id="pay" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
        <?php
        $user = $this->pageItems['user'];
        if($user->cep == "" || $user->address == "" || $user->city == "" || $user->state == "" || $user->phone == ""){?>
            <br>
            <p style="text-align: right">Falta preencher alguma informação no seu cadastro. Corrija antes de finalizar a compra.</p>
            <a href="<?php echo(SITE_URL . 'user/register');?>"><button class="btn btn-danger pull-right" style="margin-right: 10px">Corrigir Cadastro</button></a>

        <?php }else{?>
            <a onclick="pagarComBoleto();"><button class="btn buy pull-right" style="margin-right: 10px">Gerar Boleto</button></a>
            <a onclick="pagarComCartao();"><button class="btn buy pull-right" style="margin-right: 10px">Pagar com Cartão</button></a>

            <img src="<?php echo(SITE_URL);?>Images/<?php
            if(GATEWAY_INTEGRATION == "mp"){
                echo("powered-mp.png");

            } else if(GATEWAY_INTEGRATION == "moip"){
                echo("powered-moip.png");

            }?>" class="img-responsive" style="float: right; padding: 0 20px; height: 58px;">

        <?php }?>
    </div>
    <?php
    if(GATEWAY_INTEGRATION == "mp"){
        include_once(SITE_FOLDER . 'Pages/checkout-mp.php');

    } else if(GATEWAY_INTEGRATION == "moip"){
        include_once(SITE_FOLDER . 'Pages/checkout-moip.php');
    }?>

</div>