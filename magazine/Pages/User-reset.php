<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form">
        <div class="title">Digite sua nova senha:</div>
        <form role="form" method="post">
            <div class="form-group">
                <label for="password">Nova senha</label>
                <input class="form-control" type="password" name="password" id="password" required>
            </div>
            <div class="form-group">
                <label for="password-confirm">Senha</label>
                <input class="form-control" type="password" name="password-confirm" id="password-confirm" required>
            </div>
            <button type="submit" class="btn btn-danger" name="login">Salvar</button>
        </form>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
</div>