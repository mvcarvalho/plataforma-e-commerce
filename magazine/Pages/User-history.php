<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cart">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">Histórico de Pedidos:</div>
        <p style="padding-left: 10px; font-size: 13px; color: #555555">Este é seu histórico de pedidos. Todos os pedidos estarão listados aqui com seus respectivos itens.</p>
        <p style="padding-left: 10px; font-size: 13px; color: #555555">Entre as informações, o status do pedido possui a informação sobre a situação atual que se encontra o pedido. Esta informação pode estar acompanhada de um link, caso exista a possibilidade de realizar alguma ação referente ao pagamento.</p>
        <p style="padding-left: 10px; font-size: 13px; color: #555555">Note que, se o status do pedido for "Aguardando Mercado Pago" e você tenha pago por boleto, a confirmação pode demorar até 2 dias para chegar em nosso sistema. Então não se preocupe, apenas aguarde que a confirmação chegue e enviaremos sua compra em seguida.</p>
        <p style="padding-left: 10px; padding-bottom: 10px; font-size: 13px; color: #555555">Se você possuir alguma dúvida, entre em contato com nossa equipe. Estaremos sempre à disposição para ajudá-lo.</p>
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >Data</th>
                <th >Item</th>
                <th >Quantidade</th>
                <th >Valor</th>
                <th >Status</th>
                <th >Rastreio</th>
            </tr>
            <?php foreach($this->pageItems['sales'] as $sale){?>
                <tr>
                    <td style="padding-top: 35px; padding-bottom: 3px"><?php echo($sale->date);?></td>
                    <td style="padding-top: 35px; padding-bottom: 3px"></td>
                    <td style="padding-top: 35px; padding-bottom: 3px"></td>
                    <td style="padding-top: 35px; padding-bottom: 3px">R$ <?php echo(number_format($sale->value, 2, ',', '.'));?></td>
                    <td style="padding-top: 35px; padding-bottom: 3px"><?php
                        if($sale->status == 6){
                            echo('Pagamento Expirado/Cancelado');

                        }else if($sale->status == 5){
                            echo('Aguardando Confirmação');

                        } else if($sale->status == 4){
                            echo('Produto enviado');

                        } else if($sale->status == 3){
                            echo('Pagamento completo');

                        } else if($sale->status == 2){
                            if(GATEWAY_INTEGRATION == "moip"){
                                echo('Falha no pagamento');
                            }else{
                                echo('Falha no pagamento - <a href="'.SITE_URL.'user/pay-sale/'.$sale->id.'">Pagar</a>');
                            }

                        } else if($sale->status == 1){
                            if(GATEWAY_INTEGRATION == "moip"){
                                echo('Pagamento pendente');
                            }else{
                                echo('Pagamento pendente - <a href="'.SITE_URL.'user/pay-sale/'.$sale->id.'">Pagar</a>');
                            }

                        } else if($sale->status == 0){
                            if(GATEWAY_INTEGRATION == "moip"){
                                echo('Não confirmado');
                            }else{
                                echo('Não confirmado - <a href="'.SITE_URL.'user/pay-sale/'.$sale->id.'">Pagar</a>');
                            }
                        }
                        ?></td>
                    <td style="padding-top: 35px; padding-bottom: 3px"><?php echo($sale->trackingCode);?></td>
                </tr>
                <?php foreach($sale->items as $item){?>
                    <tr style="font-size: 13px">
                        <td style="padding: 5px"></td>
                        <td style="padding: 5px"><?php
                            echo($item->product->shortDescription);
                            if(strlen($item->optionsText) > 0){
                                echo( " - " . $item->optionsText);
                            }?></td>
                        <td style="padding: 5px"><?php echo($item->quantity);?></td>
                        <td style="padding: 5px">R$ <?php echo(number_format($item->price * $item->quantity, 2, ',', '.'));?></td>
                        <td style="padding: 5px"></td>
                        <td style="padding: 5px"></td>
                    </tr>
                <?php }?>
            <?php }?>
        </table>
    </div>
</div>