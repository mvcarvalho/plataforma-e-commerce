<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Abrir novo ticket</h3>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form role="form" method="post" style="padding-bottom: 20px">
        <div class="form-group">
            <label for="subject">Assunto</label>
            <input class="form-control" type="text" name="subject" id="subject" placeholder="Assunto (um título para o ticket)" required>
        </div>
        <div class="form-group">
            <label for="saleid">Número do Pedido</label>
            <input class="form-control" type="text" name="saleid" id="saleid" placeholder="Número do pedido" required>
        </div>
        <div class="form-group">
            <label for="message">Mensagem</label>
            <textarea class="form-control" name="message" id="message" required style="resize: vertical; height: 100px" placeholder="Mensagem para nossa equipe..."></textarea>
        </div>
        <button type="submit" class="btn btn-success" name="send">Enviar</button>
    </form>
</div>