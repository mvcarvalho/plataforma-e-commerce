<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cart">
    <div class="row" style="padding: 20px">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">Novo pagamento!</div>

        <p style="padding-left: 10px; padding-right: 10px; font-size: 13px; color: #555555">Verifique as informações abaixo. Tenha certeza de que estão corretas.</p>
        <p style="padding-left: 10px; padding-right: 10px; font-size: 13px; color: #555555">Caso alguma de suas informações estiver errada, altere no seu cadastro e realize o checkut novamente.</p>
        <p style="padding-left: 10px; padding-right: 10px; padding-bottom: 20px; font-size: 13px; color: #555555">Queremos garantir que tudo esteja perfeito, para você ter a certeza de que estará tudo bem após a compra!</p>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title" style="padding: 20px; padding-left: 35px;">Informações de entrega:</div>
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <td >Telefone de contato</td>
                <td ><?php echo($this->pageItems['user']->phone);?></td>
            </tr>
            <tr>
                <td >Endereço de entrega</td>
                <td >
                    <?php
                    echo(
                        $this->pageItems['user']->address . ', ' .
                        $this->pageItems['user']->city. ' - ' .
                        $this->pageItems['user']->state . ', CEP ' .
                        $this->pageItems['user']->cep);
                    ?>
                </td>
            </tr>
            <tr>
                <td >Prazo de entrega</td>
                <td >Descrito em cada um dos items acima. Quando não descrito no item, o prazo é de 7 à 15 dias úteis.</td>
            </tr>
        </table>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 total" style="margin-bottom: 15px; text-align: right">Total: R$ <?php echo(number_format($this->pageItems['sale']->value, 2, ',', '.'));?></div>

    <div id="pay" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
        <a onclick="executePay(false);"><button class="btn btn-danger pull-right" style="margin-right: 10px">Gerar Boleto</button></a>
        <a onclick="onClickCardPay();"><button class="btn btn-danger pull-right" style="margin-right: 10px">Pagar com Cartão</button></a>
    </div>

    <div id="card-pay" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display: none">

        <div class="col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-10">
            <h2 style="padding-bottom: 30px">Dados de pagamento</h2>

            <form role="form" action="<?php echo(SITE_URL . 'cart' . DS . 'finalize' . DS . 'credit-card');?>" method="post" id="form-pagar-mp">
                <input id="amount" type="hidden" value="<?php echo($this->pageItems['sale']->value);?>"/>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Número do Cartão</label>
                        <input class="form-control" data-checkout="cardNumber" type="text" id="cardNumber"/>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0">
                        <div class="form-group">
                            <label class="control-label">Código de Segurança</label>
                            <input class="form-control" data-checkout="securityCode" type="text" placeholder="123" id="securityCode"/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 0">
                        <div class="form-group">
                            <label class="control-label">Mês de Vencimento</label>
                            <input class="form-control" data-checkout="cardExpirationMonth" type="text" placeholder="12" id="cardExpirationMonth"/>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0">
                        <div class="form-group">
                            <label class="control-label">Ano de Vencimento</label>
                            <input class="form-control" data-checkout="cardExpirationYear" type="text" placeholder="2020" id="cardExpirationYear"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Nome impresso no cartão</label>
                        <input class="form-control" data-checkout="cardholderName" type="text" placeholder="JOAO SILVA SOUZA" id="cardholderName"/>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">CPF do titular (somente números)</label>
                        <input class="form-control" data-checkout="docNumber" type="text" placeholder="12345678910" id="cpf"/>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Parcelas</label>
                        <select class="form-control" id="installmentsOption" name="installmentsOption"></select>
                    </div>
                </div>
                <input data-checkout="docType" type="hidden" value="CPF"/>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input type="submit" value="Concluir pagamento" class="btn btn-info">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
    </div>

    <!-- <div id="console" style="width: 100%; padding: 0; color: #ffffff; background: #444444"> -->

    </div>

</div>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="https://secure.mlstatic.com/org-img/checkout/custom/1.0/checkout.js"></script>
<script type="text/javascript">
    Checkout.setPublishableKey("80c75eec-d69e-481b-ab8e-98252fd60cc9");

	$(document).ready(function(){
        $("#cardExpirationYear").mask("9999");
        $("#cardExpirationMonth").mask("99");
        //$("#cpf").mask("999.999.999-99");
    });
	
    $("#form-pagar-mp").submit(function( event ) {
        event.preventDefault();

        var year = document.getElementById('cardExpirationYear').value;
        var month = document.getElementById('cardExpirationMonth').value;
        var cpf = document.getElementById('cpf').value;
        var name = document.getElementById('cardholderName').value;
        var card = document.getElementById('cardNumber').value;
        var securityCode = document.getElementById('securityCode').value;

        if(year < 2014){
            alert("Verifique o ano de validade digitado.");
            return false;

        }else if(month > 12 || month <= 0){
            alert("Verifique o mês de validade digitado.");
            return false;

        }else if(name.length < 4){
            alert("Verifique o nome digitado.");
            return false;

        }else if(card.length < 12){
            alert("Verifique o número do cartão digitado.");
            return false;

        }else if(securityCode.length < 3){
            alert("Verifique o código de segurança digitado.");
            return false;

        }else{
            var $form = $(this);
            Checkout.createToken($form, mpResponseHandler);
            return false;
        }
    });

    var mpResponseHandler = function(status, response) {
        var $form = $('#form-pagar-mp');

        if (response.error) {
            alert("Não conseguimos iniciar o pagamento.\nVerifique suas informações e tente novamente.");

        } else {
            var card_token_id = response.id;
            $form.append($('<input type="hidden" id="card_token_id" name="card_token_id"/>').val(card_token_id));
            //alert(response);
            //$form.get(0).submit();
            executePay(true);
        }
    }
	
    $("input[data-checkout='cardNumber']").bind("keyup",function(){
        var bin = $(this).val().replace(/ /g, '').replace(/-/g, '').replace(/\./g, '');
        if (bin.length >= 6){
            Checkout.getPaymentMethod(bin,setPaymentMethodInfo);
        }
    });

    //Estabeleça a informação do meio de pagamento obtido
    function setPaymentMethodInfo(status, result){
        $.each(result, function(p, r){
            $.each(r.labels, function(pos, label){
                Checkout.getInstallments(r.id ,parseFloat($("#amount").val()), setInstallmentInfo);
                return;
            });
        });
    };

    //Mostre as parcelas disponíveis no div 'installmentsOption'
    function setInstallmentInfo(status, installments){
        var html_options = "";
        for(i=0; installments && i<installments.length; i++){
            html_options += "<option value='"+installments[i].installments+"'>"+installments[i].installments +" de "+installments[i].share_amount+" ("+installments[i].total_amount+")</option>";
        };
        $("#installmentsOption").html(html_options);
    };

    function onClickCardPay(){
        var div = document.getElementById('card-pay');
        div.style.display = 'block';
    }

    function httpGet(theUrl)
    {
        var xmlHttp = null;
        xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", theUrl, false );
        xmlHttp.send( null );
        return xmlHttp.responseText;
    }

    function executePay(isCard){
        var url = "";

        if(isCard == true){
            var select = document.getElementById('installmentsOption');
            var cardToken = document.getElementById('card_token_id').value;
            var installments = select.options[select.selectedIndex].value;
            url = "<?php echo(SITE_URL . 'APIs/mercadopago_execute_new_payment.php')?>?method=credit-card&sale_id=<?php echo($this->pageItems['sale']->id);?>&cardTokenId=" + cardToken + "&installmentsOption=" + installments;

        }else{
            url = "<?php echo(SITE_URL . 'APIs/mercadopago_execute_new_payment.php')?>?method=ticket&sale_id=<?php echo($this->pageItems['sale']->id);?>";
        }

        var result = httpGet(url + "");

        //var console = document.getElementById('console');
        //console.innerHTML = result;

		if(result == '1'){
            alert('Pagamento efetuado com sucesso.\nVocê será redirecionado ao seu histórico.');
            window.location = "<?php echo(SITE_URL . 'user/history');?>";

        }else if(result == '0'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, entre em contato com nosso atendimento.');

        }else if(result == '2'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, ligue para a administradora do seu cartão para autorizar o pagamento e tente novamente.');

        }else if(result == '3'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nSaldo insuficiente. Por favor, utilize outro cartão.');

        }else if(result == '4'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nCódigo de segurança inválido. Verifique e tente novamente.');

        }else if(result == '5'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nCartão com validade expirada. Por favor, verifique se a informação está correta ou utilize outro cartão.');

        }else if(result == '6'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, verifique suas informações e tente novamente.\nSe o problema persistir, entre em contato com nosso atendimento.');

        }else if(result == '7'){
            document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
            alert('Não conseguimos realizar o pagamento.\nPor favor, verifique suas informações e tente novamente.\nSe o problema persistir, entre em contato com nosso atendimento.');

        }else{
            if(isCard){
                document.getElementById('form-pagar-mp').removeChild(document.getElementById('card_token_id'));
                alert('Não conseguimos realizar o pagamento.\nPor favor, verifique suas informações e tente novamente.\nSe o problema persistir, entre em contato com nosso atendimento.');

            }else{
                var div = document.getElementById('pay');
                div.innerHTML = '<a target="_blank" onclick="redirectToHistory();" href="' + result + '"><button class="btn btn-danger pull-right" style="margin-right: 10px">Imprimir Boleto</button></a>';
            }
        }
    }

    function redirectToHistory(){
        window.location = "<?php echo(SITE_URL . 'user/history');?>";
    }

</script>
