<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Produtos</h3>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-top: 10px; padding-bottom: 10px">
        <a href="<?php echo(SITE_URL . 'adm/product/add');?>" class="btn btn-info pull-right">Adicionar</a>
    </div>

    <div class="row" style="padding-top: 10px">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >ID</th>
                <th >Nome</th>
                <th >Preço</th>
                <th >Preço Oferta</th>
                <th >Vendas</th>
                <th >Visualizações</th>
                <th style="text-align: center;">Ações</th>
            </tr>
            <?php foreach($this->viewItems['products'] as $product){?>
                <tr>
                    <td ><?php echo($product->id);?></td>
                    <td ><?php echo($product->name);?></td>
                    <td ><?php echo($product->normalPrice);?></td>
                    <td ><?php echo($product->offerPrice);?></td>
                    <td ><?php echo($product->sales);?></td>
                    <td ><?php echo($product->views);?></td>
                    <td style="text-align: center;">
                        <a href="<?php echo($this->viewItems['viewLink'] . $product->id)?>">[View]</a>
                        <a href="<?php echo(SITE_URL . 'adm/product/deactivate/' . $product->id)?>">[<?php
                            if($product->isActive == 1){
                                echo('Desativar');
                            }else{
                                echo('Ativar');
                            }
                            ?>]</a></td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>