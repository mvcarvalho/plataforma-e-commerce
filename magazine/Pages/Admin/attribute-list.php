<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Atributos</h3>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-top: 10px; padding-bottom: 10px">
        <a href="<?php echo(SITE_URL . 'adm/attribute/add');?>" class="btn btn-info pull-right">Adicionar</a>
    </div>

    <div class="row" style="padding-top: 10px">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >Nome</th>
                <th >Observação Interna</th>
                <th style="text-align: center;">Ações</th>
            </tr>
            <?php foreach($this->viewItems['attributes'] as $attribute){?>
                <tr>
                    <td><?php echo($attribute->name);?></td>
                    <td><?php echo($attribute->observation);?></td>
                    <td style="text-align: center;"><a href="<?php echo($this->viewItems['viewLink'] . $attribute->id);?>">[View]</a><?php ?></td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>