<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Adicionar Atributo</h3>
    <form rule="form" method="post" action="<?php echo $this->viewItems['formaction'];?>">
        <input type="hidden" name="id" value="0">
        <input type="hidden" name="isrequired" value="0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" id="name" name="name" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="observation">Observação Interna</label>
                <input type="text" id="observation" name="observation" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>