<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Detalhes do Cliente</h3>
    <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
        <tr>
            <td >Cliente</td>
            <td ><?php echo($this->viewItems['user']->name);?></td>
        </tr>
        <tr>
            <td >Contato</td>
            <td ><?php echo($this->viewItems['user']->email . ' - ' . $this->viewItems['user']->phone);?></td>
        </tr>
        <tr>
            <td >CPF / CNPJ</td>
            <td ><?php echo($this->viewItems['user']->cpf);?></td>
        </tr>
        <tr>
            <td >Endereço</td>
            <td ><?php echo($this->viewItems['user']->address);?></td>
        </tr>
        <tr>
            <td >Cidade</td>
            <td ><?php echo($this->viewItems['user']->city . ' - ' . $this->viewItems['user']->state);?></td>
        </tr>
        <tr>
            <td >CEP</td>
            <td ><?php echo($this->viewItems['user']->cep);?></td>
        </tr>
    </table>

    <h3>Detalhes da Venda</h3>

    <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
        <tr>
            <td>Data da Venda</td>
            <td><?php echo($this->viewItems['sale']->date);?></td>
        </tr>
        <tr>
            <td>Valor total</td>
            <td><?php echo(number_format($this->viewItems['sale']->value, 2, ',', '.'));?></td>
        </tr>
        <tr>
            <td>Status Atual</td>
            <td><?php
                if($sale->status == 6){
                    echo('Pagamento Expirado (Cancelado)');

                }else if($this->viewItems['sale']->status == 5){
                    echo('Aguardando Confirmação');

                } else if($this->viewItems['sale']->status == 4){
                    echo('Produto enviado');

                } else if($this->viewItems['sale']->status == 3){
                    echo('Pagamento completo');

                } else if($this->viewItems['sale']->status == 2){
                    echo('Falha no pagamento');

                } else if($this->viewItems['sale']->status == 1){
                    echo('Pagamento pendente');

                } else if($this->viewItems['sale']->status == 0){
                    echo('Não confirmado');
                }
                ?></td>
        </tr>
        <tr>
            <td>Id da transação</td>
            <td><?php echo($this->viewItems['sale']->externalToken);?></td>
        </tr>
        <tr>
            <td>Código de Rastreio</td>
            <td><?php echo($this->viewItems['sale']->trackingCode);?></td>
        </tr>
    </table>

    <h3>Itens da Venda</h3>
    <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
        <tr style="font-size: 14px">
            <th>ID</th>
            <th>Produto</th>
            <th>Quantidade</th>
            <th>Valor unitário</th>
            <th>Valor total</th>
        </tr>
        <?php foreach($this->viewItems['sale']->items as $item){?>
            <tr style="font-size: 14px">
                <td>
                    <?php
                    echo($item->product->id);
                    ?>
                </td>
                <td><?php
                    echo($item->product->shortDescription);
                    if(strlen($item->optionsText) > 0){
                        echo( " - " . $item->optionsText);
                    }?></td>
                <td><?php echo($item->quantity);?></td>
                <td>R$ <?php echo(number_format($item->price, 2, ',', '.'));?></td>
                <td>R$ <?php echo(number_format($item->price * $item->quantity, 2, ',', '.'));?></td>
            </tr>
        <?php }?>
    </table>

    <form rule="form" method="post" enctype="multipart/form-data" action="<?php echo $this->viewItems['viewLink'] . $this->viewItems['sale']->id;?>">
        <input type="hidden" name="id" value="<?php echo($this->viewItems['sale']->id);?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="tracking">Adicionar/Modificar Código de Rastreio</label>
                <input type="text" name="tracking" class="form-control" value="<?php echo($this->viewItems['sale']->trackingCode);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>