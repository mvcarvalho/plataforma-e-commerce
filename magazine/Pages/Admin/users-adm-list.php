<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Usuários do Sistema</h3>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-top: 10px; padding-bottom: 10px">
        <a href="<?php echo(SITE_URL . 'adm/users-adm/add');?>" class="btn btn-info pull-right">Adicionar</a>
    </div>

    <div class="row" style="padding-top: 10px">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >ID</th>
                <th >Login</th>
                <th >Nome</th>
                <th style="text-align: center;">Ações</th>
            </tr>
            <?php foreach($this->viewItems['users'] as $user){?>
                <tr>
                    <td ><?php echo($user->id);?></td>
                    <td ><?php echo($user->login);?></td>
                    <td ><?php echo($user->name);?></td>
                    <td style="text-align: center;">
                        <a href="<?php echo(SITE_URL . 'adm/users-adm/view/' . $user->id)?>">[View]</a>
                        <a href="<?php echo(SITE_URL . 'adm/users-adm/deactivate/' . $user->id)?>">[<?php
                            if($user->isActive == 1){
                                echo('Desativar');
                            }else{
                                echo('Ativar');
                            }
                            ?>]
                        </a>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>