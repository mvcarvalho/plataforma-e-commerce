<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Categoria</h3>
    <form rule="form" method="post" enctype="multipart/form-data" action="<?php echo $this->viewItems['formaction'];?>">
        <input type="hidden" name="id" value="0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" name="name" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="description">Descrição</label>
                <textarea id="description" name="description" class="ckeditor" style="min-height: 200px"></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="image">Imagem</label>
                <input type="file" name="image" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="category">Categoria Pai</label>
                <select name="category" class="form-control">
                    <option value="0">Nenhuma</option>
                    <?php foreach($this->viewItems['categories'] as $category){?>
                        <option value="<?php echo($category->id); ?>"><?php echo($category->name); ?></option>
                    <?php }?>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar Nova Categoria" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>
<script src="//cdn.ckeditor.com/4.4.5.1/standard/ckeditor.js"></script>