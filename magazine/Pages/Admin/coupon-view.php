<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Cupom</h3>
    <form rule="form" method="post" enctype="multipart/form-data" action="<?php echo $this->viewItems['formaction'];?>">
        <input type="hidden" name="id" value="<?php echo($this->viewItems['coupon']->id);?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Código</label>
                <input type="text" name="key" class="form-control" value="<?php echo($this->viewItems['coupon']->key);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="off-type">Tipo de desconto</label>
                <select name="off-type" class="form-control">
                    <option value="1" <?php if($this->viewItems['coupon']->offType == 1){echo('selected');}?>>Fixo (R$)</option>
                    <option value="2" <?php if($this->viewItems['coupon']->offType == 2){echo('selected');}?>>Porcentagem (%)</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="off">Valor</label>
                <input type="number" name="off" class="form-control" value="<?php echo($this->viewItems['coupon']->off);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="min-value">Valor Mínimo</label>
                <input type="number" name="min-value" class="form-control" value="<?php echo($this->viewItems['coupon']->minValue);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="validity">Validade</label>
                <input type="date" name="validity" class="form-control" value="<?php echo(date("Y-m-d", $this->viewItems['coupon']->validity));?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar Alterações" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>