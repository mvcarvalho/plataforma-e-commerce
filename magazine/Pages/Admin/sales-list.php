<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Vendas</h3>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="formulario" method="post" action="<?php echo(SITE_URL . 'adm/sales/list');?>">
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Data Inicial</label>
                    <input type="date" name="startdate" class="form-control" value="<?php echo(date('Y-m-d', strtotime('-30 days'))) ?>">
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Data Final</label>
                    <input type="date" name="enddate" class="form-control" value="<?php echo(date('Y-m-d'));?>">
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <option value="-1">Todos</option>
                        <option value="0">Não Confirmados</option>
                        <option value="1">Pagamento Pendente</option>
                        <option value="2">Falha no Pagamento</option>
                        <option value="3">Pagamento Completo</option>
                        <option value="4">Produto Enviado</option>
                        <option value="5">Aguardando Confirmação</option>
                        <option value="6">Pagamento Expirado (Cancelado)</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-1 col-md-3 col-sm-6 col-xs-12" style="padding-top: 25px;">
                <input type="submit" class="btn btn-success" value="Filtrar">
            </div>
			<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 pull-right">
                <div class="form-group">
                    <label>Ordenar Por</label>
                    <select class="form-control" name="orderBy" onchange="ordenacao(this)">
                        <option value="-1">Selecione</option>
                        <option value="date">Data</option>
                        <option value="name">Cliente</option>
                        <option value="value_desc">Maior Valor</option>
                        <option value="value_asc">Menor Valor</option>
                    </select>
                </div>
            </div>
        </form>

    </div>
    <div class="row" style="padding-top: 10px">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >ID</th>
                <th >Data</th>
                <th >Cliente</th>
                <th >Valor</th>
                <th >Status</th>
                <th >Rastreio</th>
                <th >Endereço Entrega</th>
                <th style="text-align: center;">Ações</th>
            </tr>
            <?php foreach($this->viewItems['sales'] as $sale){?>
                <tr>
                    <td style="font-size: 12px"><?php echo($sale->id);?></td>
                    <td style="font-size: 12px"><?php echo(date('d M Y', strtotime($sale->date)));?></td>
                    <td style="font-size: 12px"><?php echo($sale->user->name);?></td>
                    <td style="font-size: 12px">R$ <?php echo(number_format($sale->value, 2, ',', '.'));?></td>
                    <td style="font-size: 12px"><?php
                        if($sale->status == 6){
                            echo('Pagamento Expirado (Cancelado)');

                        }else if($sale->status == 5){
                            echo('Aguardando Confirmação');

                        } else if($sale->status == 4){
                            echo('Produto enviado');

                        } else if($sale->status == 3){
                            echo('Pagamento completo');

                        } else if($sale->status == 2){
                            echo('Falha no pagamento');

                        } else if($sale->status == 1){
                            echo('Pagamento pendente');

                        } else if($sale->status == 0){
                            echo('Não confirmado');
                        }
                        ?></td>
                    <td style="font-size: 12px"><?php echo($sale->trackingCode);?></td>
                    <td style="font-size: 12px"><?php echo(
                            $sale->user->address . ', ' .
                            $sale->user->city. ' - ' .
                            $sale->user->state . ', CEP ' .
                            $sale->user->cep);?></td>
                    <td style="text-align: center; font-size: 12px"><a href="<?php echo($this->viewItems['viewLink'] . $sale->id)?>">[View]</a></td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>