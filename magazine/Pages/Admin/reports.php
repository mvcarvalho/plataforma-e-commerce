<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Relatório Gerado</h3>

    <?php if(isset($this->viewItems['reportLink'])){?>
        <a target="_blank" href="<?php echo($this->viewItems['reportLink']);?>" class="btn btn-success" onclick="onClickDownload();">Baixar Relatório Gerado</a>
    <?php }else{?>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form method="post" action="<?php echo(SITE_URL . 'adm/reports/salesbydate');?>">
                <h4>Vendas por período</h4>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label>Data Inicial</label>
                        <input type="date" name="startdate" class="form-control">
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label>Data Final</label>
                        <input type="date" name="enddate" class="form-control">
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status">
                            <option value="-1">Todos</option>
                            <option value="0">Não Confirmados</option>
                            <option value="1">Pagamento Pendente</option>
                            <option value="2">Falha no Pagamento</option>
                            <option value="3">Pagamento Completo</option>
                            <option value="4">Produto Enviado</option>
                            <option value="5">Aguardando Confirmação</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-top: 25px;">
                    <input type="submit" class="btn btn-success" value="Gerar Relatório">
                </div>
            </form>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form method="post" action="<?php echo(SITE_URL . 'adm/reports/usersregistered');?>">
                <h4>Usuários Cadastrados</h4>
                <input type="submit" class="btn btn-success" value="Gerar Relatório">
            </form>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding-top: 25px">
            <form method="post" action="<?php echo(SITE_URL . 'adm/reports/exportnews');?>">
                <h4>E-mails Newletters</h4>
                <h5><b>Selecione as Categorias</b></h5>
                    <?php
                        include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');

                        $manager = new CategoriesManager();

                        $categories = $manager->getAllCategories();

                        foreach($categories as $c) {

                            echo "<div class='checkbox'>";
                            echo " <label><input type='checkbox' name='CAT_".$c->id."' value='".$c->id."'>".$c->name."</label>";
                            echo "</div>";
                        }
                    ?>
                <input type="submit" class="btn btn-success" value="Gerar">
            </form>
        </div>
    <?php }?>

</div>
<script type="text/javascript">
    function onClickDownload(){
        window.location = '<?php echo(SITE_URL . 'adm/reports');?>';
    }
</script>