<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Produto</h3>
    <form rule="form" method="post" enctype="multipart/form-data" action="<?php echo $this->viewItems['formaction'];?>">
        <input type="hidden" name="id" value="<?php echo($this->viewItems['product']->id);?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" name="name" class="form-control" value="<?php echo($this->viewItems['product']->name);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="shortdescription">Descrição Curta</label>
                <input type="text" name="shortdescription" class="form-control" value="<?php echo($this->viewItems['product']->shortDescription);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="promotionaltext">Texto Promocional</label>
                <input type="text" name="promotionaltext" class="form-control" value="<?php echo($this->viewItems['product']->shortDescription);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="description">Descrição</label>
                <textarea id="description" name="description" class="ckeditor" style="min-height: 200px"><?php echo($this->viewItems['product']->description);?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="tecnicaldata">Dados Técnicos</label>
                <textarea id="tecnicaldata" name="tecnicaldata" class="ckeditor" style="min-height: 200px"><?php echo($this->viewItems['product']->description);?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="includeditems">Itens inclusos</label>
                <textarea id="includeditems" name="includeditems" class="ckeditor" style="min-height: 200px"><?php echo($this->viewItems['product']->description);?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="rules">Informações Adicionais</label>
                <textarea id="rules" name="rules" class="ckeditor" style="min-height: 100px"><?php echo($this->viewItems['product']->rules);?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="normalprice">Preço Normal</label>
                <input type="text" name="normalprice" class="form-control" value="<?php echo($this->viewItems['product']->normalPrice);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="offerprice">Preço Oferta</label>
                <input type="text" name="offerprice" class="form-control" value="<?php echo($this->viewItems['product']->offerPrice);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="delivery_cost">Custo com Frete</label>
                <input type="text" name="delivery_cost" class="form-control" value="<?php echo($this->viewItems['product']->deliveryCost);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="delivery_days">Dias para entrega</label>
                <input type="text" name="delivery_days" class="form-control" value="<?php echo($this->viewItems['product']->deliveryDays);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="validity">Validade</label>
                <input type="date" name="validity" class="form-control" value="<?php echo($this->viewItems['product']->validity);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="image">Imagem</label>
                <input type="file" name="image" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="category">Categoria</label>
                <select name="category" class="form-control">
                    <?php foreach($this->viewItems['categories'] as $category){?>
                        <?php if($this->viewItems['product']->categoryId == $category->id){?>
                            <option selected value="<?php echo($category->id); ?>"><?php echo($category->name); ?></option>
                        <?php }else{?>
                            <option value="<?php echo($category->id); ?>"><?php echo($category->name); ?></option>
                        <?php }?>
                    <?php }?>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Características</h3>
            <div class="row" style="padding: 10px;">
                <input type="hidden" name="feature-1-id" value="<?php echo($this->viewItems['features'][0]->id);?>">
                <input type="text" name="feature-1" class="form-control" value="<?php echo($this->viewItems['features'][0]->name);?>" placeholder="Nome da característica">
                <textarea class="form-control" name="feature-1-options" placeholder="Características em cada linha" style="resize: vertical; min-height: 100px"><?php echo($this->viewItems['features'][0]->optionsText);?></textarea>
            </div>
            <div class="row" style="padding: 10px;">
                <input type="hidden" name="feature-2-id" value="<?php echo($this->viewItems['features'][1]->id);?>">
                <input type="text" name="feature-2" class="form-control" value="<?php echo($this->viewItems['features'][1]->name);?>" placeholder="Nome da característica">
                <textarea class="form-control" name="feature-2-options" placeholder="Características em cada linha" style="resize: vertical; min-height: 100px"><?php echo($this->viewItems['features'][1]->optionsText);?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px">
            <div class="form-group">
                <input type="submit" value="Salvar Alterações" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 25px">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Adicionar Informação Rápida</h3>
        <form role="form" method="post" action="<?php echo $this->viewItems['formaction-add-info'];?>">
            <input type="hidden" name="productid" value="<?php echo($this->viewItems['product']->id);?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="name">Informação</label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="description">Complemento</label>
                    <input type="text" id="description" name="description" class="form-control">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Adicionar" class="btn btn-danger">
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Lista de Informações Rápidas</h3>
        <div class="row" style="padding-top: 10px">
            <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
                <tr>
                    <th >Informação</th>
                    <th >Complemento</th>
                    <th style="text-align: center;">Remover</th>
                </tr>
                <?php foreach($this->viewItems['infos'] as $info){?>
                    <tr>
                        <td><?php echo($info->name);?></td>
                        <td><?php echo($info->description);?></td>
                        <td style="text-align: center;"><a href="<?php echo $this->viewItems['formaction-remove-info'] . $info->id;?>">[x]</a></td>
                    </tr>
                <?php }?>
            </table>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 25px">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Adicionar Attributo</h3>
        <form rule="form" method="post" action="<?php echo $this->viewItems['formaction-add'];?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="attribute">Atributo</label>
                    <select id="attribute" name="attribute" class="form-control">
                        <?php foreach($this->viewItems['attributes'] as $attribute){?>
                            <option value="<?php echo($attribute->id);?>"><?php echo($attribute->name . ' - ' . $attribute->observation);?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <input type="hidden" name="productid" value="<?php echo($this->viewItems['product']->id);?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="submit" value="Adicionar" class="btn btn-danger">
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Lista de Opções do atributo</h3>
        <div class="row" style="padding-top: 10px">
            <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
                <tr>
                    <th >Nome</th>
                    <th >Observação Interna</th>
                    <th style="text-align: center;">Remover</th>
                </tr>
                <?php foreach($this->viewItems['product']->attributes as $attribute){?>
                    <tr>
                        <td><?php echo($attribute->name);?></td>
                        <td><?php echo($attribute->observation);?></td>
                        <td style="text-align: center;"><a href="<?php echo $this->viewItems['formaction-remove'] . $attribute->id;?>">[x]</a></td>
                    </tr>
                <?php }?>
            </table>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 25px 10px">
    <?php $imgCount = 0;?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3>Imagens Adicionais</h3>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px">
            <?php foreach($this->viewItems['product']->extraImages as $img){?>
                <?php $imgCount++;?>
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="padding-top: 10px">
                    <img src="<?php echo(SITE_URL . 'Images/Products/' . $img);?>" class="img-responsive">
                    <a href="<?php
					$imgExplode = explode(".", $img);
					echo(SITE_URL . 'adm/product/remove-img/' .$imgExplode[0]);
					?>">[remover]</a>
                </div>
            <?php }?>
        </div>
    </div>
    <?php if($imgCount < 3){?>
        <form rule="form" method="post" enctype="multipart/form-data" action="<?php echo $this->viewItems['formaction-add-img'];?>">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px">
                <input type="hidden" name="prodId" value="<?php echo($this->viewItems['product']->id);?>">
                <input type="file" name="image">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px">
                <div class="form-group">
                    <input type="submit" value="Adicionar Imagem" class="btn btn-danger">
                </div>
            </div>
        </form>
    <?php }else{?>
        <p>São permitidas somente 3 imagens extras por produto.</p>
    <?php }?>
</div>

<script src="//cdn.ckeditor.com/4.4.5.1/standard/ckeditor.js"></script>