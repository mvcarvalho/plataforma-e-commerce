<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Adicionar Usuário do Sistema</h3>
    <form rule="form" method="post" action="<?php echo(SITE_URL . 'adm/users-adm/add');?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Nome completo</label>
                <input type="text" id="name" name="name" class="form-control" required>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="login">Login de acesso</label>
                <input type="text" id="login" name="login" class="form-control" required>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="password">Senha de acesso</label>
                <input type="password" id="password" name="password" class="form-control" required>
            </div>
        </div>
        <input type="hidden" value="1" name="is-active" id="is-active">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Adicionar" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>