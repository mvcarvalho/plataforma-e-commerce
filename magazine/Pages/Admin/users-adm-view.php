<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Usuário do Sistema</h3>
    <form rule="form" method="post" action="<?php echo(SITE_URL . 'adm/users-adm/view/' . $this->viewItems['user']->id);?>" autocomplete="off">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Nome completo</label>
                <input type="text" id="name" name="name" class="form-control" value="<?php echo($this->viewItems['user']->name);?>" required>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="login">Login de acesso</label>
                <input type="text" id="login" name="login" class="form-control" value="<?php echo($this->viewItems['user']->login);?>" required>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="password">Senha de acesso</label>
                <input type="password" id="password" name="password" class="form-control" value="<?php echo($this->viewItems['user']->password);?>" required>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="status">Status:</label>
                <select name="status" id="status" class="form-control" required>
                    <option value="0" <?php if($this->viewItems['user']->isActive == 0){echo("selected");}?>>Desativado</option>
                    <option value="1" <?php if($this->viewItems['user']->isActive == 1){echo("selected");}?>>Ativo</option>
                </select>
            </div>
        </div>

        <input type="hidden" value="<?php echo($this->viewItems['user']->id);?>" name="id" id="id">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>