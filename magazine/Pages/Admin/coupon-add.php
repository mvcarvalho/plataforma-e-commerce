<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Cupom</h3>
    <form rule="form" method="post" enctype="multipart/form-data" action="<?php echo $this->viewItems['formaction'];?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Código</label>
                <input type="text" name="key" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="off-type">Tipo de desconto</label>
                <select name="off-type" class="form-control">
                    <option value="1">Fixo (R$)</option>
                    <option value="2">Porcentagem (%)</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="off">Valor</label>
                <input type="number" name="off" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="min-value">Valor Mínimo</label>
                <input type="number" name="min-value" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="validity">Validade</label>
                <input type="date" name="validity" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Adicionar" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>