<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Categorias</h3>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-top: 10px; padding-bottom: 10px">
        <a href="<?php echo(SITE_URL . 'adm/category/add');?>" class="btn btn-info pull-right">Adicionar</a>
    </div>
    <div class="row" style="padding-top: 10px">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >Nome</th>
                <th >Descrição</th>
                <th >Ações</th>
            </tr>
            <?php
            include_once(SITE_FOLDER . 'Managers/PartsManager.php');
            PartsManager::printCategoriesTableAdmin($this->viewItems['categories']);
            ?>
        </table>
    </div>
</div>