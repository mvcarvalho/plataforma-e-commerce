<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Opção de Atributo</h3>
    <form method="post" action="<?php echo $this->viewItems['formaction-edit'];?>">
        <input type="hidden" name="id" value="<?php echo($this->viewItems['option']->id);?>">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="value">Nome</label>
                <input type="text" name="name" class="form-control" value="<?php echo($this->viewItems['option']->name);?>">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="value">Alteração no preço</label>
                <input type="text" name="pricechange" class="form-control" value="<?php echo($this->viewItems['option']->priceChange);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="value">Observação</label>
                <input type="text" name="observation" class="form-control" value="<?php echo($this->viewItems['option']->observation);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>