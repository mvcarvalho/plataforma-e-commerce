<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Produto</h3>
    <form rule="form" method="post" enctype="multipart/form-data" action="<?php echo $this->viewItems['formaction'];?>">
        <input type="hidden" name="id" value="0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" name="name" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="shortdescription">Descrição Curta</label>
                <input type="text" name="shortdescription" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="promotionaltext">Texto Promocional</label>
                <input type="text" name="promotionaltext" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="description">Descrição</label>
                <textarea id="description" name="description" class="ckeditor" style="min-height: 200px"></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="tecnicaldata">Dados Técnicos</label>
                <textarea id="tecnicaldata" name="tecnicaldata" class="ckeditor" style="min-height: 200px"></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="includeditems">Itens inclusos</label>
                <textarea id="includeditems" name="includeditems" class="ckeditor" style="min-height: 200px"></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="rules">Informações Adicionais</label>
                <textarea id="rules" name="rules" class="ckeditor" style="min-height: 100px"></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="normalprice">Preço Normal</label>
                <input type="text" name="normalprice" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="offerprice">Preço Oferta</label>
                <input type="text" name="offerprice" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="delivery_cost">Custo com Frete</label>
                <input type="text" name="delivery_cost" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="delivery_days">Dias para entrega</label>
                <input type="text" name="delivery_days" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="validity">Validade</label>
                <input type="date" name="validity" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="image">Imagem</label>
                <input type="file" name="image" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="category">Categoria</label>
                <select name="category" class="form-control">
                    <?php foreach($this->viewItems['categories'] as $category){?>
                        <option value="<?php echo($category->id); ?>"><?php echo($category->name); ?></option>
                    <?php }?>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Características</h3>
            <div class="row" style="padding: 10px;">
                <input type="hidden" name="feature-1-id" value="0">
                <input type="text" name="feature-1" class="form-control" placeholder="Nome da característica">
                <textarea class="form-control" name="feature-1-options" placeholder="Características em cada linha" style="resize: vertical; min-height: 100px"></textarea>
            </div>
            <div class="row" style="padding: 10px;">
                <input type="hidden" name="feature-2-id" value="0">
                <input type="text" name="feature-2" class="form-control" placeholder="Nome da característica">
                <textarea class="form-control" name="feature-2-options" placeholder="Características em cada linha" style="resize: vertical; min-height: 100px"></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar Novo Produto" class="btn btn-danger">
            </div>
        </div>
    </form>
</div>
<script src="//cdn.ckeditor.com/4.4.5.1/standard/ckeditor.js"></script>