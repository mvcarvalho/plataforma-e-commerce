<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <h3>Cupons</h3>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-top: 10px; padding-bottom: 10px">
        <a href="<?php echo(SITE_URL . 'adm/coupon/add');?>" class="btn btn-info pull-right">Adicionar</a>
    </div>
    <div class="row" style="padding-top: 10px">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >Código</th>
                <th >Tipo de desconto</th>
                <th >Valor</th>
                <th >Valor Mínimo</th>
                <th >Validade</th>
                <th >Ações</th>
            </tr>
            <?php foreach($this->viewItems['coupons'] as $coupon){?>
                <tr>
                    <td ><?php echo($coupon->key);?></td>
                    <td ><?php if($coupon->offType == 1){echo('Fixo (R$)');}else if($coupon->offType == 2){echo('Procentagem (%)');}?></td>
                    <td ><?php echo($coupon->off);?></td>
                    <td ><?php echo($coupon->minValue);?></td>
                    <td ><?php echo(date('d/m/y',$coupon->validity));?></td>
                    <td ><a href="<?php echo(SITE_URL . 'adm/coupon/view/' . $coupon->id);?>">[view]</a></td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>