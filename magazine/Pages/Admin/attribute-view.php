<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Atributo</h3>
    <form rule="form" method="post" action="<?php echo $this->viewItems['formaction'];?>">
        <input type="hidden" name="id" value="<?php echo($this->viewItems['attribute']->id);?>">
        <input type="hidden" name="isrequired" value="0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" id="name" name="name" class="form-control" value="<?php echo($this->viewItems['attribute']->name);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="observation">Observação Interna</label>
                <input type="text" id="observation" name="observation" class="form-control" value="<?php echo($this->viewItems['attribute']->observation);?>">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Salvar Alterações" class="btn btn-danger">
            </div>
        </div>
    </form>
    <br><br>
    <h3>Adicionar Opção</h3>
    <form method="post" action="<?php echo $this->viewItems['formaction-add'];?>">
        <input type="hidden" name="id" value="0">
        <input type="hidden" name="attributeid" value="<?php echo($this->viewItems['attribute']->id);?>">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="value">Nome</label>
                <input type="text" name="name" class="form-control">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label for="value">Alteração no preço</label>
                <input type="text" name="pricechange" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label for="value">Observação</label>
                <input type="text" name="observation" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <input type="submit" value="Adicionar" class="btn btn-danger">
            </div>
        </div>
    </form>
    <br><br>
    <h3>Lista de Opções do atributo</h3>
    <div class="row" style="padding-top: 10px">
        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th >Nome</th>
                <th >Observação</th>
                <th >Alteração no Valor</th>
                <th style="text-align: center;">Remover</th>
            </tr>
            <?php foreach($this->viewItems['attribute']->options as $option){?>
                <tr>
                    <td><?php echo($option->name);?></td>
                    <td><?php echo($option->observation);?></td>
                    <td><?php echo('R$ ' . $option->priceChange);?></td>
                    <td style="text-align: center;">
                        <a href="<?php echo $this->viewItems['formaction-remove'] . $option->id;?>">[x]</a>
                        <a href="<?php echo $this->viewItems['formaction-view'] . $option->id;?>">[edit]</a>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div>
</div>