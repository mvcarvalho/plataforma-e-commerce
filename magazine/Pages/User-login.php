<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px">
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 form">
        <div class="title">Já tenho cadastro:</div>
        <form role="form" method="post">
            <div class="form-group">
                <label for="emaillogin">E-mail</label>
                <input class="form-control" type="text" name="email" id="emaillogin" placeholder="seuemail@exemplo.com.br" required>
            </div>
            <div class="form-group">
                <label for="password">Senha</label>
                <input class="form-control" type="password" name="password" id="password" required>
            </div>
            <?php if(isset($this->pageItems['error'])){?>
                <div class="alert alert-warning" role="alert"><?php echo($this->pageItems['error']);?></div>
            <?php }?>
            <button type="submit" class="btn btn-danger" name="login">Continuar</button>
            <a href="<?php echo(SITE_URL . 'user/reset-email');?>">Esqueci minha senha</a>
        </form>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 form">
        <div class="title">Quero me cadastrar:</div>
        <form role="form" method="post" action="<?php echo(SITE_URL . 'user' . DS . 'register');?>">
            <div class="form-group">
                <label for="emailLogin">E-mail</label>
                <input class="form-control" type="text" name="email" id="emailLogin" placeholder="seuemail@exemplo.com.br" required>
            </div>
            <button type="submit" class="btn btn-danger" name="login">Cadastrar</button>
        </form>
    </div>
</div>