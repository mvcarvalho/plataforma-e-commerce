<?php
$productLink = SITE_URL . 'produto/' . $this->pageItems['product']->id . '/' . strtolower(StringUtils::clean($this->pageItems['product']->name));
$shareName = SITE_NAME . ' - ' . $this->pageItems['product']->name;
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-view">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="top-name"><?php echo($this->pageItems['product']->name);?></h4>
            <div class="fb-like" data-href="<?php echo($productLink);?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
        </div>
        <!-- IMAGENS -->
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 images">
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 product-thumbs" style="padding: 5px 0;">
                <?php if($this->pageItems['product']->image != ""){?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3" style="padding: 1px;">
                        <a href="<?php echo(PRODUCTS_IMAGES_URL . $this->pageItems['product']->image);?>"><img class="img-responsive thumbs" src="<?php echo(PRODUCTS_IMAGES_URL . $this->pageItems['product']->image);?>" /></a>
                    </div>
                <?php }?>
                <?php foreach($this->pageItems['product']->extraImages as $img){?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3" style="padding: 1px;">
                        <a href="<?php echo(PRODUCTS_IMAGES_URL . $img);?>"><img class="img-responsive thumbs" src="<?php echo(PRODUCTS_IMAGES_URL . $img);?>" /></a>
                    </div>
                <?php }?>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12" style="padding: 0;">
                <img id="showingImg" src="<?php
                if($this->pageItems['product']->image == "" || empty($this->pageItems['product']->image)){
                    echo(PRODUCTS_IMAGES_URL . 'no-image.png');
                }else{
                    echo(PRODUCTS_IMAGES_URL . $this->pageItems['product']->image);
                }
                ?>" class="img-responsive showing" alt="<?php echo($this->pageItems['product']->name);?>">
            </div>
        </div>

        <!-- CONFIGURAÇÕES E INFOS RÁPIDAS -->
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 configurations">
            <form method="post" action="<?php echo(SITE_URL . 'cart' . DS . 'add' . DS . $this->pageItems['product']->id . DS . '1');?>" id="form-add-to-cart">
                <div class="row">
                    <!-- NOME -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title"><?php echo($this->pageItems['product']->shortDescription);?></div>

                    <!-- TEXTO PROMOCIONAL -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 promotional-text"></div>

                    <!-- OPÇÃO -->
                    <?php if(count($this->pageItems['product']->attributes) > 0) {?>
                        <div class="row" style="padding-top: 25px">
                            <?php foreach($this->pageItems['product']->attributes as $attribute) {?>
                                <?php if(count($attribute->options) > 0) {?>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="input-group" style="width: 100%">
                                            <label for="select-<?php echo($attribute->id);?>"><?php echo($attribute->name);?></label>
                                            <select name="select-<?php echo($attribute->id);?>" class="form-control select" style="border-radius: 0">
                                                <option value="0" selected>Selecione</option>
                                                <?php foreach($attribute->options as $option) {?>
                                                    <option value="<?php echo($option->id);?>"><?php
                                                        if($option->priceChange > 0){
                                                            echo($option->name . ' (+R$ ' . number_format($option->priceChange, 2, ',', '.') . ')');

                                                        }else if($option->priceChange < 0){
                                                            echo($option->name . ' (-R$ ' . number_format($option->priceChange * -1, 2, ',', '.') . ')');

                                                        }else{
                                                            echo($option->name);
                                                        }
                                                        ?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                <?php }?>
                            <?php }?>
                        </div>
                    <?php }?>

                    <?php if(count($this->pageItems['features']) > 0) {?>
                        <div class="row" style="padding-top: 25px">
                            <?php foreach($this->pageItems['features'] as $feature) {?>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="input-group" style="width: 100%">
                                        <label for="feature-<?php echo($feature->id);?>"><?php echo($feature->name);?></label>
                                        <select name="feature-<?php echo($feature->id);?>" class="form-control select" style="border-radius: 0">
                                            <option value="0" selected>Selecione</option>
                                            <?php foreach($feature->options as $option) {?>
                                                <option value="<?php echo($option->name);?>"><?php echo($option->name);?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    <?php }?>

                    <!-- PREÇO -->
                    <?php if($this->pageItems['product']->normalPrice > 0){?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normal-price">de <?php echo(GeneralUtils::getFormattedValue($this->pageItems['product']->normalPrice));?></div>
                    <?php }?>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding: 0">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 price"><?php echo(GeneralUtils::getFormattedValue($this->pageItems['product']->offerPrice));?></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 installments"><?php
                            $inst = GeneralUtils::getMaxInstallments($this->pageItems['product']->offerPrice, MIN_INSTALLMENT_VALUE, MAX_INSTALLMENTS);
                            echo($inst['installments'] . 'x de ' . GeneralUtils::getFormattedValue($inst['value']) . ' no cartão');
                            ?></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding: 0">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 15px 0;">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="0;"></div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" style="0;">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($productLink);?>" onclick="share(this); return false;" target="_blank" title="<?php echo($shareName);?>">
                                    <img src="<?php echo(SITE_URL . 'Images' . DS . 'facebook.png');?>" class="img-responsive" style="display: inline-block; padding: 3px;">
                                </a>
                                <a href="https://twitter.com/home?status=<?php echo($productLink);?>" onclick="share(this); return false;" target="_blank" title="<?php echo($shareName);?>">
                                    <img src="<?php echo(SITE_URL . 'Images' . DS . 'twitter.png');?>" class="img-responsive" style="display: inline-block; padding: 3px;">
                                </a>
                                <a href="https://plus.google.com/share?url=<?php echo($productLink);?>" onclick="share(this); return false;" target="_blank" title="<?php echo($shareName);?>">
                                    <img src="<?php echo(SITE_URL . 'Images' . DS . 'google-plus.png');?>" class="img-responsive" style="display: inline-block; padding: 3px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- COMPRAR -->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left: 0">
                        <?php if($this->pageItems['product']->isActive == 1){?>
                            <input type="submit" class="btn buy" value="comprar" onclick="" style="width: 100%">
                        <?php }else{?>
                            <div class="buy uppercase">produto indisponível</div>
                        <?php }?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="0;"></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" style="0;">
                            <img src="<?php echo(SITE_URL . 'Images' . DS . 'rapid-ssl.png');?>" class="img-responsive" style="display: inline-block; padding: 3px;">
                        </div>
                    </div>
                </div>
            </form>

            <!-- SOCIAL - PRODUTO -->
            <?php if(false){?>
                <div class="row">
                    <div class="col-lg-3 col-md-2 col-sm-1 col-xs-12"></div>

                </div>
            <?php }?>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="tab active"><a href="#pane1" data-toggle="tab">Descrição do produto</a></li>
                    <li class="tab"><a href="#pane2" data-toggle="tab">Informações adicionais</a></li>
                </ul>
                <div class="tab-content">
                    <div id="pane1" class="tab-pane active">
                        <div class="description-content"><?php echo($this->pageItems['product']->description);?></div>
                    </div>
                    <div id="pane2" class="tab-pane">
                        <div class="description-content">
                            <?php echo($this->pageItems['product']->rules);?>

                            <!-- INFORMAÇÕES RÁPIDAS -->
                            <?php if(count($this->pageItems['infos']) > 0){?>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
                                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-10"><h3 style="text-align: center">Informações Rápidas</h3></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                                    <div class="col-lg-3 col-md-3 col-sm-2 col-xs-1"></div>
                                    <table class="col-lg-6 col-md-6 col-sm-8 col-xs-10">
                                        <?php foreach($this->pageItems['infos'] as $info) {?>
                                            <tr>
                                                <td style="border: 1px solid #000000; padding: 5px; text-align: center; font-weight: bold;" class="info"><?php echo($info->name);?></td>
                                                <td style="border: 1px solid #000000; padding: 5px; text-align: center;" class="info"><?php echo($info->description);?></td>
                                            </tr>
                                        <?php }?>
                                    </table>
                                </div>
                            <?php }?>
                        </div>

                    </div>

                </div><!-- /.tab-content -->
            </div><!-- /.tabbable -->
        </div>

    </div>
    <!-- DESCRIÇÃO E REGRAS -->

</div>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    var selects = document.getElementsByClassName("select");
    for(var x = 0; x < selects.length; x++){
        //selects[x].selectedIndex = 0;
    }

    $(document).ready(function(){
        $(".product-view .images .product-thumbs a").click(function(){
            var largePath = $(this).attr("href");
            var largeAlt = $(this).attr("title");
            $("#showingImg").attr({ src: largePath, alt: largeAlt });
            return false;
        });
    });

    $("#form-add-to-cart").submit(function( event ) {
        event.preventDefault();

        var allSelected = true;
        var selects = document.getElementsByClassName("select");
        for(var x = 0; x < selects.length; x++){
            if(selects[x].selectedIndex == 0){
                allSelected = false;
            }
        }

        if(allSelected){
            $("#form-add-to-cart").get(0).submit();
        }else{
            alert("Selecione todas as opções do produto.");
        }
    });

    function share(button){
        window.open(button.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=500');
    }
</script>