<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12"></div>
<div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
    <h2>Política de Segurança</h2>
    <p style="font-size: 13px; color: #555555">Política de Segurança de Dados</p>
    <br><p style="font-size: 13px; color: #555555">A Política de Segurança de dados da <?php echo(SITE_NAME)?> estabelece o compromisso com a segurança e a confidencialidade das informações que são coletadas dos seus usuários.</p>
    <br><br><p style="font-size: 13px; color: #555555">Segurança</p>
    <p style="font-size: 13px; color: #555555">O site da <?php echo(SITE_NAME)?> foi desenvolvido para manter total segurança de seus dados e processos.</p>
    <p style="font-size: 13px; color: #555555">A fim de comprovar tal segurança, foi análisado pelos principais meios certificadores mundiais. Comprove você mesmo:</p><br>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 10px">
        <a href="https://safeweb.norton.com/report/show?url=<?php echo(SITE_URL_SMALL)?>" target="_blank">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: center">
                <img src="<?php echo(SITE_URL)?>Images/norton_safe_web.png" class="img-responsive" style="display: block; margin: 0 auto">
            </div>
        </a>
        <a href="https://www.google.com/safebrowsing/diagnostic?site=<?php echo(SITE_URL_SMALL)?>" target="_blank">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: center">
                <img src="<?php echo(SITE_URL)?>Images/google_safe_browsing.png" class="img-responsive" style="display: block; margin: 0 auto">
            </div>
        </a>
        <a href="http://www.urlvoid.com/scan/<?php echo(SITE_URL_SMALL)?>/" target="_blank">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="text-align: center">
                <img src="<?php echo(SITE_URL)?>Images/urlvoid-logo.png" class="img-responsive" style="display: block; margin: 0 auto">
            </div>
        </a>
    </div>
</div>
<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12"></div>