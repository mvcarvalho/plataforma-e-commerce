<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Histórico do ticket #<?php echo($this->viewItems['ticket']->id);?> - <?php
        if($this->viewItems['ticket']->status == 2){
            echo('Resolvido');

        } else if($this->viewItems['ticket']->status == 1){
            echo('Em análise');

        } else if($this->viewItems['ticket']->status == 0){
            echo('Recebido');
        }
        ?></h3>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h4>Assunto: <?php echo($this->viewItems['ticket']->subject);?></h4>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 10px">
    <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
        <tr>
            <th>Data</th>
            <th>Mensagems</th>
            <th>Autor</th>
        </tr>

        <?php foreach($this->viewItems['ticket']->messages as $message){?>
        <tr>
            <td style="font-size: 12px; width: 15%"><?php echo(date('d M Y', strtotime($message->date)));?></td>
            <td style="font-size: 12px; width: 60%"><?php echo($message->message);?></td>
            <td style="font-size: 12px; width: 25%"><?php
                if($message->isFromUser){
                    echo('Cliente: ' . $this->viewItems['user']->name);
                }else{
                    echo('Equipe ' . SITE_NAME);
                }
                ?></td>
        </tr>
        <?php }?>
    </table>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Responder ao cliente</h3>
    <form role="form" method="post" style="padding-bottom: 20px">
        <div class="form-group">
            <label for="status">Status</label>
            <select class="form-control" name="status" id="status">
                <?php if($this->viewItems['ticket']->status != 2){?>
                    <option value="1">Em análise</option>
                    <option value="2">Resolvido</option>
                <?php }else{?>
                    <option value="2">Resolvido</option>
                <?php }?>
            </select>
        </div>
        <div class="form-group">
            <label for="message">Mensagem</label>
            <textarea class="form-control" name="message" id="message" required style="resize: vertical; height: 100px" placeholder="Mensagem para o cliente..."></textarea>
        </div>
        <button type="submit" class="btn btn-success" name="send">Responder</button>
    </form>
</div>