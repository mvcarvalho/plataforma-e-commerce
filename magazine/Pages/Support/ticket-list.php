<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h3>Tickets Cadastrados</h3>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 20px">
    <?php if(count($this->viewItems['tickets']) <= 0){?>
        <h4>Não existem tickets abertos.</h4>
    <?php }else{?>

        <table class="table table-hover" style="background-color: #ffffff; padding: 20px;">
            <tr>
                <th>ID</th>
                <th>Número do Pedido</th>
                <th>Data</th>
                <th>Status</th>
                <th>Assunto</th>
                <th style="text-align: center;">Ações</th>
            </tr>

            <?php foreach($this->viewItems['tickets'] as $ticket){?>
            <tr>
                <td style="font-size: 12px"><?php echo($ticket->id);?></td>
                <td style="font-size: 12px"><?php echo($ticket->saleId);?></td>
                <td style="font-size: 12px"><?php echo(date('d M Y', strtotime($ticket->date)));?></td>
                <td style="font-size: 12px"><?php
                    if($ticket->status == 2){
                        echo('Resolvido');

                    } else if($ticket->status == 1){
                        echo('Em análise');

                    } else if($ticket->status == 0){
                        echo('Recebido');
                    }
                    ?></td>
                <td style="font-size: 12px"><?php echo($ticket->subject);?></td>
                <td style="text-align: center; font-size: 12px"><a href="<?php echo(SITE_URL . 'support/ticket/view/' . $ticket->id)?>">[View]</a></td>
            </tr>
            <?php }?>

        </table>
    <?php }?>
</div>