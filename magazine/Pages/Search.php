<?php if(count($this->viewItems['seccondary-banners']) >= 2) {?>
    <div style="padding: 25px 0" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?//Exibe os banners secundários no topo. ?>
        <div style="overflow: hidden; padding-left: 15px; padding-bottom: 29px; padding-right: 0" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a href="<?php echo($this->viewItems['seccondary-banners'][0]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['seccondary-banners'][0]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
        </div>
        <div style="overflow: hidden; padding-left: 15px; padding-bottom: 0; padding-right: 0" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <a href="<?php echo($this->viewItems['seccondary-banners'][1]->url);?>"><img src="<?php echo(BANNERS_IMAGES_URL . $this->viewItems['seccondary-banners'][1]->image);?>" class="img-responsive" style="margin: 0 auto; padding: 0;" alt="<?php echo(SITE_NAME);?>"></a>
        </div>
    </div>
<?php }?>

<h3 class="session-name">
    <img src="<?php echo(SITE_URL . 'Images/arrow-right.png');?>" class="img-responsive" style="display: inline-block; padding-right: 7px; margin-top: -4px;">
    <?php
    $search = SessionManager::getTempAction();
    if(isset($search['search'])){
        echo('Pesquisando por: ' . $search['search']);
    }else{
        echo('Os mais acessados');
    }
    ?> :)
</h3>

<div class="row">
    <?php
    $count = 0;

    foreach($this->viewItems['products'] as $product){ ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="<?php
            if(isset($this->viewItems['category'])){
                echo(SITE_URL . 'produto' . DS . $product->id . '/' . strtolower(StringUtils::clean($this->viewItems['category']->name)) . '/' . strtolower(StringUtils::clean($product->name)));
            }else{
                echo(SITE_URL . 'produto' . DS . $product->id . '/' . strtolower(StringUtils::clean($product->name)));
            }
            ?>">
                <div class="product-grid transition">

                    <img src="<?php
                    if($product->image == "" || empty($product->image)){
                        echo(PRODUCTS_IMAGES_URL . 'no-image.png');
                    }else{
                        echo(PRODUCTS_IMAGES_URL . $product->image);
                    }
                    ?>" class="img-responsive" alt="<?php echo($product->name);?>">

                    <div class="product-name"><?php echo($product->name);?></div>

                    <?php if($product->normalPrice > 0){?>
                        <div class="product-normal-price">de <?php echo(GeneralUtils::getFormattedValue($product->normalPrice));?> por</div>
                        <div class="product-offer-price"><?php echo(GeneralUtils::getFormattedValue($product->offerPrice));?></div>
                    <?php }else{?>
                        <div class="product-offer-price"><?php echo(GeneralUtils::getFormattedValue($product->offerPrice));?></div>
                        <div class="product-normal-price">ou <?php
                            $inst = GeneralUtils::getMaxInstallments($product->offerPrice, MIN_INSTALLMENT_VALUE, MAX_INSTALLMENTS);
                            echo($inst['installments'] . 'x de ' . GeneralUtils::getFormattedValue($inst['value']) . ' no cartão');
                            ?></div>
                    <?php }?>
                </div>
            </a>
        </div>

        <?php
        $count ++;
        if($count == 4){
            echo('</div><div class="row">');
            $count = 0;
        }
    }?>
</div>