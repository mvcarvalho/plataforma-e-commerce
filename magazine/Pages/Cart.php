<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cart">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title">
        <?php if (count($this->pageItems['cart']->items) > 0) { ?>
            Carrinho de compras
        <?php } else { ?>
            Carrinho de compras vazio
        <?php } ?>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cart-list">
        <?php foreach ($this->pageItems['cart']->items as $item) { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item">

                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" style="padding: 0">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 image"><img src="<?php
                        if ($item->product->image == "" || empty($item->product->image)) {
                            echo(PRODUCTS_IMAGES_URL . 'no-image.png');
                        } else {
                            echo(PRODUCTS_IMAGES_URL . $item->product->image);
                        }
                        ?>" class="img-responsive"></div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 name"><?php
                        echo($item->product->name);
                        if (strlen($item->optionsText) > 0) {
                            echo("<br>" . $item->optionsText);
                        }?><br>
                        <a href="<?php echo(SITE_URL . 'cart' . DS . 'remove' . DS . $item->id); ?>">
                            <div style="padding-top: 5px;">Remover do Carrinho</div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="padding: 0">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 info">
                        <div class="input-group" style="max-width: 125px; margin: 0 auto; border-radius: 0">
                            <?php if ($item->quantity > 1) { ?>
                                <span class="input-group-btn"><a
                                        href="<?php echo(SITE_URL . 'cart' . DS . 'remove-quantity' . DS . $item->id); ?>">
                                        <button class="btn buy" type="button"
                                                style="padding: 0; height: 32px; font-size: 21px !important; min-width: 30px;">
                                            &nbsp;-&nbsp;</button>
                                    </a></span>
                            <?php } ?>
                            <div class="form-control" style="text-align: center"><?php echo($item->quantity); ?></div>
                            <span class="input-group-btn"><a
                                    href="<?php echo(SITE_URL . 'cart' . DS . 'add-quantity' . DS . $item->id); ?>">
                                    <button class="btn buy" type="button"
                                            style="padding: 0; height: 32px; font-size: 21px !important; min-width: 30px;">
                                        &nbsp;+&nbsp;</button>
                                </a></span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 info">
                        <!--<div style="text-align: center; padding-top: 27px; text-decoration: line-through; color: #777;">de R$ 1499,99</div>-->
                        <div style="text-align: center; font-weight: bold; padding-top: 8px; font-size: 14px">por
                            R$ <?php echo(number_format($item->price, 2, ',', '.')); ?></div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 info">
                        <div style="text-align: center; padding-top: 8px; font-size: 14px;">
                            R$ <?php echo(number_format($item->price * $item->quantity, 2, ',', '.')); ?></div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if (count($this->pageItems['cart']->items) == 0) { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px;">
                <a href="<?php echo(SITE_URL); ?>">
                    <button class="btn btn-danger">Adicionar Itens</button>
                </a>
            </div>
        <?php } ?>
    </div>
    <?php if (count($this->pageItems['cart']->items) > 0) { ?>

        <?php if (!isset($this->pageItems['coupon'])) { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 total" style="margin-bottom: 20px;">
            <?php if (isset($this->pageItems['coupon_msg'])) { ?>
                <p><?php echo($this->pageItems['coupon_msg']); ?></p>
            <?php } ?>
                <form role="form" action="<?php echo(SITE_URL . 'cart' . DS . 'set-coupon'); ?>" method="post">
                    <button type="submit" class="btn btn-info pull-right" style="margin-left: 10px">Validar</button>
                    <input type="text" id="coupon" name="coupon" class="form-control form-inline pull-right" style="width: 200px" placeholder="Cupom de promoção">
                </form>
            </div>
        <?php }?>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 total" style="margin-bottom: 5px; text-align: right">Total
            em produtos: R$ <?php echo(number_format($this->pageItems['total'], 2, ',', '.')); ?></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 total" style="margin-bottom: 5px; text-align: right">Frete:
            R$ <?php echo(number_format($this->pageItems['deliveryCost'], 2, ',', '.')); ?></div>
        <?php if (isset($this->pageItems['coupon'])) { ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 total" style="margin-bottom: 5px; text-align: right">Desconto
            do cupom <?php echo($this->pageItems['coupon']); ?>: R$ <?php echo(number_format($this->pageItems['coupon_off'], 2, ',', '.')); ?></div>
        <?php } ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 total" style="margin-bottom: 15px; text-align: right">Total
            da compra: R$ <?php echo(number_format($this->pageItems['total'] + $this->pageItems['deliveryCost'] - $this->pageItems['coupon_off'], 2, ',', '.')); ?></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
            <a href="<?php echo(SITE_URL . 'cart' . DS . 'checkout'); ?>">
                <button class="btn buy pull-right">Finalizar Compra</button>
            </a>
        </div>
    <?php } ?>
</div>

<?php if (isset($this->pageItems['pixel']) && $this->pageItems['pixel'] == true) { ?>
<?php } ?>