<!-- HEADER -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-bar">
    <div class="container" style="padding-top: 20px; margin: 0px; width: 100%;">
        <div >
            <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <a href="<?php echo(SITE_URL)?>"><img src="<?php echo(SITE_URL)?>Images/top-logo.png" style="margin: 0 auto; width: 200px; display: block"></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
            </div>
        </div>
        <?php
        if(SessionManager::getAdminName() != ""){?>
            <ul class="menu">
                <li><a href="<?php echo(SITE_URL . 'adm/users-adm');?>"><div class="item uppercase transition">Usuários</div></a></li>
                <li><a href="<?php echo(SITE_URL . 'adm/reports');?>"><div class="item uppercase transition">Relatórios</div></a></li>
                <li><a href="<?php echo(SITE_URL . 'adm/sales');?>"><div class="item uppercase transition">Vendas</div></a></li>
                <li><a href="<?php echo(SITE_URL . 'adm/product');?>"><div class="item uppercase transition">Produtos</div></a></li>
                <li><a href="<?php echo(SITE_URL . 'adm/category');?>"><div class="item uppercase transition">Categorias</div></a></li>
                <li><a href="<?php echo(SITE_URL . 'adm/attribute');?>"><div class="item uppercase transition">Atributos</div></a></li>
                <li><a href="<?php echo(SITE_URL . 'adm/coupon');?>"><div class="item uppercase transition">Cupons</div></a></li>
                <li><a href="<?php echo(SITE_URL . 'adm/user/logout');?>"><div class="item uppercase transition">Sair</div></a></li>
            </ul>
        <?php }?>
    </div>
</div>