<!-- HEADER -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-bar">
    <div class="container" style="margin-top: 0">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-links">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding: 0">
                <h6 style="color: #005f8c; margin: 0; padding-top: 7px"><i class="glyphicon glyphicon-lock"></i> Site seguro com criptografia SSL</h6>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="padding: 0">
                <a href="#" class="transition pull-right"><img src="<?php echo(SITE_URL . "Images/")?>telefone.png" class="img-responsive" style="display: inline-block;"> 11 3181-4952</a>
                <a href="<?php echo(SITE_URL)?>page/contact" class="transition pull-right"><img src="<?php echo(SITE_URL . "Images/")?>fale_conosco.png" class="img-responsive" style="display: inline-block;">  Fale Conosco</a>
                <a href="<?php echo(SITE_URL)?>user/history" class="transition pull-right"><img src="<?php echo(SITE_URL . "Images/")?>meus_pedidos.png" class="img-responsive" style="display: inline-block;">  Meus Pedidos</a>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 20px 0;">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
                <a href="<?php echo(SITE_URL)?>"><img src="<?php echo(SITE_URL)?>Images/top-logo.png" style="padding-top: 5px;" class="img-responsive" alt="<?php echo(SITE_NAME);?>"></a>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding-top: 10px; padding-left: 60px">
                <form role="form" method="post" action="<?php echo(SITE_URL . 'pesquisa')?>">
                    <div class="form-group">
                        <input class="form-control search-field" type="text" name="search" id="search" placeholder="Digite aqui a sua busca" required value="<?php
                        $search = SessionManager::getTempAction();
                        if(isset($search['search'])){echo($search['search']);}?>">
                        <img src="<?php echo(SITE_URL)?>Images/search.png" style="position: absolute; right: 25px; top: 20px;" alt="Pesquisa">
                    </div>
                </form>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12" style="padding-top: 14px">
                <div class="header-links">
                    <a href="<?php echo(SITE_URL . 'cart')?>" class="transition pull-right" style="padding-left: 10px"><img src="<?php echo(SITE_URL)?>Images/cart.png" class="img-responsive" style="display: inline-block; padding-right: 5px" alt="Carrinho">Itens no carrinho</a>
                    <?php if(SessionManager::getUserId() > 0){?>
                        <a href="<?php echo(SITE_URL)?>user/profile" class="transition pull-right"><img src="<?php echo(SITE_URL . "Images/")?>meu_cadastro.png" class="img-responsive" style="display: inline-block;">  Meu Cadastro</a>
                    <?php }else{?>
                        <a href="<?php echo(SITE_URL)?>user/login" class="transition pull-right"><img src="<?php echo(SITE_URL . "Images/")?>meu_cadastro.png" class="img-responsive" style="display: inline-block;">  Entre ou cadastre-se</a>
                    <?php }?>
                </div>

            </div>
        </div>
    </div>
    <ul class="menu">
        <?php
        include_once(SITE_FOLDER . 'Managers' . DS . 'PartsManager.php');
        $categories = PartsManager::getMainCategories();
        foreach($categories as $category){?>
            <li>
                <a href="<?php echo(SITE_URL . $category->id . '/' . strtolower(StringUtils::clean($category->name)));?>">
                    <?php if($category->image != ""){?>
                        <div class="item transition">
                            <img src="<?php echo(SITE_URL . 'Images/categories/' . $category->image);?>" class="img-responsive" style="margin: 0 auto; height: 40px;">
                            <?php echo($category->name);?>
                        </div>
                    <?php }else{ ?>
                        <div class="item transition" style="padding-top: 21px;">
                            <?php echo($category->name);?>
                        </div>
                    <?php }?>
                </a>
            </li>
        <?php }?>
    </ul>
    <?php
    $action = SessionManager::getTempAction();
    if($action != '' && isset($action['category'])){
        $categories = PartsManager::getSubCategories($action['category']);?>
        <?php if(count($categories)  > 0){?>
            <ul class="submenu">
                <?php foreach($categories as $category){?>
                    <li>
                        <a href="<?php echo(SITE_URL . $category->id . '/' . strtolower(StringUtils::clean($category->name)));?>">
                            <?php if($category->image != ""){?>
                                <div class="item transition">
                                    <img src="<?php echo(SITE_URL . 'Images/categories/' . $category->image);?>" class="img-responsive" style="margin: 0 auto; height: 25px; display: inline; padding-bottom: 5px">
                                    <?php echo($category->name);?>
                                </div>
                            <?php }else{ ?>
                                <div class="item transition" style="padding-top: 5px;">
                                    <?php echo($category->name);?>
                                </div>
                            <?php }?>
                        </a>
                    </li>
                <?php }?>
            </ul>
        <?php }?>
    <?php }?>
</div>