<?php $show = SessionManager::getNewsLetter();?>
<?php if($show){?>
    <div name="divHrefB" style="height: 0px;width: 0px;overflow:hidden;">
        <a href="#" id="abrir-land" class="btn btn-lg btn-success"
           data-toggle="modal" vis
           data-target="#basicModal">Click to open Modal</a>
    </div>
    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                    <h4 class="modal-title" id="myModalLabel">Cadastre-se e Receba nossas ofertas por E-mail</h4>
                </div>
                <form rule="form" action="<?php echo SITE_URL . "news/add"  ?>" method="post">
                    <div class="modal-body">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">@</span>
                            <input type="text" name="email" class="form-control" placeholder="E-mail" aria-describedby="basic-addon1" required>
                        </div>

                        <div class="form-group" style="padding-top: 10px">
                            <select class="form-control" name="sexo">
                                <option value="-1">Selecione o sexo</option>
                                <option value="m">Masculino</option>
                                <option value="f">Feminino</option>
                            </select>
                        </div>
                        <h4>Interesses</h4>
                        <?php
                        include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
                        $manager = new CategoriesManager();
                        $categories = $manager->getAllCategories();
                        foreach($categories as $c) {
                            echo "<div class='checkbox'>";
                            echo " <label><input type='checkbox' name='CAT_".$c->id."' value='".$c->id."'>".$c->name."</label>";
                            echo "</div>";
                        }

                        ?>
                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo(SITE_URL);?>news/already">Já sou cadastrado</a>
                        <input class="btn btn-primary" type="submit" value="Enviar">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php


    if($_SESSION['useremail'] == null) {

        echo "<script type='text/javascript'>$(document).ready(function(){document.getElementById('abrir-land').click();});</script>";
    } else {
        $newsManager = new NewsletterManager();
        $news = new Newsletter();
        $news = $newsManager->getNewsWithEmail($_SESSION['useremail']);
        if($news->email == null || $news->email == "") {
            echo "<script type='text/javascript'>$(document).ready(function(){document.getElementById('abrir-land').click();});</script>";
        }
    }
    ?>
<?php }?>