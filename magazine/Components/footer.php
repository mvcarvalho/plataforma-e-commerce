<!-- RODAPÉ -->
<?php if(SessionManager::getUserId() == -93884923){?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 user-shortcuts">
        <div class="container">
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="<?php echo(SITE_URL)?>cart" class="transition">
                        <img src="<?php echo(SITE_URL)?>Images/cart.png" alt="Meu Carrinho" class="img-responsive" style="width: 80px; margin: 0 auto">
                        Meu Carrinho
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="<?php echo(SITE_URL)?>user/profile" class="transition">
                        <img src="<?php echo(SITE_URL)?>Images/user.png" alt="Meu Cadastro" class="img-responsive" style="width: 65px; margin: 0 auto">
                        Meu Cadastro
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="<?php echo(SITE_URL)?>user/history" class="transition">
                        <img src="<?php echo(SITE_URL)?>Images/list.png" alt="Minhas Compras" class="img-responsive" style="width: 65px; margin: 0 auto">
                        Minhas Compras
                    </a>
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
        </div>
    </div>
<?php }?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer-search">
    <div class="container" style="padding-top: 15px">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <form role="form" method="post" action="<?php echo(SITE_URL . 'pesquisa')?>">
                <div class="form-group">
                    <input class="form-control search-field" type="text" name="search" id="search" placeholder="Digite aqui a sua busca" required value="<?php
                    $search = SessionManager::getTempAction();
                    if(isset($search['search'])){echo($search['search']);}?>">
                    <img src="<?php echo(SITE_URL)?>Images/search.png" style="position: absolute; right: 25px; top: 10px;" alt="Pesquisa">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer">
    <div class="container" style="width: 90%">

        <div class="row">
            <!-- COLUNA 1 -->
            <div class="col-lg-2 col-md-3 col-sm-5 col-xs-12 column" style="width: 19%">
                <h5>Conheça o <?php echo(SITE_NAME)?> ;)</h5>
                <a href="<?php echo(SITE_URL)?>page/about" class="transition">Quem somos</a>
                <a href="<?php echo(SITE_URL)?>page/privacy" class="transition">Política de privacidade</a>
                <a href="<?php echo(SITE_URL)?>page/security" class="transition">Segurança</a>
                <a href="<?php echo(SITE_URL)?>page/contract" class="transition">Termos de uso</a>
                <a href="<?php echo(SITE_URL)?>page/contact" class="transition">Fale conosco</a>
                <a href="<?php echo(SITE_URL)?>user/ticket-list" class="transition">Tickets</a>
            </div>

            <!-- COLUNA 2 -->
            <div class="col-lg-2 col-md-3 col-sm-7 col-xs-12 column" style="width: 27%">
                <h5 style="padding-left: 5px">Certificados e segurança</h5>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0">
                    <a href="https://safeweb.norton.com/report/show_mobile?url=<?php echo(SITE_URL_SMALL)?>" target="_blank" style="width: auto">
                        <img src="<?php echo(SITE_URL)?>Images/selo-norton.png" alt="Norton Safe Web" class="img-responsive" style="display: block; width: 91px; float: left; margin: 3px;">
                    </a>
                    <a href="https://www.google.com/safebrowsing/diagnostic?site=<?php echo(SITE_URL_SMALL)?>" target="_blank" style="width: auto">
                        <img src="<?php echo(SITE_URL)?>Images/selo-google.png" alt="Google Safe Browsing" class="img-responsive" style="display: block; width: 97px; float: left; margin-left: 16px; margin-top:4px;">
                    </a>
                    
                    <a href="http://www.urlvoid.com/scan/<?php echo(SITE_URL_SMALL)?>/" target="_blank" style="width: auto">
                        <img src="<?php echo(SITE_URL)?>Images/selo-void.png" alt="URL Void" class="img-responsive" style="display: block; width:114px; float: left; margin-left: 20px; margin-top:8px;">
                    </a>
                    <a href="http://www.correios.com.br/" target="_blank"><img src="<?php echo(SITE_URL)?>Images/correios.png" alt="URL Void" class="img-responsive" style="display: block; width: 115px; float: left; margin-top:15px; margin-left:5px;"></a>
                    
                </div>
            </div>

            <!-- COLUNA 3 -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 column" style="width: 20%">
                <h5>Formas de pagamento</h5>
                <img src="<?php echo(SITE_URL)?>Images/bandeiras.png" alt="Meios de pagamento" class="img-responsive"/>
            </div>

            <!-- COLUNA 4 -->
            <div class="col-lg-2 col-md-6 col-sm-3 col-xs-12 column" style="width: 19%">
                <h5>Atendimento SAC</h5>
                <p><!--<i class="glyphicon glyphicon-phone"></i>--> 
                <img src="<?php echo(SITE_URL)?>Images/icone-tel-rodape.png" style="margin-right:5px;" />(11) 3181-4952</p>
            </div>

            <!-- COLUNA 5 -->
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 column" style="width: 15%">
                <h5>Curta nossa fan page</h5>
                <div class="fb-like" data-href="https://www.facebook.com/pages/Big-Vitrine/397882953710939?fref=ts" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-3 col-xs-12 column">
                <p>Copyright &copy; 2014-2015 <?php echo(SITE_NAME)?>.<br>Todos os direitos reservados.</p>
            </div>

           <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=768518623163724&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rights">
  <b>Atenção:</b> Todas as regras e promoções são válidas apenas para produtos vendidos e entregues pelo <?php echo(SITE_NAME)?> através deste site.<br>
    O valor de oferta do produto será garantido após a finalização da compra. Havendo divergência, prevalecerá o menor preço ofertado.<br>
    O <?php echo(SITE_NAME)?> se reserva no direito de erros de digitação no cadastro das ofertas de produtos. Os preços poderão ser alterados sem prévio aviso.<br><br>
&copy; <?php echo(SITE_NAME)?> 2015 - <?php echo(SITE_URL_SMALL)?> </div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 security-info">
    <div class="container">
        <img src="<?php echo(SITE_URL)?>Images/lock.png" class="img-responsive pull-left" style="display: inline-block; vertical-align: top;" alt="SSL">
        <p style="display: inline-block; margin: 0 5px; text-align: left; float: left; font-size:12px; color:#999; line-height:14px;"><b>Site seguro com criptografia (SSL).</b><br>Todas as transações efetuadas no <?php echo(SITE_NAME)?> estão protegidas por uma chave de segurança certificada, homologada pela Certisign.</p>
        <img src="<?php echo(SITE_URL);?>Images/<?php
        if(GATEWAY_INTEGRATION == "mp"){
            echo("powered-mp.png");
        } else if(GATEWAY_INTEGRATION == "moip"){
            echo("powered-moip.png");
        }?>" class="img-responsive" style="float: right; max-height: 60px">
    </div>
</div>