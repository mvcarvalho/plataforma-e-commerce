<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 27/11/14
 * Time: 20:25
 */

class StringUtils {

    public static function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
}