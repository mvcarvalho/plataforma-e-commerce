<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 12/11/14
 * Time: 21:24
 */

class ACL {

    private $passwords;

    public function __construct(){
        $this->passwords = array(
            'adminMagazine' => array(       //Login
                'password' => 'p@ssw0rd',   //Senha
                'id' => 1                   //ID do usuário
            )
        );
    }

    public function getUserId($login = ""){
        if(isset($this->passwords[$login])){
            return $this->passwords[$login]['id'];
        }else{
            return 0;
        }
    }

    public function isRightLogin($login = "", $password = ""){
        if(isset($this->passwords[$login])){
            if($this->passwords[$login]['password'] == $password){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
} 