<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 17:49
 */

function consoleLog($data, $tag = null){
    if($tag == null){
        $tag = "";
    }else{
        $tag = $tag . " - ";
    }
    if(is_array($data) || is_object($data)){
        $_SESSION['console_log'] .= "<script>console.log('PHP - " . date("H:i:s") . ": " . $tag . json_encode($data) . "');</script>";

    } else {
        $_SESSION['console_log'] .= "<script>console.log('PHP - " . date("H:i:s") . ": " . $tag . $data . "');</script>";
    }
}