<?php

class FileUtils {

    public function upload($file, $path, $fileName){
        $uploadPath =  $path . $fileName;
        consoleLog("Upload: " . $uploadPath, "FileUtils");

        if( !file_exists($path) ){
            mkdir($path);
        }
        if (move_uploaded_file($file['tmp_name'], $uploadPath)) {
            return true;
        }else{
            return false;
        }
    }
} 