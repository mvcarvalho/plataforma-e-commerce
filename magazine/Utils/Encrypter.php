<?php
define("ENCRYPT_KEY","a2554e5374ea40ffe2dba27c");
define("ENCRYPT_IV_STRING","09182736");

function encrypt($text){
    $key = hash("md5", base64_decode(ENCRYPT_KEY), TRUE);
    for ($x = 0; $x < 8; $x++) {
        $key = $key.substr($key, $x, 1);
    }
    $padded = pkcs5Pad($text, mcrypt_get_block_size(MCRYPT_3DES, MCRYPT_MODE_CBC));
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_3DES, $key, $padded, MCRYPT_MODE_CBC, ENCRYPT_IV_STRING));

    return $encrypted;
}

function decrypt($text){
    $key = hash("md5", base64_decode(ENCRYPT_KEY), TRUE);
    for ($x = 0; $x < 8; $x++) {
        $key = $key.substr($key, $x, 1);
    }

    $decrypted = mcrypt_decrypt(MCRYPT_3DES, $key, base64_decode($text), MCRYPT_MODE_CBC, ENCRYPT_IV_STRING);
    $decrypted = pkcs5Unpad($decrypted);
    return $decrypted;
}

function pkcs5Pad($text, $blocksize) {
    $pad = $blocksize - (strlen($text) % $blocksize);
    return $text . str_repeat(chr($pad), $pad);
}

function pkcs5Unpad($text) {
    $pad = ord($text{strlen($text)-1});
    if ($pad > strlen($text)){
        return false;
    }
    if (strspn($text, chr($pad), strlen($text) - $pad) != $pad){
        return false;
    }
    return substr($text, 0, -1 * $pad);
}