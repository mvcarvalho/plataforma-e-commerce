<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 09/10/14
 * Time: 23:18
 */

class ParametersParser {

    public function parseGet(){
        $getParams = array();
        foreach($_GET as $key => $value){
            $getParams[$key] = $value;
        }
        return $getParams;
    }

    public function parsePost(){
        $getParams = array();
        foreach($_POST as $key => $value){
            $getParams[$key] = $value;
        }
        return $getParams;
    }
} 