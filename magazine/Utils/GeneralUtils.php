<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 27/11/14
 * Time: 21:29
 */

class GeneralUtils {

    public static function getMaxInstallments($productPrice = 1, $minValue = 1, $maxInstallments){
        try{
            $value = $productPrice / $maxInstallments;

            if($value >= $minValue){
                return array('installments' => $maxInstallments, 'value' => $value);
            }else{
                $installments = $productPrice / $minValue;

                if($installments > 1){
                    $value = $productPrice / floor($installments);

                }else{
                    $installments = 1;
                    $value = $productPrice;
                }

                return array('installments' => floor($installments), 'value' => $value);
            }
        }catch (Exception $e){
            return array('installments' => 1, 'value' => $productPrice);
        }
    }

    public static function getFormattedValue($value, $symbol = 'R$'){
        return $symbol . number_format($value, 2, ',', '.');
    }

    public static function getStatusText($status = 0){
        if($status == 5){
            return 'Aguardando Confirmação';

        } else if($status == 4){
            return 'Produto enviado';

        } else if($status == 3){
            return 'Pagamento completo';

        } else if($status == 2){
            return 'Falha no pagamento';

        } else if($status == 1){
            return 'Pagamento pendente';

        } else{
            return 'Não confirmado';
        }
    }
} 