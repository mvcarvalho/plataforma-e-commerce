<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 24/11/14
 * Time: 23:51
 */

include_once(SITE_FOLDER . 'Libs/phpexcel/Classes/PHPExcel.php');

class XlsExporter {

    public static function generateSalesReport($sales){
        include_once(SITE_FOLDER . 'Entities/Sale.php');

        $objPHPExcel = new PHPExcel();
        // Definimos o estilo da fonte
        $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

        // Criamos as colunas
        $objPHPExcel->setActiveSheetIndex(0);

        // Podemos configurar diferentes larguras paras as colunas como padrão
        //$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(90);
        //$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        //$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        //$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);

        $line = 0;
        foreach($sales as $sale){
            $line += 1;
            // Também podemos escolher a posição exata aonde o dado será inserido (coluna, linha, dado);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $line, "Fulano");
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $line, " da Silva");
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $line, "fulano@exemplo.com.br");
        }

        // Podemos renomear o nome das planilha atual, lembrando que um único arquivo pode ter várias planilhas
        $objPHPExcel->getActiveSheet()->setTitle('Vendas - ' . date('d:m:y'));

        // Cabeçalho do arquivo para ele baixar
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="vendas_' . date('d:m:y') . '.xls"');
        header('Cache-Control: max-age=0');

        // Acessamos o 'Writer' para poder salvar o arquivo
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
        $objWriter->save('php://output');

        //exit;
    }
}