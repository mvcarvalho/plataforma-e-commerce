<?php
/**
 * CONTANTES DE TEXTOS UTILIZADOS NA PLATAFORMA.
 */

//BANNERS ADMIN PAGE
define("TXT_BANNER_MAIN_NAME", "Banner Principal");
define("TXT_BANNER_SECCONDARY_NAME", "Banner Secundário Estático");
define("TXT_BANNER_PRODUCT_NAME", "Banner de Produto");
define("TXT_BANNER_STRIPE_NAME", "Banner Faixa");

define("TXT_BANNER_MAIN_SIZE_HELP", "570 x 595px.");
define("TXT_BANNER_SECCONDARY_SIZE_HELP", "570 x 282px.");
define("TXT_BANNER_PRODUCT_SIZE_HELP", "370 x 307px.");
define("TXT_BANNER_STRIPE_SIZE_HELP", "1168 x 73px.");