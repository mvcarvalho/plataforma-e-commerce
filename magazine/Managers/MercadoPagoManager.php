<?php
try{
    include_once('../Config.php');
    include_once('../Utils/Log.php');
    include_once ('../Managers/SalesManager.php');
}catch (Exception $e){}

class MercadoPagoManager {

    public static function setPaymentStatus($saleId, $status, $collectionId, $requestString = null){
        $manager = new SalesManager();
        $sale = $manager->getSaleById($saleId);
        $sale->status = $status;
        $sale->externalToken = $collectionId;

        if($requestString != null){
            $sale->paymentRequestString = $requestString;
        }

        $manager->updateSale($sale);
    }

} 