<?php

require_once (SITE_FOLDER . 'Libs/mercadopago.php');

class PaymentManager {

    public function executeCheckout($gateway, $params){
        switch($gateway){
            case 'MP':
                return $this->executeMercadoPagoCheckout($params);
        }
    }

    public function receivePaymentInfo($gateway, $id){
        switch($gateway){
            case 'MP':
                return $this->receiveMercadoPagoPaymentInfo($id);
        }
    }

    public function requestPaymentInfo($gateway, $id){
        switch($gateway){
            case 'MP':
                return $this->requestMercadoPagoPaymentInfo($id);
        }
    }

    public function cancelPayment($gateway, $id){
        switch($gateway){
            case 'MP':
                return $this->cancelMercadoPagoPayment($id);
        }
    }

    public function getAccessToken($gateway){
        switch($gateway){
            case 'MP':
                return $this->requestMercadoPagoAccessToken();
        }
    }

    public function refoundPayment($gateway, $id){
        switch($gateway){
            case 'MP':
                return $this->refoundMercadoPagoPayment($id);
        }
    }

    public function executePost($uri, $data){
        $restClient = new MPRestClient();
        return $restClient->post($uri, $data);
    }

    private function executeMercadoPagoCheckout($params){
        $mp = new MP (MP_CLIENT_ID, MP_CLIENT_SECRET);
        $preference = $mp->create_preference($params);

        return $preference;
    }

    private function receiveMercadoPagoPaymentInfo($id){
        $mp = new MP (MP_CLIENT_ID, MP_CLIENT_SECRET);
        $paymentInfo = $mp->get_payment_info ($id);

        if ($paymentInfo["status"] == 200) {
            return $paymentInfo;
        }else{
            return false;
        }
    }

    private function requestMercadoPagoPaymentInfo($id){
        $mp = new MP (MP_CLIENT_ID, MP_CLIENT_SECRET);
        $filters = array(
            "external_reference" => $id
        );
        $searchResult = $mp->search_payment($filters);

        return $searchResult;
    }

    private function cancelMercadoPagoPayment($id){
        $mp = new MP (MP_CLIENT_ID, MP_CLIENT_SECRET);
        $result = $mp->cancel_payment($id);
        echo(json_encode($result) . '<br><br>');

        return $result;
    }

    private function requestMercadoPagoAccessToken(){
        $mp = new MP (MP_CLIENT_ID, MP_CLIENT_SECRET);
        $result = $mp->get_access_token();
        return $result;
    }

    private function refoundMercadoPagoPayment($id){
        $mp = new MP (MP_CLIENT_ID, MP_CLIENT_SECRET);
        $result = $mp->refund_payment($id);
        echo(json_encode($result) . '<br><br>');

        return $result;
    }
}