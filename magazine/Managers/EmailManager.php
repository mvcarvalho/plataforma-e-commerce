<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 09/12/14
 * Time: 21:49
 */

class EmailManager {
	
	private static function buildHeaders($sender, $from, $reply, $boundary){
		$newline = "\n";
		$header = '';
		$header .= 'MIME-Version: 1.0' . $newline;
		$header .= 'Date: ' . date('D, d M Y H:i:s O') . $newline;
		$header .= 'From: ' . '=?UTF-8?B?' . base64_encode($sender) . '?=' . ' <' . $from . '>' . $newline;
		$header .= 'Reply-To: ' . '=?UTF-8?B?' . base64_encode($sender) . '?=' . ' <' . $reply . '>' . $newline;
		$header .= 'Return-Path: ' . $from . $newline;
		$header .= 'X-Mailer: PHP/' . phpversion() . $newline;
		$header .= 'Content-Type: multipart/related; boundary="' . $boundary . '"' . $newline . $newline;
		return $header;
	}
	
	private static function buildBody($boundary, $content){
		$newline = "\n";
		$message  = '--' . $boundary . $newline;
		$message .= 'Content-Type: multipart/alternative; boundary="' . $boundary . '_alt"' . $newline . $newline;
		$message .= '--' . $boundary . '_alt' . $newline;
		$message .= 'Content-Type: text/plain; charset="utf-8"' . $newline;
		$message .= 'Content-Transfer-Encoding: 8bit' . $newline . $newline;
		$message .= '--' . $boundary . '_alt' . $newline;
		$message .= 'Content-Type: text/html; charset="utf-8"' . $newline;
		$message .= 'Content-Transfer-Encoding: 8bit' . $newline . $newline;
		$message .= $content . $newline;
		$message .= '--' . $boundary . '_alt--' . $newline;
		$message .= '--' . $boundary . '--' . $newline;
		return $message;
	}

    public static function sendSimpleEmail($sender, $from, $reply, $destination, $subject, $content){
		$boundary = '----=_NextPart_' . md5(time());
		$head = EmailManager::buildHeaders($sender, $from, $reply, $boundary);
		$body = EmailManager::buildBody($boundary, $content);
		ini_set('sendmail_from', $from);
        mail($destination,'=?UTF-8?B?'.base64_encode($subject).'?=',$body,$head);
    }


} 