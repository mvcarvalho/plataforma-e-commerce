<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 17:40
 */

include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'DAOs/UserDAO.php');
include_once(SITE_FOLDER . 'DAOs/TicketDAO.php');
include_once(SITE_FOLDER . 'Entities/User.php');
include_once(SITE_FOLDER . 'Entities/Ticket.php');
include_once(SITE_FOLDER . 'Config.php');

class UsersManager {

    public function getUsersById($userId){
        $userDAO = new UserDAO();
        return $userDAO->selectUserById($userId);
    }

    public function getUserByEmail($email){
        $userDAO = new UserDAO();
        return $userDAO->selectUserByEmail($email);
    }

    public function getAllUsers(){
        $userDAO = new UserDAO();
        return $userDAO->select(new User());
    }

    public function getAllUsersPage($page){
        $userDAO = new UserDAO();

        if(($page - 1) > 0){
            $page = $page - 1;
        }else{
            $page = 0;
        }

        return $userDAO->selectPage($page, ADMIN_PAGINATION_COUNT);
    }

    public function login($email, $password){
        $userDAO = new UserDAO();
        $password = md5($password);
        $user = $userDAO->selectUserByEmailAndPassword($email, $password);

        if($user->id != 0){
            SessionManager::setUserName($user->name);
            SessionManager::setUserEmail($user->email);
            SessionManager::setUserId($user->id);
            return $user;
        }else{
            return false;
        }
    }

    public function register($email, $password, $name){
        $userDAO = new UserDAO();
        $userTemp = $userDAO->selectUserByEmail($email);

        if($userTemp->id == 0){
            $user = new User();
            $user->email = $email;
            $user->password = md5($password);
            $user->name = $name;
            $user = $userDAO->insert($user);

            if($user->id != 0){
                SessionManager::setUserName($user->name);
                SessionManager::setUserEmail($user->email);
                SessionManager::setUserId($user->id);
                return $user;

            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    public function logout(){
        SessionManager::setUserName("");
        SessionManager::setUserEmail("");
        SessionManager::setUserId(0);
    }

    public function changePassword($userId, $password){
        $userDAO = new UserDAO();
        $user = new User();
        $user->password = md5($password);
        $user->id = $userId;
        return $userDAO->updatePassword($user);
    }

    public function saveUserData(User $user){
        $userDAO = new UserDAO();
        $userDAO->update($user);
    }

    public function getUserTickets($userId){
        $userDAO = new UserDAO();
        $user = $userDAO->selectUserById($userId);

        $tickets = array();

        if($user->id > 0){
            $ticketDAO= new TicketDAO();
            $tickets = $ticketDAO->selectByUser($user->id);
        }

        return $tickets;
    }

    public function getUserTicketById($userId, $ticketId){
        $userDAO = new UserDAO();
        $user = $userDAO->selectUserById($userId);

        $ticket = new Ticket();
        if($user->id > 0){
            $ticketDAO= new TicketDAO();
            $ticket = $ticketDAO->selectById($ticketId);
            if($ticket->userId != $user->id){
                $ticket = new Ticket();
            }
        }

        return $ticket;
    }

    public function createUserTicket($userId, $subject, $saleId = 0){
        $userDAO = new UserDAO();
        $user = $userDAO->selectUserById($userId);

        $ticket = new Ticket();

        if($user->id > 0){
            $ticketDAO = new TicketDAO();
            $ticket->saleId = $saleId;
            $ticket->userId = $user->id;
            $ticket->subject = $subject;
            $ticketDAO->insert($ticket);
        }

        return $ticket;
    }

    public function updateUserTicketById(Ticket $ticket){
        $userDAO = new UserDAO();
        $user = $userDAO->selectUserById($ticket->userId);

        if($user->id > 0){
            $ticketDAO = new TicketDAO();
            $ticketDAO->update($ticket);
        }
    }

    public function addUserTicketMessage($ticketId, $message){
        $ticket = new TicketMessage();
        $ticketDAO = new TicketMessageDAO();
        $ticket->isFromUser = true;
        $ticket->message = $message;
        $ticket->ticketId = $ticketId;
        $ticketDAO->insert($ticket);

        return $ticket;
    }
} 