<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 07/10/14
 * Time: 23:10
 */

include_once(SITE_FOLDER . 'DAOs/ProductDAO.php');
include_once(SITE_FOLDER . 'DAOs/CategoryDAO.php');
include_once(SITE_FOLDER . 'Entities/Product.php');
include_once(SITE_FOLDER . 'Config.php');

class ProductsManager {

    public function saveProduct(Product $product){
        if($product != null){
            $productDAO = new ProductDAO();
            if($product->id == 0){

                $product = $productDAO->insert($product);

            }else{
                $productDAO->update($product);
            }
        }
        return $product;
    }

    public function getProductById($productId){
        $productDAO = new ProductDAO();
        $product = $productDAO->selectById($productId);

        if($product->id != 0){
            return $product;
        }else{
            return false;
        }
    }

    public function activateDeactivateProduct($productId){
        $product = $this->getProductById($productId);
        $productDAO = new ProductDAO();
        if($product->id > 0){
            if($product->isActive == 0){
                $product->isActive = 1;
            }else{
                $product->isActive = 0;
            }
            $productDAO->update($product);
        }

    }

    public function addProductAttribute($productId, $attributeId){
        $product = $this->getProductById($productId);
        $productDAO = new ProductDAO();
        if($product->id > 0){
            $productDAO->deleteProductAttribute($productId, $attributeId);
            $productDAO->insertProductAttribute($productId, $attributeId, 0);
        }
    }

    public function removeProductAttribute($productId, $attributeId){
        $product = $this->getProductById($productId);
        $productDAO = new ProductDAO();
        if($product->id > 0){
            $productDAO->deleteProductAttribute($productId, $attributeId);
        }
    }

    public function getProductByCategory($categoryId, $actives = false){
        $categoryDAO = new CategoryDAO();
        $categories[] = $categoryDAO->selectSubCategories($categoryId);

        $products = $this->getProductsSubcategories($categories, $actives);
        return $products;
    }

    public function getProductBySearch($search, $actives = false){
        $dao = new ProductDAO();
        $products = $dao->selectBySearch($search, $actives);
        return $products;
    }

    public function getRandomProductsByCategory($categoryId, $actives = false, $random  = false){
        $categoryDAO = new CategoryDAO();
        $categories[] = $categoryDAO->selectSubCategories($categoryId);

        $products = $this->getProductsSubcategories($categories, $actives, $random);
        return $products;
    }

    public function getAllProducts($active = false){
        $productDAO = new ProductDAO();
        $products = $productDAO->selectAll($active);

        return $products;
    }

    public function getRandomProducts($active = false){
        $productDAO = new ProductDAO();
        $products = $productDAO->selectRandom($active);

        return $products;
    }

    /**
     * Soma a quantidade referenciada em $qtd na coluna sales do produto
     *
     * @param $productId
     *
     * @param $qtd
     *
     */
    public function addQtdSaleProduct($productId, $qtd) {

        $p = new Product();
        $p = $this->getProductById($productId);
        $p->sales += $qtd;
        $this->saveProduct($p);
    }

    private function getProductsSubcategories($categories, $actives = false, $random = false){
        $productDAO = new ProductDAO();
        $products = array();

        foreach($categories as $category){

            if($category->id != 0){

                $productsTmp = $productDAO->selectByCategoryId($category->id, $actives, $random);
                foreach($productsTmp as $p){
                    $products[] = $p;
                }
            }

            if(count($category->subcategories) > 0){
                $productsTmp = $this->getProductsSubcategories($category->subcategories, $actives, $random);
                foreach($productsTmp as $p){
                    $products[] = $p;
                }
            }
        }

        return $products;
    }

    public function addProductImage($prodId, $imgName){
        $dao = new ProductDAO();
        $dao->insertProductImage($prodId, $imgName);
    }

    public function removeProductImage($prodId, $imgName){
        $dao = new ProductDAO();
        $dao->deleteProductImage($prodId, $imgName);
    }

    public function selectProductIdByImageName($imgName){
        $dao = new ProductDAO();
        return $dao->selectProductIdByImage($imgName);
    }
} 