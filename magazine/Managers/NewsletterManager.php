<?php
include_once(SITE_FOLDER . 'DAOs/NewsletterDAO.php');
include_once(SITE_FOLDER . 'Config.php');

class NewsletterManager {

    public function saveNewsletter(Newsletter $newsletter){
        if($newsletter != null){

            $newsletterDAO = new NewsletterDAO();
            if($newsletter->newsletter_id == 0){

                $newsletter = $newsletterDAO->insert($newsletter);
            } else {
                $newsletterDAO->update($newsletter);
            }
        }
        return $newsletter;
    }

    public function getAllNews() {

        $newsDAO = new NewsletterDAO();
        return $newsDAO->select();
    }

    public function getNewsOfCategories($arrayCategories) {

        $newsDAO = new NewsletterDAO();
        return $newsDAO->selectCategories($arrayCategories);
    }

    public function getNewsWithEmail($email) {

        $newsDAO = new NewsletterDAO();
        return $newsDAO->getNewsWithEmail($email);
    }

    /** Metodo de teste */
    public function test2e() {

        $t = new Newsletter();

        $arr = array();
        $arr[] = new Interest($t->newsletter_id,15);
        $arr[] = new Interest($t->newsletter_id,16);
        $arr[] = new Interest($t->newsletter_id,17);
        $arr[] = new Interest($t->newsletter_id,18);

        $t->email = "rubens.machion@gmail.com";
        $t->sexo = "M";
        $t->interest = $arr;

        $this->saveNewsletter($t);
    }
} 