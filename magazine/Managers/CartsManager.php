<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 07/10/14
 * Time: 23:11
 */

include_once(SITE_FOLDER . 'DAOs/CartDAO.php');
include_once(SITE_FOLDER . 'DAOs/CartItemDAO.php');
include_once(SITE_FOLDER . 'DAOs/CartItemOptionDAO.php');
include_once(SITE_FOLDER . 'Entities/Cart.php');
include_once(SITE_FOLDER . 'Entities/CartItem.php');
include_once(SITE_FOLDER . 'Entities/CartItemOption.php');
include_once(SITE_FOLDER . 'Config.php');

class CartsManager {

    public function getCart($userId = 0){
        $cartDAO = new CartDAO();
        $cart = $cartDAO->selectByUserId($userId);

        if($cart->id == 0){
            $cart->userId = $userId;
            $cart->creationDate = time();
            $cartDAO->insert($cart);
        }

        foreach($cart->items as $item){
            if(count($item->options) > 0){
                foreach($item->options as $option){
                    $item->product->name .= ' - ' . $option->attribute->name;
                }
            }
        }

        return $cart;
    }

    public function cleanCart($userId = 0){
        $cartDAO = new CartDAO();
        $cart = $cartDAO->selectByUserId($userId);

        if($cart->id > 0){
            $cartDAO->delete($userId);
        }

        $cart->userId = $userId;
        $cart->creationDate = time();
        $cartDAO->insert($cart);

        return $cart;
    }

    public function addCartItem($cartId, $itemId, $quantity, $price, $featureOptions = ""){
        $cartItemDAO = new CartItemDAO();
        $cartItem = new CartItem();
        $cartItem->cartId = $cartId;
        $cartItem->productId = $itemId;
        $cartItem->quantity = $quantity;
        $cartItem->price = $price;
        $cartItem->optionsText = $featureOptions;
        $cartItemDAO->insert($cartItem);
        return $cartItem->id;
    }

    public function addCartItemOption($cartItemId, $optionId, $price){
        $cartItemOptionDAO = new CartItemOptionDAO();
        $cartItemOption = new CartItemOption();

        $cartItemOption->attributeId = $optionId;
        $cartItemOption->cartItemId = $cartItemId;
        $cartItemOption->priceChange = $price;

        $cartItemOptionDAO->insert($cartItemOption);
    }

    public function removeCartItemOption($cartItemId){
            $cartItemOptionDAO = new CartItemOptionDAO();
            $cartItemOptionDAO->deleteByItemId($cartItemId);
    }

    public function getCartItemByProductId($cartId, $productId){
        $cartItemDAO = new CartItemDAO();
        return $cartItemDAO->selectByCartAndProductId($cartId, $productId);
    }

    public function getCartItem($cartItemId){
        $cartItemDAO = new CartItemDAO();
        return $cartItemDAO->selectById($cartItemId);
    }

    public function removeCartItem($cartItemId){
        $cartItemDAO = new CartItemDAO();
        $cartItemDAO->delete($cartItemId);
    }

    public function updateCartItemQuantity($cartItemId, $quantity){
        $cartItemDAO = new CartItemDAO();
        $cartItem = $cartItemDAO->selectById($cartItemId);
        $cartItem->quantity = $quantity;
        $cartItemDAO->update($cartItem);
    }

    public function updateCart(Cart $cart){
        if($cart->id > 0){
            $dao = new CartDAO();
            $dao->update($cart);
        }
    }
} 