<?php
/**
 * Created by PhpStorm.
 * User: Mateus Carvalho
 * Date: 07/04/2015
 * Time: 21:07
 */

include_once(SITE_FOLDER . 'DAOs/CouponDAO.php');
include_once(SITE_FOLDER . 'Entities/Coupon.php');
include_once(SITE_FOLDER . 'Config.php');

class CouponsManager {

    public function saveCoupon(Coupon $coupon){
        $dao = new CouponDAO();
        if($coupon->id > 0){
            $dao->update($coupon);
        }else{
            $dao->insert($coupon);
        }
    }

    public function getCouponByKey($key){
        if($key){
            $dao = new CouponDAO();
            $coupon = $dao->selectByKey($key);
            return $coupon;
        }
    }

    public function getCouponById($id){
        if($id){
            $dao = new CouponDAO();
            $coupon = $dao->selectById($id);
            return $coupon;
        }
    }

    public function getAllCoupons(){
        $dao = new CouponDAO();
        $coupons = $dao->select();
        return $coupons;
    }
} 