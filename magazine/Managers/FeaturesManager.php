<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 03/12/14
 * Time: 21:42
 */

include_once(SITE_FOLDER . 'DAOs/FeatureDAO.php');
include_once(SITE_FOLDER . 'DAOs/FeatureOptionDAO.php');
include_once(SITE_FOLDER . 'Entities/Feature.php');
include_once(SITE_FOLDER . 'Entities/FeatureOption.php');


class FeaturesManager {

    public function getFeaturesByProductId($productId = 0){
        $featureDAO = new FeatureDAO();
        $featureOptionDAO = new FeatureOptionDAO();
        $features = $featureDAO->selectByProductId($productId);

        foreach($features as $feature){
            $feature->options = $featureOptionDAO->selectByFeatureId($feature->id);
            $optionsText = "";
            $count = 0;
            foreach($feature->options as $option){
                $count++;
                $optionsText .= $option->name;
                if($count < count($feature->options)){
                    $optionsText .= PHP_EOL;
                }
            }
            $feature->optionsText = $optionsText;
        }

        return $features;
    }

    public function getFeaturesById($id = 0){
        $featureDAO = new FeatureDAO();
        $featureOptionDAO = new FeatureOptionDAO();
        $feature = $featureDAO->selectById($id);

        $feature->options = $featureOptionDAO->selectByFeatureId($feature->id);
        $optionsText = "";
        $count = 0;
        foreach($feature->options as $option){
            $count++;
            $optionsText .= $option->name;
            if($count < count($feature->options)){
                $optionsText .= PHP_EOL;
            }
        }
        $feature->optionsText = $optionsText;

        return $feature;
    }

    public function deleteFeatureById($featureId = 0){
        $featureDAO = new FeatureDAO();
        $featureOptionDAO = new FeatureOptionDAO();
        $feature = $featureDAO->selectById($featureId);

        foreach($feature->options as $option){
            $featureOptionDAO->delete($option->id);
        }

        $featureDAO->delete($feature);
    }

    public function saveFeature(Feature $feature){
        $featureDAO = new FeatureDAO();

        if($feature->id == 0){
            $feature = $featureDAO->insert($feature);
        }else{
            $featureDAO->update($feature);
        }

        return $feature;
    }

    public function addFeatureOption(FeatureOption $option){
        $featureOptionDAO = new FeatureOptionDAO();

        if($option->featureId > 0){
            $featureOptionDAO->insert($option);
        }
    }

    public function removeFeatureOption(FeatureOption $option){
        $featureOptionDAO = new FeatureOptionDAO();

        if($option->featureId > 0){
            $featureOptionDAO->delete($option);
        }
    }

    public function removeAllFeatureoptions($featureId = 0){
        $featureOptionDAO = new FeatureOptionDAO();
        $featureOptionDAO->deleteByFeatureId($featureId);
    }
} 