<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 30/12/14
 * Time: 16:58
 */

include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'DAOs/TicketDAO.php');
include_once(SITE_FOLDER . 'Entities/Ticket.php');
include_once(SITE_FOLDER . 'Config.php');

class SupportManager {

    public function getTickets($responsibleId = 0){
        $ticketDAO = new TicketDAO();
        $tickets = $ticketDAO->selectByResponsible($responsibleId);
        return $tickets;
    }

    public function getTicketById($ticketId){
        $ticketDAO = new TicketDAO();
        $ticket = $ticketDAO->selectById($ticketId);
        return $ticket;
    }

    public function updateTicket(Ticket $ticket){
        $userDAO = new UserDAO();
        $user = $userDAO->selectUserById($ticket->userId);

        if($user->id > 0){
            $ticketDAO = new TicketDAO();
            $ticketDAO->update($ticket);
        }
    }

    public function addTicketMessage($ticketId, $message){
        $ticket = new TicketMessage();
        $ticketDAO = new TicketMessageDAO();
        $ticket->isFromUser = false;
        $ticket->message = $message;
        $ticket->ticketId = $ticketId;
        $ticketDAO->insert($ticket);

        return $ticket;
    }
} 