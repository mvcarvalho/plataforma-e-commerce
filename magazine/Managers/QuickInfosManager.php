<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 07/10/14
 * Time: 23:11
 */

include_once(SITE_FOLDER . 'DAOs/QuickInfoDAO.php');
include_once(SITE_FOLDER . 'Entities/QuickInfo.php');
include_once(SITE_FOLDER . 'Config.php');


class QuickInfosManager {

    public function saveQuickInfo(QuickInfo $quickInfo){
        if($quickInfo != null){
            $quickInfoDAO = new QuickInfoDAO();
            if($quickInfo->id == 0){
                $quickInfoDAO->insert($quickInfo);
            }else{
                $quickInfoDAO->update($quickInfo);
            }
        }
    }

    public function getQuickInfoById($quickInfoId){
        $quickInfoDAO = new QuickInfoDAO();
        $quickInfo = $quickInfoDAO->selectById($quickInfoId);
        return $quickInfo;
    }

    public function getQuickInfoByProductId($productId){
        $quickInfoDAO = new QuickInfoDAO();
        $quickInfos = $quickInfoDAO->selectByProductId($productId);
        return $quickInfos;
    }

    public function removeAttributeOption($quickInfoId){
        $quickInfoDAO = new QuickInfoDAO();
        $quickInfoDAO->delete($quickInfoId);
    }
} 