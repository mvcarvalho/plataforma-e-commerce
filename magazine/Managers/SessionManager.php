<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 07/10/14
 * Time: 23:11
 */

class SessionManager {

    public static function setNewsletter($showNewsletter){
        $_SESSION['showNewsletter'] = $showNewsletter;
        setcookie('showNewsletter', $showNewsletter, time() + (86400 * 5), "/");
    }

    public static function getNewsLetter(){
        if(isset($_SESSION['showNewsletter'])){
            SessionManager::setUserName($_SESSION['showNewsletter']);
            return $_SESSION['showNewsletter'];

        }else if(isset($_COOKIE['showNewsletter'])){
            SessionManager::setUserName($_COOKIE['showNewsletter']);
            return $_COOKIE['showNewsletter'];

        }else{
            return true;
        }
    }

    public static function setUserName($username){
        $_SESSION['username'] = $username;
        setcookie('username', $username, time() + (86400 * 30), "/");
    }

    public static function getUserName(){
        if(isset($_SESSION['username'])){
            SessionManager::setUserName($_SESSION['username']);
            return $_SESSION['username'];

        }else if(isset($_COOKIE['username'])){
            SessionManager::setUserName($_COOKIE['username']);
            return $_COOKIE['username'];

        }else{
            return "";
        }
    }

    public static function setUserEmail($useremail){
        $_SESSION['useremail'] = $useremail;
        setcookie('useremail', $useremail, time() + (86400 * 30), "/");
    }

    public static function getUserEmail(){
        if(isset($_SESSION['useremail'])){
            SessionManager::setUserEmail($_SESSION['useremail']);
            return $_SESSION['useremail'];

        }else if(isset($_COOKIE['useremail'])){
            SessionManager::setUserEmail($_COOKIE['useremail']);
            return $_COOKIE['useremail'];

        }else{
            return "";
        }
    }

    public static function setUserId($userid){
        $_SESSION['userid'] = $userid;
        setcookie('userid', $userid, time() + (86400 * 30), "/");
    }

    public static function getUserId(){
        if(isset($_SESSION['userid'])){
            SessionManager::setUserId($_SESSION['userid']);
            return $_SESSION['userid'];

        }else if(isset($_COOKIE['userid'])){
            SessionManager::setUserId($_COOKIE['userid']);
            return $_COOKIE['userid'];

        }else{
            return 0;
        }
    }

    public static function setAdminName($adminName){
    $_SESSION['admin_name'] = $adminName;
}

    public static function getAdminName(){
        if(isset($_SESSION['admin_name'])){
            return $_SESSION['admin_name'];
        }else{
            return "";
        }
    }

    public static function setAdminId($adminId){
        $_SESSION['admin_id'] = $adminId;
    }

    public static function getAdminId(){
        if(isset($_SESSION['admin_id'])){
            return $_SESSION['admin_id'];
        }else{
            return 0;
        }
    }

    public static function setTempAction($tempAction){
        $_SESSION['temp_action'] = $tempAction;
    }

    public static function getTempAction(){
        if(isset($_SESSION['temp_action'])){
            return $_SESSION['temp_action'];
        }else{
            return "";
        }
    }
} 