<?php
/**
 * Created by PhpStorm.
 * User: mateu_000
 * Date: 21/01/15
 * Time: 22:35
 */

include_once(SITE_FOLDER . 'DAOs/SystemUserDAO.php');
include_once(SITE_FOLDER . 'Entities/SystemUser.php');
include_once(SITE_FOLDER . 'Config.php');

class SystemUsersManager {

    public function saveUser(SystemUser $user){
        $userDAO = new SystemUserDAO();
        if($user->id <= 0){
            $userDAO->insert($user);
        }else{
            $tempUser = $userDAO->selectUserById($user->id);
            $userDAO->update($user);
            if($tempUser->password != $user->password){
                $tempUser->password = md5($tempUser->password);
                $userDAO->updateUserPassword($user);
            }
        }
    }

    public function getAll(){
        $userDAO = new SystemUserDAO();
        return $userDAO->select();
    }

    public function getById($id = 0){
        $userDAO = new SystemUserDAO();
        return $userDAO->selectUserById($id);
    }

    public function getByLoginAndPassword($login = "", $password = ""){
        $userDAO = new SystemUserDAO();
        return $userDAO->selectUserByLoginAndPassword($login, $password);
    }
} 