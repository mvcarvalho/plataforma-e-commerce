<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 07/10/14
 * Time: 23:11
 */

include_once(SITE_FOLDER . 'DAOs/AttributeDAO.php');
include_once(SITE_FOLDER . 'DAOs/AttributeOptionDAO.php');
include_once(SITE_FOLDER . 'Entities/Attribute.php');
include_once(SITE_FOLDER . 'Entities/AttributeOption.php');
include_once(SITE_FOLDER . 'Config.php');


class AttributesManager {

    public function saveAttribute(Attribute $attribute){
        if($attribute != null){
            $attributeDAO = new AttributeDAO();
            if($attribute->id == 0){
                $attributeDAO->insert($attribute);
            }else{
                $attributeDAO->update($attribute);
            }
        }
    }

    public function getAttributeById($attributeId){
        $attributeDAO = new AttributeDAO();
        $attribute = $attributeDAO->selectById($attributeId);

        if($attribute->id != 0){
            return $attribute;
        }else{
            return false;
        }
    }

    public function getAllAttributes(){
        $attributeDAO = new AttributeDAO();
        return $attributeDAO->select();
    }

    public function getAllAttributesPage($page){
        $attributeDAO = new AttributeDAO();

        if(($page - 1) > 0){
            $page = $page - 1;
        }else{
            $page = 0;
        }

        return $attributeDAO->selectPage($page, ADMIN_PAGINATION_COUNT);
    }

    public function addAttributeOption(AttributeOption $attribute){
        if($attribute != null){
            $attributeDAO = new AttributeOptionDAO();
            $attributeDAO->insert($attribute);
        }
    }

    public function removeAttributeOption($attributeOptionId){
        $attributeDAO = new AttributeOptionDAO();
        $attributeDAO->delete($attributeOptionId);
    }

    public function getAttributeOption($attributeOptionId){
        $attributeDAO = new AttributeOptionDAO();
        return $attributeDAO->selectById($attributeOptionId);
    }

    public function updateAttributeOption(AttributeOption $attributeOption){
        $attributeDAO = new AttributeOptionDAO();
        $attributeDAO->update($attributeOption);
    }
} 