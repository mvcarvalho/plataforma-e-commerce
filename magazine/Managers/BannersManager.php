<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 11/12/14
 * Time: 20:51
 */

include_once(SITE_FOLDER . 'DAOs/BannerDAO.php');
include_once(SITE_FOLDER . 'Entities/Banner.php');

class BannersManager {

    public function getBanners($type = null){
        $bannerDao = new BannerDAO();
        if($type != null){
            return $bannerDao->selectByType($type);
        }else{
            return $bannerDao->select();
        }
    }

    public function getBannerById($id){
        $bannerDao = new BannerDAO();
        return $bannerDao->selectById($id);
    }

    public function deleteBannerById($id){
        $bannerDao = new BannerDAO();
        $bannerDao->deleteById($id);
    }

    public function saveBanner(Banner $banner){
        $bannerDao = new BannerDAO();
        if($banner->id == 0){
            $bannerDao->insert($banner);
        }else{
            $bannerDao->update($banner);
        }
    }
} 