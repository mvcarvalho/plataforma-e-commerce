<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:34
 */

include_once(SITE_FOLDER . 'DAOs/CartDAO.php');
include_once(SITE_FOLDER . 'DAOs/SaleDAO.php');
include_once(SITE_FOLDER . 'DAOs/SaleItemDAO.php');
include_once(SITE_FOLDER . 'DAOs/SaleItemOptionDAO.php');
include_once(SITE_FOLDER . 'Entities/Cart.php');
include_once(SITE_FOLDER . 'Entities/Sale.php');
include_once(SITE_FOLDER . 'Entities/SaleItem.php');
include_once(SITE_FOLDER . 'Entities/SaleItemOption.php');
include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
include_once(SITE_FOLDER . 'Managers/CouponsManager.php');
include_once(SITE_FOLDER . 'Config.php');

class SalesManager {

    /**
     * @param Cart $cart
     * @param int $deliveryCost
     * @return Sale
     */
    public function convertCartToSale(Cart $cart, $deliveryCost = 0){
        $saleDAO = new SaleDAO();
        $saleItemDAO = new SaleItemDAO();
        $saleItemOptionDAO = new SaleItemOptionDAO();
        $sale = new Sale();
        $pm = new ProductsManager();
        $p = new Product();

        $total = 0;
        foreach($cart->items as $item){
            $total += ($item->price * $item->quantity);
        }

        $couponManager = new CouponsManager();
        if(isset($cart->coupon) && $cart->coupon != "") {
            $coupon = $couponManager->getCouponByKey($cart->coupon);
            if($coupon->offType == 1){
                $total -= $coupon->off;
            }
            if($coupon->offType == 2){
                $total -= ($total / 100) * $coupon->off;
            }
        }

        $total += $deliveryCost;

        $sale->date = time();
        $sale->status = 1;
        $sale->userId = $cart->userId;
        $sale->value = $total;

        $sale = $saleDAO->insert($sale);

        foreach($cart->items as $item){
            $saleItem = new SaleItem();
            $saleItem->price = $item->price;
            $saleItem->productId = $item->productId;
            $saleItem->quantity = $item->quantity;
            $saleItem->optionsText = $item->optionsText;
            $saleItem->saleId = $sale->id;

            $saleItem = $saleItemDAO->insert($saleItem);

            foreach($item->options as $option){
                $saleItemOption = new SaleItemOption();
                $saleItemOption->attributeId = $option->attributeId;
                $saleItemOption->priceChange = $option->priceChange;
                $saleItemOption->saleItemId = $saleItem->id;

                $saleItemOptionDAO->insert($saleItemOption);
            }
        }
		
		$cartDAO = new CartDAO();
		$cart->coupon = "";
		$cartDAO->update($cart);

        return $sale;
    }

    public function getSaleById($saleId = 0){
        $saleDAO = new SaleDAO();
        $sales = $saleDAO->selectById($saleId);
        return $sales;
    }

    public function getSaleByTrackingCode($code = 0){
        $saleDAO = new SaleDAO();
        $sales = $saleDAO->selectByTrackingCode($code);
        return $sales;
    }

    public function getSales($userId = 0){
        $saleDAO = new SaleDAO();
        $sales = $saleDAO->selectByUserId($userId);
        return $sales;
    }

    public function updateSale(Sale $sale){
        $saleDAO = new SaleDAO();
        $saleDAO->update($sale);
    }

    public function updateSaleTrackingCode(Sale $sale){
        $saleDAO = new SaleDAO();
        $saleDAO->updateTrackingCode($sale);
    }

    public function getAllSales($page = 0){
        $salesDAO = new SaleDAO();
        $sales = $salesDAO->select($page);
        return $sales;
    }

    public function getNumberOfPages(){
        $salesDAO = new SaleDAO();
        $sales = $salesDAO->select();
        return $sales;
    }

    public function getSalesByPeriod($startDate, $endDate, $status = -1, $orderBy = null){
        $salesDAO = new SaleDAO();
        $sales = $salesDAO->selectByTime($startDate, $endDate, $status, $orderBy);
        return $sales;
    }

    public function deleteincompleteSale($saleId){
        $salesDAO = new SaleDAO();
        $salesDAO->deleteById($saleId);
    }
} 