<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 19:16
 */

include_once(SITE_FOLDER . 'DAOs/CategoryDAO.php');
include_once(SITE_FOLDER . 'Entities/Category.php');
include_once(SITE_FOLDER . 'Config.php');

class CategoriesManager {

    public function saveCategory(Category $category){
        if($category != null){
            $categoryDAO = new CategoryDAO();
            if($category->id == 0){
                $categoryDAO->insert($category);
            }else{
                $categoryDAO->update($category);
            }
        }
    }

    public function getCategoryById($categoryId){
        $categoryDAO = new CategoryDAO();
        $category = $categoryDAO->selectById($categoryId);

        if($category->id != 0){
            return $category;
        }else{
            return false;
        }
    }

    public function getAllCategories(){
        $categoryDAO = new CategoryDAO();
        return $categoryDAO->select();
    }

    public function getAllCategoriesPage($page){
        $categoryDAO = new CategoryDAO();

        if(($page - 1) > 0){
            $page = $page - 1;
        }else{
            $page = 0;
        }

        return $categoryDAO->selectPage($page, ADMIN_PAGINATION_COUNT);
    }

    public function getAllCategoriesStructure(){
        return $this->getSubcategories(0);
    }

    private function getSubcategories($categoryId){
        $categoryDAO = new CategoryDAO();

        //Retorna do banco, todas as categorias que o ID da categoria pai, seja o informado.
        $categories = $categoryDAO->selectByParentId($categoryId);

        //Para cada uma das categorias encontradas, são retorndas as suas subcategorias.
        for($x = 0; $x < count($categories); $x++){
            $categories[$x]->subcategories = $categoryDAO->selectByParentId($categories[$x]->id);

            for($y = 0; $y < count($categories[$x]->subcategories); $y++){
                $categories[$x]->subcategories[$y]->subcategories = $this->getSubcategories($categories[$x]->subcategories[$y]->id);
            }
        }

        consoleLog($categories);

        return $categories;
    }
} 