<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 06/11/14
 * Time: 10:19
 */

class PartsManager{

    public static function getMainCategories(){
        $categoryDAO = new CategoryDAO();
        $categories = $categoryDAO->selectByParentId(0, true);
        return $categories;
    }

    public static function getSubCategories($id){
        $categoryDAO = new CategoryDAO();
        $categories = $categoryDAO->selectByParentId($id, true);
        return $categories;
    }

    public static function printCategoriesTableAdmin($categories){
        foreach($categories as $category){
            echo ('<tr>');
            echo ('<td>' . $category->name . '</td>');
            echo ('<td>' . $category->description . '</td>');
            echo ('<td style="text-align: center;">');
            echo ('<a href="' . SITE_URL . 'adm/category/view/' . $category->id . '">[View]</a>');
            if($category->isActive){
                echo ('<a href="' . SITE_URL . 'adm/category/active/' . $category->id . '">[Desativar]</a>');
            }else{
                echo ('<a href="' . SITE_URL . 'adm/category/active/' . $category->id . '">[Ativar]</a>');
            }
            echo ('</td>');
            echo ('</tr>');

            if(count($category->subcategories) > 0){
                PartsManager::printCategoriesTableAdmin($category->subcategories);
            }
        }
    }
} 