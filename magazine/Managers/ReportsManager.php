<?php

include_once(SITE_FOLDER . 'Libs/phpexcel/Classes/PHPExcel.php');
include_once(SITE_FOLDER . 'Managers/SalesManager.php');
include_once(SITE_FOLDER . 'Entities/Sale.php');
include_once(SITE_FOLDER . 'Utils/Log.php');
include_once(SITE_FOLDER . 'Utils/GeneralUtils.php');
include_once(SITE_FOLDER . 'Managers/NewsletterManager.php');

class ReportsManager {

    private function createClass($filename, $reportSubject){
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator(SITE_NAME)
            ->setLastModifiedBy(SITE_NAME)
            ->setTitle($filename)
            ->setSubject($reportSubject)
            ->setDescription($reportSubject . " - " . SITE_NAME . ".");

        return $objPHPExcel;
    }

    private function saveXlsxFile($objPHPExcel, $filename){
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(SITE_FOLDER . 'Reports' . DS . $filename . ".xlsx");
    }

    public function exportSalesByPeriod($startDate, $endDate, $status = -1){
        $filename = "relatorio_de_vendas_por_periodo_" . date('d-m-Y', $startDate) . "_" . date('d-m-Y', $endDate) . "_" . time();

        $objPHPExcel = $this->createClass($filename, "Relatório de vendas por período");

        //Define o cabeçalho
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Data')
            ->setCellValue('B1', 'ID Pedido')
            ->setCellValue('C1', 'Produto')
			->setCellValue('D1', 'Descrição Curta')
            ->setCellValue('E1', 'Quantidade')
            ->setCellValue('F1', 'Valor')
            ->setCellValue('G1', 'Cliente')
            ->setCellValue('H1', 'Contato')
            ->setCellValue('I1', 'Endereço')
            ->setCellValue('J1', 'Cidade')
            ->setCellValue('K1', 'Estado')
            ->setCellValue('L1', 'CEP')
            ->setCellValue('M1', 'Status')
            ->setCellValue('N1', 'Rastreio');

        $manager = new SalesManager();
        $sales = $manager->getSalesByPeriod($startDate, $endDate, $status);

        $index = 2;
        foreach($sales as $sale){
            foreach($sale->items as $item){
                $name = $item->product->name;

                if(strlen($item->optionsText) > 0){
                    $name .= ' - ' . $item->optionsText;
                }

                foreach($item->options as $option){
                    $name .= ' - ' . $option->attribute->name;
                }

                $objPHPExcel->getActiveSheet()
                    ->setCellValue('A' . $index, $sale->date) //Data
                    ->setCellValue('B' . $index, $sale->id) //ID Pedido
                    ->setCellValue('C' . $index, $name) //Produto
					->setCellValue('D' . $index, $item->product->shortDescription) //Produto
                    ->setCellValue('E' . $index, $item->quantity) //Quantidade
                    ->setCellValue('F' . $index, $item->price) //Valor
                    ->setCellValue('G' . $index, $sale->user->name) //Cliente
                    ->setCellValue('H' . $index, $sale->user->phone . " - " . $sale->user->email) //Contato
                    ->setCellValue('I' . $index, $sale->user->address) //Endereço
                    ->setCellValue('J' . $index, $sale->user->city) //Cidade
                    ->setCellValue('K' . $index, $sale->user->state) //Estado
                    ->setCellValue('L' . $index, $sale->user->cep) //CEP
                    ->setCellValue('M' . $index, GeneralUtils::getStatusText($sale->status)) //Status
                    ->setCellValue('N' . $index, $sale->trackingCode); //Rastreio

                $index++;
            }
        }

        $this->saveXlsxFile($objPHPExcel, $filename);

        return $filename . ".xlsx";
    }

    public function exportUsers(){
        $filename = "relatorio_de_usuarios_" . date('d-m-Y') . "_" . time();
        consoleLog("Gerando Relatório.", "ReportsManager");
        $objPHPExcel = $this->createClass($filename, "Relatório de Usuários Cadastrados");

        //Define o cabeçalho
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'Nome')
            ->setCellValue('C1', 'CPF')
            ->setCellValue('D1', 'E-mail')
            ->setCellValue('E1', 'Telefone')
            ->setCellValue('F1', 'Endereço')
            ->setCellValue('G1', 'Cidade')
            ->setCellValue('H1', 'Estado')
            ->setCellValue('I1', 'CEP');

        $manager = new UsersManager();
        $users = $manager->getAllUsers();

        $index = 2;
        foreach($users as $user){
            $objPHPExcel->getActiveSheet()
                ->setCellValue('A' . $index, $user->id) //Data
                ->setCellValue('B' . $index, $user->name) //ID Pedido
                ->setCellValue('C' . $index, $user->cpf) //Produto
                ->setCellValue('D' . $index, $user->email) //Quantidade
                ->setCellValue('E' . $index, $user->phone) //Valor
                ->setCellValue('F' . $index, $user->address) //Cliente
                ->setCellValue('G' . $index, $user->city) //Contato
                ->setCellValue('H' . $index, $user->state) //Endereço
                ->setCellValue('I' . $index, $user->cep); //Cidade
            $index++;

        }

        $this->saveXlsxFile($objPHPExcel, $filename);
        consoleLog("Finalizando.", "ReportsManager");
        return $filename . ".xlsx";
    }

    public function exportNews($arrayCategories = null){
        $filename = "relatorio_de_newsletters_" . date('d-m-Y') . "_" . time();

        $objPHPExcel = $this->createClass($filename, "Relatório de E-mails Cadastrados");

        //Define o cabeçalho
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Categorias Selecionadas: ')
            ->setCellValue('A2', 'ID')
            ->setCellValue('B2', 'E-mail')
            ->setCellValue('C2', 'Sexo');

        $manager = new NewsletterManager();
        if($arrayCategories == null)
            $news = $manager->getAllNews();
        else
            $news = $manager->getNewsOfCategories($arrayCategories);

        $styleArray = array(
            'borders' => array(
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                ),
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                ),
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                )
            )
        );

        $objPHPExcel->getActiveSheet(0)->getStyle("A1:Z1")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet(0)->getColumnDimension('B')->setWidth(50);
        $objPHPExcel->getActiveSheet(0)->getRowDimension(1)->setRowHeight(30);
        $objPHPExcel->getActiveSheet(0)->getStyle("A1:Z1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet(0)->freezePane("A3");
        $index = 3;
        foreach($news as $new){
            $valueA1 = $objPHPExcel->getActiveSheet(0)->getCell("A1");

            $objPHPExcel->getActiveSheet()
                ->setCellValue('A1', $valueA1.", ".$new->interest)
                ->setCellValue('A' . $index, $new->newsletter_id) //id
                ->setCellValue('B' . $index, $new->email) //Email
                ->setCellValue('C' . $index, $new->sexo); // Sexo
            $index++;
            consoleLog("Obj ".$new->email);
        }



        $this->saveXlsxFile($objPHPExcel, $filename);

        return $filename . ".xlsx";
    }
} 