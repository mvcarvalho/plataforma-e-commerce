<?php
if(isset($_GET['installments']) && isset($_GET['rate']) && isset($_GET['value'])){

    try{session_start();}catch (Exception $e){}

    include_once('../Config.php');
    include_once('../Utils/Log.php');
    include_once(SITE_FOLDER . 'Libs/moip/autoload.inc.php');

    $moip = new Moip();
    $moip->setEnvironment('test');
    $moip->setCredential(array(
        'key' => MOIP_KEY,
        'token' => MOIP_TOKEN
    ));

    $result = $moip->queryParcel(MOIP_LOGIN, $_GET['installments'], $_GET['rate'], $_GET['value']);
    echo(json_encode($result));

}else{
    echo(json_encode(array("error" => "Informe o valor, juros e parcela. (installments, rate e value)")));
}