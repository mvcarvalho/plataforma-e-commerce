<?php
if(isset($_GET['userId']) && isset($_GET['status']) && isset($_GET['token'])){

    try{session_start();}catch (Exception $e){}

    echo(json_encode($_GET, JSON_PRETTY_PRINT) . '<br><br>');

    include_once('../Config.php');
    include_once('../Utils/Log.php');
    include_once(SITE_FOLDER . 'Managers/CartsManager.php');
    include_once(SITE_FOLDER . 'Managers/SalesManager.php');
    include_once(SITE_FOLDER . 'Managers/UsersManager.php');
    include_once(SITE_FOLDER . 'Managers/APIPaymentsManager.php');

    $manager = new UsersManager();
    $user = $manager->getUsersById($_GET['userId']);

    if($user->id > 0){
        $manager = new CartsManager();
        $salesManager = new SalesManager();

        $cart = $manager->getCart($user->id);
        if(count($cart->items) > 0){
            $manager->cleanCart($_GET['userId']);
            $sale = $salesManager->getsaleByExternalId($_GET['token']);

            echo(json_encode($sale, JSON_PRETTY_PRINT) . '<br><br>');

            if($sale->id > 0){
                APIPaymentsManager::setPaymentStatus($sale->id, $_GET['status'], $_GET['token']);
            }
        }
    }
}