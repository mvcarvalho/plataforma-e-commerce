<?php
try{session_start();}catch (Exception $e){}

include_once('../Config.php');
include_once(SITE_FOLDER . 'Utils/Log.php');
include_once(SITE_FOLDER . 'Managers/SalesManager.php');
include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'Managers/UsersManager.php');
include_once(SITE_FOLDER . 'Managers/PaymentManager.php');
include_once(SITE_FOLDER . 'Managers/MercadoPagoManager.php');
include_once(SITE_FOLDER . 'DAOs/AttributeOptionDAO.php');

$userId = SessionManager::getUserId();

if(isset($_GET['method']) && isset($_GET['sale_id'])){
    $method = $_GET['method'];
    $saleId = $_GET['sale_id'];

    if($userId > 0){

        $salesManager = new SalesManager();
        $userManager = new UsersManager();
        $paymentManager = new PaymentManager();

        $user = $userManager->getUsersById($userId);
        $sale = $salesManager->getSaleById($saleId);

        if($sale->id > 0){

            //Caso a conversão ocorra bem, continua o processamento.
            if($method == 'credit-card'){
                try{
                    $token = $paymentManager->getAccessToken('MP');
                }catch (Exception $e){
                    echo ("0");
                    return;
                }
                $uri = "/checkout/custom/create_payment?access_token=" . $token;

                if(!empty($sale->paymentRequestString)){
                    $data = json_decode($sale->paymentRequestString, true);

                }else{
                    $data = array(
                        "payer_email"=> $user->email,
                        //"payer_email" => "test_user_42481957@testuser.com",
                        "amount" => (float)$sale->value,
                        "reason" => "Compra Total Fast",
                        "currency" => "BRL",
                        "card_token_id" => $_GET['cardTokenId'],
                        "installments" => (int)$_GET['installmentsOption'],
                        "external_reference" => $sale->id,
                        "notification_url" => SITE_URL . "APIs/marcadopago_notificacao.php"
                    );
                }

                $result = $paymentManager->executePost($uri, $data);
                if($result['status'] != 201){
                    echo("0"); //Erro ao pagar

                }else{
                    if($result['response']['status_detail'] == 'accredited'){
                        MercadoPagoManager::setPaymentStatus($sale->id, 3, $result['response']['payment_id']);
                        echo("1");

                    }else if($result['response']['status_detail'] == 'pending_contingency'){
                        MercadoPagoManager::setPaymentStatus($sale->id, 5, $result['response']['payment_id']);
                        echo("1");

                    }else if($result['response']['status_detail'] == 'pending_review_manual'){
                        MercadoPagoManager::setPaymentStatus($sale->id, 5, $result['response']['payment_id']);
                        echo("1");

                    }else if($result['response']['status_detail'] == 'cc_rejected_call_for_authorize'){
						$salesManager->deleteincompleteSale($sale->id);
						echo("2");

					}else if($result['response']['status_detail'] == 'cc_rejected_insufficient_amount'){
						$salesManager->deleteincompleteSale($sale->id);
						echo("3");

					}else if($result['response']['status_detail'] == 'cc_rejected_bad_filled_security_code'){
						$salesManager->deleteincompleteSale($sale->id);
						echo("4");

					}else if($result['response']['status_detail'] == 'cc_rejected_bad_filled_date'){
						$salesManager->deleteincompleteSale($sale->id);
						echo("5");

					}else if($result['response']['status_detail'] == 'cc_rejected_bad_filled_other'){
						$salesManager->deleteincompleteSale($sale->id);
						echo("6");

					}else if($result['response']['status_detail'] == 'cc_rejected_other_reason'){
						$salesManager->deleteincompleteSale($sale->id);
						echo("7");

					}else{
                        MercadoPagoManager::setPaymentStatus($sale->id, 2, $result['response']['payment_id']);
                        echo("0");
                    }
                }
            }else if($method == 'ticket'){
                try{
                    $token = $paymentManager->getAccessToken('MP');
                }catch (Exception $e){
                    echo ("0");
                    return;
                }

                if(!empty($token)){
                    $uri = "/checkout/custom/create_payment?access_token=" . $token;
                    $data = array(
                        "payer_email"=> $user->email,
                        //"payer_email" => "test_user_42481957@testuser.com",
                        "amount"=> (float)$sale->value,
                        "reason"=> "Compra Total Fast",
                        "payment_method_id"=> "bolbradesco",
                        "external_reference"=> $sale->id,
                        "notification_url" => SITE_URL . "APIs/marcadopago_notificacao.php"
                    );

                    $result = $paymentManager->executePost($uri, $data);
                    if(isset($result['response']['activation_uri'])){
                        MercadoPagoManager::setPaymentStatus($sale->id, 5, $result['response']['payment_id']);
                        echo($result['response']['activation_uri']);

                    }else{
                        echo("0"); //Erro ao pagar

                    }
                }else{
                    echo('0'); //Erro ao criar token
                }
            }
        }else{
            echo('0'); //Erro ao criar venda (sistema)
        }
    }else{
        echo('0'); //Usuário não logado
    }
}else{
    echo('0'); //Método não informado
}