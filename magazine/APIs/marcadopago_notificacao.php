<?php
session_start();
include_once('../Config.php');
include_once('../Utils/Log.php');
include_once('../Entities/Product.php');
include_once('../Entities/PaymentNotification.php');
include_once('../DAOs/PaymentNotificationDAO.php');
include_once ('../Managers/SalesManager.php');
include_once ('../Managers/PaymentManager.php');
include_once ('../Managers/ProductsManager.php');

$manager = new PaymentManager();
$paymentDAO = new PaymentNotificationDAO();
$payment = new PaymentNotification();
$prodManager = new ProductsManager();

$result = $manager->receivePaymentInfo('MP', $_GET["id"]);
echo(json_encode($result) . '<BR><BR>');

$payment = $paymentDAO->selectByGatewayId($_GET["id"]);
echo(json_encode($payment) . '<BR><BR>');

if($payment->id == 0){
    $payment->gatewayName = 'Mercado Pago';
    $payment->gatewayId = $_GET["id"];
    $payment->date = time();
}

$pageName = "notification_" . $_GET["id"] . ".json";
$file = fopen($pageName, "w");
fwrite($file, json_encode($result));
fclose($file);

if($result != false){
    $result = $result['response'];
    $payment->status = $result['collection']['status_detail'];
    $payment->value = $result['collection']['transaction_amount'];
    $payment->paymentMethod = $result['collection']['payment_type'];

    if($payment->id == 0){
        $paymentDAO->insert($payment);
    }else{
        $paymentDAO->update($payment);
    }
    echo(json_encode($payment) . '<BR><BR>');

    $saleManager = new SalesManager();
    $id = $result['collection']['external_reference'];
    $sale = $saleManager->getSaleById($id);
    echo(json_encode($sale) . '<BR><BR>');

    if($sale->id > 0){
        $sale->externalToken = $_GET["id"];

        if($result['collection']['status_detail'] == 'accredited'){
            if($sale->status != 3){
                try{
                    foreach($sale->items as $item){
                        $product = $prodManager->getProductById($item->$productId);
                        $product->sales += 1;
                        $prodManager->saveProduct($product);
                    }
                }catch (Exception $e){}
                $sale->status = 3;
            }

        }else if($result['response']['status_detail'] == 'pending_contingency'){
            if($sale->status != 5){
                $sale->status = 5;
            }

        }else if($result['response']['status_detail'] == 'pending_review_manual'){
            if($sale->status != 5){
                $sale->status = 5;
            }

        }else if($result['response']['status_detail'] == 'expired'){
            if($sale->status != 6){
                $sale->status = 6;
            }

        }else if($result['response']['status_detail'] == 'refunded'){
            if($sale->status != 6){
                $sale->status = 6;
            }

        }else{
            $sale->status = 1;
        }

        $saleManager->updateSale($sale);
        echo(json_encode($sale) . '<BR><BR>');
        echo('---------------------------NOTIFICAÇÃO PROCESSADA---------------------------');
    }
}