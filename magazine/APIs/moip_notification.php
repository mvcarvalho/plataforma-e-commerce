<?php
if(isset($_POST['id_transacao']) && isset($_POST['status_pagamento'])){

    echo(json_encode($_POST, JSON_PRETTY_PRINT));

    try{session_start();}catch (Exception $e){}

    include_once('../Config.php');
    include_once('../Utils/Log.php');
    include_once(SITE_FOLDER . 'Managers/UsersManager.php');
    include_once(SITE_FOLDER . 'Managers/APIPaymentsManager.php');

    $salesManager = new SalesManager();
    $sale = $salesManager->getSaleByExternalId($_POST['id_transacao']);

    if($sale->id > 0){

        if($_POST['status_pagamento'] == 1 || $_POST['status_pagamento'] == 4){
            $sale->status = 3;

        }else if($_POST['status_pagamento'] == 5 || $_POST['status_pagamento'] == 9){
            $sale->status = 6;

        }else if($_POST['status_pagamento'] == 2 || $_POST['status_pagamento'] == 6){
            $sale->status = 5;

        }else if($_POST['status_pagamento'] == 3){
            $sale->status = 5;

        }else if($_POST['status_pagamento'] == 7){
            $sale->status = 2;
        }

        APIPaymentsManager::setPaymentStatus($sale->id, $sale->status, $sale->externalToken);
    }
}