<?php
try{session_start();}catch (Exception $e){}

include_once('../Config.php');
include_once(SITE_FOLDER . 'Utils/Log.php');
include_once(SITE_FOLDER . 'Managers/CartsManager.php');
include_once(SITE_FOLDER . 'Managers/SalesManager.php');
include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'Managers/UsersManager.php');
include_once(SITE_FOLDER . 'Managers/PaymentManager.php');
include_once(SITE_FOLDER . 'Managers/MercadoPagoManager.php');
include_once(SITE_FOLDER . 'Managers/CouponsManager.php');
include_once(SITE_FOLDER . 'DAOs/AttributeOptionDAO.php');

$userId = SessionManager::getUserId();

if(isset($_GET['method'])){
    $method = $_GET['method'];

    if($userId > 0){

        $manager = new CartsManager();
        $salesManager = new SalesManager();
        $userManager = new UsersManager();
        $paymentManager = new PaymentManager();

        $user = $userManager->getUsersById($userId);
        $cart = $manager->getCart($user->id);

        if(count($cart->items) > 0){

            $total = 0;
            $deliveryCost = 0;

            $items = array();

            //Verifica o preço do maior frete
            foreach($cart->items as $item){
                if($item->product->deliveryCost > $deliveryCost){
                    $deliveryCost = $item->product->deliveryCost;
                }

                $total += (int)$item->quantity * (float)$item->price;

                $items[] = array(
                    "id" => $item->productId,
                    "title" => $item->product->name,
                    "description" => $item->product->name,
                    "category_id" => "others",
                    "quantity" => (int) $item->quantity,
                    "unit_price" => (float) $item->price
                );
            }

            $couponManager = new CouponsManager();
            if(isset($cart->coupon) && $cart->coupon != "") {
                $coupon = $couponManager->getCouponByKey($cart->coupon);
                if($coupon->offType == 1){
                    $total -= $coupon->off;
                }
                if($coupon->offType == 2){
                    $total -= ($total / 100) * $coupon->off;
                }
            }

            $total += $deliveryCost;

            //Converte o carrinho para uma venda, e adiciona o valor do frete.
            $sale = $salesManager->convertCartToSale($cart, $deliveryCost);

            if($sale->id > 0){

                //Caso a conversão ocorra bem, continua o processamento.
                if($method == 'credit-card'){
                    try{
                        $token = $paymentManager->getAccessToken('MP');
                    }catch (Exception $e){
                        $salesManager->deleteincompleteSale($sale->id);
                        echo ("0");
                        return;
                    }

                    $uri = "/checkout/custom/create_payment?access_token=" . $token;
                    $data = array(
                        "payer_email"=> $user->email,
                        //"payer_email" => "test_user_42481957@testuser.com",
                        "amount" => $total,
                        "reason" => "Compra " . SITE_NAME . " - " . $sale->id,
                        "currency" => "BRL",
                        "card_token_id" => $_GET['cardTokenId'],
                        "installments" => (int)$_GET['installmentsOption'],
                        "external_reference" => $sale->id,
                        "notification_url" => SITE_URL . "APIs/marcadopago_notificacao.php",
                        "customer" => array(
                            "email" => $user->email,
                            "first_name" => $user->name,
                            "last_name" => " ",
                            "identification" => array(
                                "type" => "CPF",
                                "number" => $user->cpf
                            ),
                            "address" => array(
                                "zip_code" => $user->cep,
                                "street_name" => $user->address,
                                "street_number" => 0
                            ),
                            "phone" => array(
                                "area_code" => 0,
                                "number" => $user->phone
                            )
                        ),
                        "items" => $items
                    );

                    try{
                        $result = $paymentManager->executePost($uri, $data);
                    }catch (Exception $e){
                        $result['status'] = 0;
                    }

                    if($result['status'] != 201){
                        $salesManager->deleteincompleteSale($sale->id);
                        echo("0"); //Erro ao pagar

                    }else{
                        if($result['response']['status_detail'] == 'accredited'){
                            $manager->cleanCart($userId);
                            MercadoPagoManager::setPaymentStatus($sale->id, 3, $result['response']['payment_id'], json_encode($data));
                            echo("1");

                        }else if($result['response']['status_detail'] == 'pending_contingency'){
                            $manager->cleanCart($userId);
                            MercadoPagoManager::setPaymentStatus($sale->id, 5, $result['response']['payment_id'], json_encode($data));
                            echo("1");

                        }else if($result['response']['status_detail'] == 'pending_review_manual'){
                            $manager->cleanCart($userId);
                            MercadoPagoManager::setPaymentStatus($sale->id, 5, $result['response']['payment_id'], json_encode($data));
                            echo("1");

                        }else if($result['response']['status_detail'] == 'cc_rejected_call_for_authorize'){
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("2");

                        }else if($result['response']['status_detail'] == 'cc_rejected_insufficient_amount'){
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("3");

                        }else if($result['response']['status_detail'] == 'cc_rejected_bad_filled_security_code'){
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("4");

                        }else if($result['response']['status_detail'] == 'cc_rejected_bad_filled_date'){
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("5");

                        }else if($result['response']['status_detail'] == 'cc_rejected_bad_filled_other'){
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("6");

                        }else if($result['response']['status_detail'] == 'cc_rejected_other_reason'){
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("7");

                        }else{
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("0");
                        }
                    }
                }else if($method == 'ticket'){
                    try{
                        $token = $paymentManager->getAccessToken('MP');
                    }catch (Exception $e){
                        $salesManager->deleteincompleteSale($sale->id);
                        echo ("0");
                        return;
                    }

                    if(!empty($token)){
                        $uri = "/checkout/custom/create_payment?access_token=" . $token;
                        $data = array(
                            "payer_email"=> $user->email,
                            //"payer_email" => "test_user_42481957@testuser.com",
                            "amount"=> $total,
                            "reason"=> "Compra Total Fast",
                            "payment_method_id"=> "bolbradesco",
                            "external_reference"=> $sale->id,
                            "notification_url" => SITE_URL . "APIs/marcadopago_notificacao.php"
                        );

                        try{
                            $result = $paymentManager->executePost($uri, $data);
                        }catch (Exception $e){
                            $result = array('status' => 0);
                        }

                        if(isset($result['response']['activation_uri'])){
                            $manager->cleanCart($userId);
                            MercadoPagoManager::setPaymentStatus($sale->id, 5, $result['response']['payment_id']);
                            echo($result['response']['activation_uri']);

                        }else{
                            $salesManager->deleteincompleteSale($sale->id);
                            echo("0"); //Erro ao pagar

                        }
                    }else{
                        $salesManager->deleteincompleteSale($sale->id);
                        echo('0'); //Erro ao criar token
                    }
                }
            }else{
                echo('0'); //Erro ao criar venda (sistema)
            }
        }else{
            echo('0'); //Carrinho vazio
        }
    }else{
        echo('0'); //Usuário não logado
    }
}else{
    echo('0'); //Método não informado
}