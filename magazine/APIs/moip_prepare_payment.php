<?php
if(isset($_GET['value']) && isset($_GET['userId']) && isset($_GET['reason']) && isset($_GET['token'])){

    try{session_start();}catch (Exception $e){}

    include_once('../Config.php');
    include_once('../Utils/Log.php');
    include_once(SITE_FOLDER . 'Libs/moip/autoload.inc.php');
    include_once(SITE_FOLDER . 'Managers/UsersManager.php');
    include_once(SITE_FOLDER . 'Managers/CartsManager.php');
    include_once(SITE_FOLDER . 'Managers/SalesManager.php');
    include_once(SITE_FOLDER . 'Managers/APIPaymentsManager.php');

    $manager = new UsersManager();
    $user = $manager->getUsersById($_GET['userId']);

    if($user->id > 0){

        $manager = new CartsManager();
        $salesManager = new SalesManager();

        $deliveryCost = 0;
        $cart = $manager->getCart($user->id);

        if($cart->id > 0){
            if(count($cart->items) > 0){
                foreach($cart->items as $item){
                    if($item->product->deliveryCost > $deliveryCost){
                        $deliveryCost = $item->product->deliveryCost;
                    }
                }

                $sale = $salesManager->convertCartToSale($cart, $deliveryCost);

                $moip = new Moip();
                $moip->setEnvironment('test');
                $moip->setCredential(array(
                    'key' => MOIP_KEY,
                    'token' => MOIP_TOKEN
                ));

                $sale->externalToken = $_GET['token'];
                APIPaymentsManager::setPaymentStatus($sale->id, 5, $sale->externalToken);

                $moip->setUniqueID($_GET['token']);
                $moip->setValue($_GET['value']);
                $moip->setReason($_GET['reason']);
                $moip->addPaymentWay('creditCard');
                $moip->addPaymentWay('billet');
                $moip->addParcel('1', MAX_INSTALLMENTS, MOIP_RATE);
                $moip->setPayer(array(
                    'name' => $user->name,
                    'email' => $user->email,
                    'payerId' => $user->id,
                    'billingAddress' => array(
                        'address' => $user->address,
                        'number' => '0',
                        'complement' => '-',
                        'city' => $user->city,
                        'neighborhood' => '-',
                        'state' => $user->state,
                        'country' => '-',
                        'zipCode' => $user->cep,
                        'phone' => $user->phone)));
                $moip->validate('Identification');

                $moip->send();
                $result = $moip->getAnswer();

                if($result->error == ""){
                    echo($result->token);
                }else{
                    $salesManager->deleteincompleteSale($sale->id);
                    echo('error_1');//Falha ao criar pagamento.
                }
            }else{
                echo('error_4');//Carrinho vazio.
            }
        }else{
            echo('error_3');//Carrinho não encontrado.
        }
    }else{
        echo('error_2');//Usuário não encontrado.
    }
}else{
    echo('error_0');//Parâmetros incorretos.
}