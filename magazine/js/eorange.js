function httpGet(theUrl){
    var xmlHttp = null;
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false );
    xmlHttp.send();
    return xmlHttp.responseText;
}

function httpGetCallback(theUrl, callback){
    var xmlHttp = null;
    xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange(callback);
    xmlHttp.open( "GET", theUrl, true );
    xmlHttp.send();
    return xmlHttp.responseText;
}