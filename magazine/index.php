<?php
error_reporting(E_ERROR | E_PARSE);
session_start();

include_once('Config.php');
include_once('texts-pt.php');
include_once(SITE_FOLDER . 'Builders/HomeBuilder.php');
include_once(SITE_FOLDER . 'Builders/CategoryBuilder.php');
include_once(SITE_FOLDER . 'Builders/ProductBuilder.php');
include_once(SITE_FOLDER . 'Builders/SearchBuilder.php');
include_once(SITE_FOLDER . 'Builders/AdminBuilder.php');
include_once(SITE_FOLDER . 'Builders/CartBuilder.php');
include_once(SITE_FOLDER . 'Builders/UserBuilder.php');
include_once(SITE_FOLDER . 'Builders/InfosBuilder.php');
include_once(SITE_FOLDER . 'Builders/NewslettersBuilder.php');
include_once(SITE_FOLDER . 'Builders/MktBuilder.php');
include_once(SITE_FOLDER . 'Builders/SupportBuilder.php');
include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'Utils/Encrypter.php');
include_once(SITE_FOLDER . 'Utils/Log.php');
include_once(SITE_FOLDER . 'Utils/GeneralUtils.php');
include_once(SITE_FOLDER . 'Utils/StringUtils.php');
include_once(SITE_FOLDER . 'Utils/ParametersParser.php');

$parser = new ParametersParser();
$getParams = $parser->parseGet();
$postParams = $parser->parsePost();

$isAdm = false;
$isMkt = false;
$isSupport = false;
$showBanner = false;

//Verifica se existe parâmetros GET para carregar a página
if(isset($getParams['builder'])){
    switch($getParams['builder']){
        case 'pesquisa':
            $builder = new SearchBuilder();
            break;

        case 'produto':
            $builder = new ProductBuilder();
            break;

        case 'categoria':
            $builder = new CategoryBuilder();
            break;

        case 'inicio':
            $showBanner = true;
            $builder = new HomeBuilder();
            break;

        case 'cart':
            $builder = new CartBuilder();
            break;

        case 'user':
            $builder = new UserBuilder();
            break;

        case 'page':
            $builder = new InfosBuilder();
            break;

        case 'news':
            $builder = new NewslettersBuilder();
            break;

        case 'adm':
            $isAdm = true;
            $builder = new AdminBuilder();
            break;

        case 'mkt':
            $isMkt = true;
            $builder = new MktBuilder();
            break;

        case 'support':
            $isSupport = true;
            $builder = new SupportBuilder();
            break;

        default:
            $showBanner = true;
            $builder = new HomeBuilder();
            break;
    }
}else{
    $builder = new HomeBuilder();
    $showBanner = true;
}
$builder->loadData($getParams, $postParams); ?><!DOCTYPE html>
<html lang="pt-br" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo($builder->pageName);?>">
    <meta name="author" content="<?php echo(SITE_NAME);?>">

    <!-- FACEBOOK METATAGS -->
    <meta property="og:title" content="<?php echo($builder->pageName);?>">
    <meta property="og:site_name" content="<?php echo(SITE_NAME);?>">

    <title><?php echo($builder->pageName);?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>css/gallery.prefixed.css">
    <link rel="shortcut icon" href="<?php echo(SITE_URL . 'Images/');?>favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>Libs/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <?php
    //Inclui na página o script do pixel do Facebook configurado no builder.
    echo($builder->facebookPixel); ?>
</head>

<body style="font-family: Arial, Helvetica, sans-serif !important">
<?php //var_dump($getParams); ?>
<?php //var_dump($postParams); ?>
<?php //Inclui na página o topo.?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo(SITE_URL)?>js/bootstrap.js"></script>
<script src="<?php echo(SITE_URL)?>js/eorange.js"></script>
<script src="<?php echo(SITE_URL)?>js/jquery.maskedinput.min.js"></script>
<script src="<?php echo(SITE_URL)?>Libs/fancybox/jquery.fancybox.js?v=2.1.5"></script>

<!-- LandPage -->
<?php include_once(SITE_FOLDER . 'Components\newsletter-form.php');?>

<?php
if($isAdm){
    include_once(SITE_FOLDER . 'Components/header-adm.php');
    echo('<div class="container" style="padding-top: 20px; margin: 0px; width: 100%;">');

} else if($isMkt){
    include_once(SITE_FOLDER . 'Components/header-mkt.php');
    echo('<div class="container" style="padding-top: 20px; margin: 0px; width: 100%;">');

} else if($isSupport){
    include_once(SITE_FOLDER . 'Components/header-support.php');
    echo('<div class="container" style="padding-top: 20px; margin: 0px; width: 100%;">');

} else{
    include_once(SITE_FOLDER . 'Components/header.php');
    echo('<div class="container" style="padding-top: 20px">');
}
?>
    <?php //inclui o conteúdo da página configurada no builder. ?>
    <?php $builder->build(); ?>
</div>

<?php //Scripts para JQuery e Bootstrap. NÃO REMOVER.?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-57580468-1', 'totalfast.com.br');
ga('send', 'pageview');

</script>

<?php //Inclui na página o script do analytics configurado no builder.?>
<?php echo($builder->analytics);?>
<?php echo($builder->pageScripts);?>

<?php //Inclui na página o rodapé.?>
<?php
if(!$isAdm && !$isMkt  && !$isSupport){
    include_once(SITE_FOLDER . 'Components/footer.php');
}
?>

<?php 
echo($_SESSION['console_log']);
$_SESSION['console_log'] = "";
?>



</body>
</html>