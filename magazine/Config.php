<?php
//Configurações gerais do site
//define('SITE_NAME', 'Bananas Shop');
define('SITE_NAME', 'Tatu Fast');

date_default_timezone_set('America/Sao_Paulo');

//Configurações do sistema
define('DATABASE_URL', 'localhost');

//define('DATABASE_USER', 'root');
//define('DATABASE_PASS', '');
//define('DATABASE_NAME', 'magazine');

define('DATABASE_USER', 'tatufast_root');
define('DATABASE_PASS', 'p@ssw0rd');
define('DATABASE_NAME', 'tatufast_db');

//define('DATABASE_USER', 'godevsco_magazin');
//define('DATABASE_PASS', 'p@ssw0rd');
//define('DATABASE_NAME', 'godevsco_magazine_db');

//define('DATABASE_USER', 'totalfas_magazin');
//define('DATABASE_PASS', 'p@ssw0rd');
//define('DATABASE_NAME', 'totalfas_magazine_db');

//define('DATABASE_USER', 'bananass_dba');
//define('DATABASE_PASS', 'p@ssw0rd');
//define('DATABASE_NAME', 'bananass_shop');

define('SITE_URL', 'http://www.tatufast.com/');
define('SITE_URL_SMALL', 'www.tatufast.com');
define('SITE_CONTACT_MAIL', 'falecom@tatufast.com');
//define('SITE_URL', 'http://www.godevs.com.br/bigvitrine/');
//define('SITE_URL', 'http://www.totalfast.com.br/');
//define('SITE_URL', 'http://www.godevs.com.br/bananas/');

define('DS', '/');
define('ROOT', $_SERVER['DOCUMENT_ROOT'] . DS);

//define('SITE_FOLDER', ROOT . 'bigvitrine' . DS);
define('SITE_FOLDER', ROOT);
define('IMAGES_FOLDER', SITE_FOLDER . 'Images' . DS);
define('IMAGES_URL', SITE_URL . 'Images' . DS);
define('PRODUCTS_IMAGES_FOLDER', IMAGES_FOLDER . 'Products' . DS);
define('PRODUCTS_IMAGES_URL', IMAGES_URL . 'Products' . DS);
define('CATEGORIES_IMAGES_FOLDER', IMAGES_FOLDER . 'categories' . DS);
define('CATEGORIES_IMAGES_URL', IMAGES_URL . 'categories' . DS);
define('BANNERS_IMAGES_FOLDER', IMAGES_FOLDER . 'banners' . DS);
define('BANNERS_IMAGES_URL', IMAGES_URL . 'banners' . DS);

define('ADMIN_PAGINATION_COUNT', 20);
define("MAX_INSTALLMENTS", 12);
define("MIN_INSTALLMENT_VALUE", 30);

define("GATEWAY_INTEGRATION", "mp");

//Dados de Produção
define("MP_CLIENT_ID","4290476529623958");
define("MP_CLIENT_SECRET","iWzdFhpR3WtJUYZcXCxHPkzbqX0zocUy");
define("MP_PUBLIC_KEY", "66734eeb-383c-4400-a2b4-13b55612aebf");

define("MOIP_LOGIN", "");
define("MOIP_KEY", "");
define("MOIP_TOKEN", "");
define("MOIP_RATE", "2");//Taxa de juros MOIP


//Configurações sobre pagamento
/*
 * Status
 *
 * 0 - Não finalizado
 * 1 - Aguardando Pagamento
 * 2 - Falha no Pagamento
 * 3 - Pagamento Concluído
 * 4 - Pagamento Cancelado
 * 5 - Pagamento Pendente
 *
 */