<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 08/10/14
 * Time: 00:39
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Utils/StringUtils.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');
include_once(SITE_FOLDER . 'Entities/Product.php');
include_once(SITE_FOLDER . 'Entities/Category.php');
include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
include_once(SITE_FOLDER . 'Managers/BannersManager.php');

class SearchBuilder extends Builder{

    private $viewItems = array();

    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME . " - Pesquisa";

        $bannerManager = new BannersManager();

        if(isset($post['search']) && $post['search'] != ""){
            $search = preg_replace('/[^A-Za-z0-9\-]/', '-', $post['search']); // Removes special chars.
            header('Location: ' . SITE_URL . 'pesquisa/' . $search);

        }else if(isset($get['param1']) && $get['param1'] != ""){
            $this->viewItems = array();
            $search = str_replace("-", "%", $get['param1']);
            $productsManager = new ProductsManager();
            $this->viewItems['products'] = $productsManager->getProductBySearch($search, true);
            SessionManager::setTempAction(array("search" => str_replace("-", " ", $get['param1'])));

            $this->viewItems['seccondary-banners'] = $bannerManager->getBanners('seccondary');
            $this->viewItems['stripe-banners'] = $bannerManager->getBanners('stripe');

        }else{
            header('Location: ' . SITE_URL);
        }
    }

    public function build(){
        include_once("Pages/Search.php");
    }
}