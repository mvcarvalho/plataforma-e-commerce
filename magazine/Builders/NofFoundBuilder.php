<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 09/10/14
 * Time: 22:28
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');

class NofFoundBuilder extends Builder{

    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME . " - Página não encontrada";

    }

    public function build()
    {
        $content1 = "\"Conteúdo de testes.\"";
        include_once("Pages/Home.php");
    }
} 