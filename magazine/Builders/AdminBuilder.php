<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 12/10/14
 * Time: 12:26
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Utils/ACL.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');
include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'Managers/CouponsManager.php');
include_once(SITE_FOLDER . 'Managers/SystemUsersManager.php');

class AdminBuilder extends Builder{

    private $action = "";
    private $pageInclude = "";
    private $viewItems = array();
    private $entityValue;
    private $extraValue;
    private $categoriesList = "";

    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME . " - Administração";

        //Carrega o parâmetro da página de administração
        $this->setPage($get);

        //Carrega o parâmetro de ação
        $this->setAction($get);

        if(SessionManager::getAdminName() === ""){
            if($this->action != 'user-auth' && $this->action != 'user-login'){
                header('Location: ' . SITE_URL . 'adm/user/login');
            }else{
                if($this->action == 'user-login'){
                    $this->userLogin();
                }else if($this->action == 'user-auth'){
                    $this->userAuth($post);
                }
            }
        }else{
            //Carrega o parâmetro do valor da entidade
            $this->setEntityValue($get);

            //Carrega o parâmetro do valor extra da URL
            $this->setExtraValue($get);

            $this->executeAction($get, $post);
        }
    }

    public function build(){
        include_once(SITE_FOLDER . 'Pages/Admin/' . $this->pageInclude . '.php');
    }

    private function setPage($get){
        if(isset($get['param1'])){
            switch($get['param1']){
                case 'user':
                    $this->pageInclude = 'user';
                    break;

                case 'coupon':
                    $this->pageInclude = 'coupon';
                    break;

                case 'users-adm':
                    $this->pageInclude = 'users-adm';
                    break;

                case 'sales':
                    $this->pageInclude = 'sales';
                    break;

                case 'reports':
                    $this->pageInclude = 'reports';
                    break;

                case 'product':
                    $this->pageInclude = 'product';
                    break;

                case 'category':
                    $this->pageInclude = 'category';
                    break;

                case 'attribute':
                    $this->pageInclude = 'attribute';
                    break;

                case 'attribute-option':
                    $this->pageInclude = 'attribute-option';
                    break;

                default:
                    $this->pageInclude = 'dashboard';
                    break;
            }
        }else{
            $this->pageInclude = 'dashboard';
        }
    }

    private function setAction($get){
        if(isset($get['param2'])){
            switch($get['param2']){
                case 'auth':
                    $this->action = $this->pageInclude . '-auth';
                    break;

                case 'login':
                    $this->action = $this->pageInclude . '-login';
                    break;

                case 'logout':
                    $this->action = $this->pageInclude . '-logout';
                    break;

                case 'save':
                    $this->action = $this->pageInclude . '-save';
                    break;

                case 'view':
                    $this->action = $this->pageInclude . '-view';
                    break;

                case 'add':
                    $this->action = $this->pageInclude . '-add';
                    break;

                case 'add-info':
                    $this->action = $this->pageInclude . '-add-info';
                    break;

                case 'add-img':
                    $this->action = $this->pageInclude . '-add-img';
                    break;

                case 'remove-info':
                    $this->action = $this->pageInclude . '-remove-info';
                    break;

                case 'remove-img':
                    $this->action = $this->pageInclude . '-remove-img';
                    break;

                case 'remove':
                    $this->action = $this->pageInclude . '-remove';
                    break;

                case 'active':
                    $this->action = $this->pageInclude . '-active';
                    break;

                case 'deactivate':
                    $this->action = $this->pageInclude . '-deactivate';
                    break;

                case 'tracking-add':
                    $this->action = $this->pageInclude . '-tracking-add';
                    break;

                case 'add-attribute':
                    $this->action = $this->pageInclude . '-add-attribute';
                    break;

                case 'remove-attribute':
                    $this->action = $this->pageInclude . '-remove-attribute';
                    break;

                case 'salesbydate':
                    $this->action = $this->pageInclude . '-salesbydate';
                    break;

                case 'usersregistered':
                    $this->action = $this->pageInclude . '-usersregistered';
                    break;

                case 'exportnews':
                    $this->action = $this->pageInclude . '-exportnews';
                    break;

                default:
                    $this->action = $this->pageInclude . '-list';
                    break;
            }
        }else{
            $this->action = $this->pageInclude . '-list';
        }
    }

    private function setEntityValue($get){
        if(isset($get['param3'])){
            $this->entityValue = $get['param3'];
        }else{
            $this->entityValue = 0;
        }
    }

    private function setExtraValue($get){
        if(isset($get['param4'])){
            $this->extraValue = $get['param4'];
        }else{
            $this->extraValue = 0;
        }
    }

    private function executeAction($get, $post){


        switch($this->action){

            case 'coupon-list':
                $this->couponsList();
                break;

            case 'coupon-add':
                $this->couponAdd();
                break;

            case 'coupon-view':
                $this->couponView();
                break;

            case 'coupon-save':
                $this->couponSave($post);
                break;


            case 'user-logout':
                $this->userLogout();
                break;

            case 'user-login':
                $this->userLogin();
                break;

            case 'users-adm-list':
                $this->usersAdmList();
                break;

            case 'users-adm-view':
                $this->usersAdmView($post);
                break;

            case 'users-adm-deactivate':
                $this->usersAdmDeactivate();
                break;

            case 'users-adm-add':
                $this->usersAdmAdd($post);
                break;

            // SALES
            case 'sales-list':
                $this->salesList($post);
                break;

            case 'sales-view':
                $this->salesView();
                break;

            case 'sales-tracking-add':
                $this->salesTrackingAdd($post);
                break;


            //REPORTS
            case 'reports-salesbydate':
                $this->salesExport($post);
                break;

            case 'reports-usersregistered':
                $this->usersExport();
                break;

            case 'reports-exportnews':
                $this->exportNews($post);
                break;

            // PRODUCTS
            case 'product-add':
                $this->productAdd();
                break;

            case 'product-save':
                $this->produtcSave($post);
                break;

            case 'product-deactivate':
                $this->produtcDeactivate();
                break;

            case 'product-view':
                $this->productView();
                break;

            case 'product-list':
                $this->productList();
                break;

            case 'product-add-attribute':
                $this->productAttributeAdd($post);
                break;

            case 'product-add-img':
                $this->productAddImg($post);
                break;

            case 'product-remove-img':
                $this->productRemoveImg();
                break;

            case 'product-remove-attribute':
                $this->productAttributeRemove();
                break;

            // CATEGORIES
            case 'category-add':
                $this->categoryAdd();
                break;

            case 'category-save':
                $this->categorySave($post);
                break;

            case 'category-view':
                $this->categoryView();
                break;

            case 'category-list':
                $this->categoryList();
                break;

            case 'category-active':
                $this->categoryActive();
                break;

            // ATTRIBUTES
            case 'attribute-add':
                $this->attributeAdd();
                break;

            case 'attribute-save':
                $this->attributeSave($post);
                break;

            case 'attribute-view':
                $this->attributeView();
                break;

            case 'attribute-list':
                $this->attributeList();
                break;

            case 'attribute-option-add':
                $this->attributeOptionAdd($post);
                break;

            case 'attribute-option-remove':
                $this->attributeOptionRemove();
                break;

            case 'attribute-option-view':
                $this->attributeOptionView($post);
                break;

            case 'product-add-info':
                $this->addProductInfo($post);
                break;

            case 'product-remove-info':
                $this->removeProductInfo($post);
                break;

            default:
                break;
        }
    }

    /****************************************************************
     * COUPONS ACTIONS
     */
    private function couponsList(){
        $manager = new CouponsManager();
        $coupons = $manager->getAllCoupons();
        $this->viewItems['coupons'] = $coupons;
        $this->pageInclude .= '-list';
    }

    private function couponView(){
        $manager = new CouponsManager();
        $coupon = $manager->getCouponById($this->entityValue);
        $this->viewItems['coupon'] = $coupon;
        $this->viewItems['formaction'] = SITE_URL . 'adm/coupon/save';
        $this->pageInclude .= '-view';
    }

    private function couponAdd(){
        $this->viewItems['formaction'] = SITE_URL . 'adm/coupon/save';
        $this->pageInclude .= '-add';
    }

    private function couponSave($post){
        $manager = new CouponsManager();
        $coupon = new Coupon();
        if(isset($post['id'])){
            $coupon->id = $post['id'];
        }else{
            $coupon->id = 0;
        }
        $coupon->key = $post['key'];
        $coupon->off = $post['off'];
        $coupon->offType = $post['off-type'];
        $coupon->minValue = $post['min-value'];
        $coupon->validity = strtotime(str_replace("/", "-", $post['validity']));
        $manager->saveCoupon($coupon);
        header('Location: ' . SITE_URL . 'adm/coupon');
    }


    /****************************************************************
     * USER ACTIONS
     */
    private function userLogin(){
        $this->pageInclude .= '-login';
        $this->viewItems['login_action'] = SITE_URL . 'adm/user/auth';
    }

    private function userAuth($post){
        //$acl = new ACL();
        $manager = new SystemUsersManager();
        $user = $manager->getByLoginAndPassword($post['login'], md5($post['password']));

        if($user->id > 0 && $user->isActive){
            SessionManager::setAdminName($post['login']);
            SessionManager::setAdminId($user->id);
            header('Location: ' . SITE_URL . 'adm/dashboard');

        }else{
            header('Location: ' . SITE_URL . 'adm/user/login');
        }
    }

    private function userLogout(){
        SessionManager::setAdminName("");
        SessionManager::setAdminId(0);
        header('Location: ' . SITE_URL . 'adm/user/login');
    }

    private function usersAdmList(){
        $manager = new SystemUsersManager();
        $this->viewItems['users'] = $manager->getAll();
        $this->pageInclude .= '-list';
    }

    private function usersAdmView($post){
        $manager = new SystemUsersManager();
        if(isset($post['login']) && isset($post['password']) && isset($post['name']) && isset($post['status'])){
            $user = new SystemUser();
            $user->id = $this->entityValue;
            $user->login = $post['login'];
            $user->password = md5($post['password']);
            $user->name = $post['name'];
            $user->isActive = $post['status'];
            $manager->saveUser($user);
            header('Location: ' . SITE_URL . 'adm/users-adm/list');

        }else{
            $this->viewItems['user'] = $manager->getById($this->entityValue);
            $this->pageInclude .= '-view';
        }
    }

    private function usersAdmAdd($post){
        if(isset($post['login']) && isset($post['password']) && isset($post['name']) && isset($post['is-active'])){
            $manager = new SystemUsersManager();
            $user = new SystemUser();
            $user->login = $post['login'];
            $user->password = md5($post['password']);
            $user->name = $post['name'];
            $user->isActive = $post['is-active'];
            $manager->saveUser($user);
            header('Location: ' . SITE_URL . 'adm/users-adm/list');

        }else{
            $this->pageInclude .= '-add';
        }
    }

    private function usersAdmDeactivate(){
        $manager = new SystemUsersManager();
        $user = $manager->getById($this->entityValue);
        if($user->isActive == 1){
            $user->isActive = 0;

        }else{
            $user->isActive = 1;
        }

        $manager->saveUser($user);
        header('Location: ' . SITE_URL . 'adm/users-adm/list');
    }

    /****************************************************************
     * SALES ACTIONS
     */
    private function salesList($post){
        try{
            include_once(SITE_FOLDER . 'Managers/SalesManager.php');
            include_once(SITE_FOLDER . 'Entities/Sale.php');

            $this->pageScripts  = "<script type='text/javascript'>";
            $this->pageScripts .= "     function ordenacao(elemento){";
            $this->pageScripts .= "         var form = document.forms[0];";
            $this->pageScripts .= "         form.submit();";
            $this->pageScripts .= "     }";
            $this->pageScripts .= "</script>";

            $this->pageInclude = $this->pageInclude . "-list";
            $this->viewItems = array();

            $manager = new SalesManager();

            if($post['orderBy'] > -1) {


                switch($post['orderBy']) {

                    case "value_desc":
                        $post['orderBy'] = "value DESC";
                        break;

                    case "value_asc":
                        $post['orderBy'] = "value ASC";
                        break;

                    case "date":
                        $post['orderBy'] = null;
                        break;

                    default :
                        $post['orderBy'] .= " ASC";
                }

                if(isset($post['startdate']) && isset($post['enddate']) && isset($post['status'])) {
                    $sales = $manager->getSalesByPeriod(strtotime($post['startdate']), strtotime($post['enddate']), $post['status'], $post['orderBy']);
                }
            } else if(isset($post['startdate']) && isset($post['enddate']) && isset($post['status'])) {

                $sales = $manager->getSalesByPeriod(strtotime($post['startdate']), strtotime($post['enddate']), $post['status']);
            }  else {

                $sales = $manager->getAllSales(-1);
            }

            $this->viewItems['sales'] = $sales;
            $this->viewItems['viewLink'] = SITE_URL . 'adm' . DS . 'sales' . DS . 'view' . DS;
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function salesView(){
        try{
            include_once(SITE_FOLDER . 'Managers/SalesManager.php');
            include_once(SITE_FOLDER . 'Managers/UsersManager.php');
            include_once(SITE_FOLDER . 'Entities/Sale.php');

            $this->pageInclude = $this->pageInclude . "-view";
            $this->viewItems = array();

            $manager = new SalesManager();
            $usersManager = new UsersManager();
            $sale = $manager->getSaleById($this->entityValue);
            $sale->user = $usersManager->getUsersById($sale->userId);

            $this->viewItems['sale'] = $sale;
            $this->viewItems['user'] = $sale->user;
            $this->viewItems['viewLink'] = SITE_URL . 'adm' . DS . 'sales' . DS . 'tracking-add' . DS;
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function salesExport($post){
        try{
            include_once(SITE_FOLDER . 'Managers/ReportsManager.php');

            if(isset($post['startdate']) && isset($post['enddate']) && strlen($post['startdate']) > 7  && strlen($post['enddate']) > 7){

                if(isset($post['status'])){
                    $status = $post['status'];
                }else{
                    $status = -1;
                }

                $manager = new ReportsManager();
                $report = $manager->exportSalesByPeriod(strtotime($post['startdate']), strtotime($post['enddate']), $status);

                $this->viewItems = array();
                $this->viewItems['reportLink'] = SITE_URL . 'Reports/' . $report;
                $this->pageInclude = 'reports';
            }
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function usersExport(){
        try{
            include_once(SITE_FOLDER . 'Managers/ReportsManager.php');
            $manager = new UsersManager();
            $report = $manager->exportUsers();

            $this->viewItems = array();
            $this->viewItems['reportLink'] = SITE_URL . 'Reports/' . $report;
            $this->pageInclude = 'reports';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function exportNews($post) {

        try{
            include_once(SITE_FOLDER . 'Managers/ReportsManager.php');
            $manager = new ReportsManager();
            $arrayCategories = array();

            $keys = array_keys($post);
            consoleLog("RUBENS exportNews", "Teste");
            $i = 0;
            foreach($post as $value) {
                consoleLog(" -- KEY ".$keys[$i]);
                if(strripos($keys[$i],"CAT") > -1) {

                    consoleLog("Valor " . $value);
                    $arrayCategories[] = $value;
                }
                $i++;
            }

            $report = $manager->exportNews($arrayCategories);

            $this->viewItems = array();
            $this->viewItems['reportLink'] = SITE_URL . 'Reports/' . $report;
            $this->pageInclude = 'reports';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function salesTrackingAdd($post){
        try{
            include_once(SITE_FOLDER . 'Managers/SalesManager.php');
            include_once(SITE_FOLDER . 'Entities/Sale.php');

            $manager = new SalesManager();
            $sale = $manager->getSaleById($this->entityValue);
            $sale->trackingCode = $post['tracking'];
            $sale->status = 4;
            $manager->updateSaleTrackingCode($sale);

            header('Location: ' . SITE_URL . 'adm' . DS . 'sales' . DS . 'view' . DS . $this->entityValue);
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    /****************************************************************
     * PRODUCTS ACTIONS
     */
    private function productAdd(){
        try{
            include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
            include_once(SITE_FOLDER . 'Entities/Category.php');

            $this->pageInclude = $this->pageInclude . "-add";
            $this->viewItems = array();

            $categoriesManager = new CategoriesManager();
            $categories = $categoriesManager->getAllCategories();

            $this->viewItems['categories'] = $categories;
            $this->viewItems['formaction'] = SITE_URL . 'adm/product/save';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function productView(){
        try{
            include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
            include_once(SITE_FOLDER . 'Entities/Category.php');
            include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
            include_once(SITE_FOLDER . 'Entities/Product.php');
            include_once(SITE_FOLDER . 'Managers/AttributesManager.php');
            include_once(SITE_FOLDER . 'Entities/Attribute.php');
            include_once(SITE_FOLDER . 'Managers/QuickInfosManager.php');
            include_once(SITE_FOLDER . 'Entities/QuickInfo.php');
            include_once(SITE_FOLDER . 'Managers/FeaturesManager.php');
            include_once(SITE_FOLDER . 'Entities/Feature.php');

            $this->pageInclude = $this->pageInclude . "-view";
            $this->viewItems = array();

            $categoriesManager = new CategoriesManager();
            $categories = $categoriesManager->getAllCategories();

            $productsManager = new ProductsManager();
            $product = $productsManager->getProductById($this->entityValue);

            $attributesManager = new AttributesManager();
            $attributes = $attributesManager->getAllAttributes();

            $infosManager = new QuickInfosManager();
            $infos = $infosManager->getQuickInfoByProductId($this->entityValue);

            $featuresManager = new FeaturesManager();
            $features = $featuresManager->getFeaturesByProductId($product->id);

            if(!isset($features[0])){
                $feature1 = new Feature();
                $features[] = $feature1;
            }

            if(!isset($features[1])){
                $feature2 = new Feature();
                $features[] = $feature2;
            }

            $this->viewItems['product'] = $product;
            $this->viewItems['categories'] = $categories;
            $this->viewItems['attributes'] = $attributes;
            $this->viewItems['infos'] = $infos;
            $this->viewItems['features'] = $features;
            $this->viewItems['formaction'] = SITE_URL . 'adm/product/save';
            $this->viewItems['formaction-add'] = SITE_URL . 'adm/product/add-attribute/' . $product->id;
            $this->viewItems['formaction-remove'] = SITE_URL . 'adm/product/remove-attribute/' . $product->id . '/';
            $this->viewItems['formaction-add-info'] = SITE_URL . 'adm/product/add-info/' . $product->id . '/';
            $this->viewItems['formaction-remove-info'] = SITE_URL . 'adm/product/remove-info/' . $product->id . '/';
            $this->viewItems['formaction-add-img'] = SITE_URL . 'adm/product/add-img';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function productList(){
        try{
            include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
            include_once(SITE_FOLDER . 'Entities/Product.php');

            $this->pageInclude = $this->pageInclude . "-list";
            $this->viewItems = array();

            $productsManager = new ProductsManager();
            $products = $productsManager->getAllProducts();

            $this->viewItems['products'] = $products;
            $this->viewItems['viewLink'] = SITE_URL . 'adm/product/view/';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function produtcSave($post){
        try{
            include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
            include_once(SITE_FOLDER . 'Managers/FeaturesManager.php');
            include_once(SITE_FOLDER . 'Entities/Product.php');
            include_once(SITE_FOLDER . 'Utils/FileUtils.php');

            $product = new Product();
			$manager = new ProductsManager();
            $product->id = $post['id'];

			if($product->id > 0){
				$product = $manager->getProductById($product->id);
			}

            $product->name = $post['name'];
            $product->description = $post['description'];
            $product->rules = $post['rules'];
            $product->isActive = true;
            $product->normalPrice = $post['normalprice'];
            $product->offerPrice = $post['offerprice'];
            $product->categoryId = $post['category'];
            $product->deliveryCost = $post['delivery_cost'];
            $product->deliveryDays = $post['delivery_days'];
            $product->validity = date('Y-m-d h:i:s', strtotime($post['validity']));

            $fileName = "";
            if(count($_FILES) > 0 && $_FILES['image']['size'] > 1){
                $fileName = 'product-' . time() . '.jpg';
                $fileUtils = new FileUtils();
                $fileUtils->upload($_FILES['image'], PRODUCTS_IMAGES_FOLDER, $fileName);

                if($product->image != $fileName && strlen($product->image) > 10 && $product->image != 'no-image.png'){
                    try{
                        unlink(PRODUCTS_IMAGES_FOLDER . $product->image);
                    }catch (Exception $e){}
                }
            }
            $product->image = $fileName;
            $product->shortDescription = $post['shortdescription'];
            $product->promotionalText = $post['promotionaltext'];
            $product->technicalData = $post['tecnicaldata'];
            $product->itemsIncluded = $post['includeditems'];

            $product = $manager->saveProduct($product);

            $featureManager = new FeaturesManager();
            if(isset($post['feature-1-id']) && isset($post['feature-1']) && isset($post['feature-1-options'])){
                if($post['feature-1-id'] > 0 && ($post['feature-1'] == "" || $post['feature-1-options'] == "")){
                    $featureManager->deleteFeatureById($post['feature-1-id']);

                } else if($post['feature-1'] != "" && $post['feature-1-options'] != ""){
                    $feature = new Feature();
                    $feature->id = $post['feature-1-id'];
                    $feature->name = $post['feature-1'];
                    $feature->productId = $product->id;
                    $feature = $featureManager->saveFeature($feature);
                    $featureManager->removeAllFeatureoptions($feature->id);

                    $options = explode(PHP_EOL, $post['feature-1-options']);
                    foreach($options as $option){
                        $featureOption = new FeatureOption();
                        $featureOption->featureId = $feature->id;
                        $featureOption->name = $option;
                        $featureManager->addFeatureOption($featureOption);
                    }
                }
            }

            if(isset($post['feature-2-id']) && isset($post['feature-2']) && isset($post['feature-2-options'])){
                if($post['feature-2-id'] > 0 && ($post['feature-2'] == "" || $post['feature-2-options'] == "")){
                    $featureManager->deleteFeatureById($post['feature-2-id']);

                } else if($post['feature-2'] != "" && $post['feature-2-options'] != ""){
                    $feature = new Feature();
                    $feature->id = $post['feature-2-id'];
                    $feature->name = $post['feature-2'];
                    $feature->productId = $product->id;
                    $feature = $featureManager->saveFeature($feature);
                    $featureManager->removeAllFeatureoptions($feature->id);

                    $options = explode(PHP_EOL, $post['feature-2-options']);
                    foreach($options as $option){
                        $featureOption = new FeatureOption();
                        $featureOption->featureId = $feature->id;
                        $featureOption->name = $option;
                        $featureManager->addFeatureOption($featureOption);
                    }
                }
            }

            $this->productList();
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function productAddImg($post){
        $manager = new ProductsManager();
        $product = $manager->getProductById($post['prodId']);
        if($product->id > 0){
            $fileName = "";
            if(count($_FILES) > 0 && $_FILES['image']['size'] > 1){
                $fileName = 'product-' . $product->id . '-' . time() . '.jpg';
                $fileUtils = new FileUtils();
                $fileUtils->upload($_FILES['image'], PRODUCTS_IMAGES_FOLDER, $fileName);
            }

            $manager->addProductImage($product->id, $fileName);

            header("Location: " . SITE_URL . 'adm/product/view/' . $product->id);
        }
    }

    private function productRemoveImg(){
        try{
            unlink(PRODUCTS_IMAGES_FOLDER . $this->entityValue . '.jpg');
        }catch (Exception $e){}

        $manager = new ProductsManager();
        $prodId = $manager->selectProductIdByImageName($this->entityValue . '.jpg');
        $manager->removeProductImage($prodId , $this->entityValue . '.jpg');
        header("Location: " . SITE_URL . 'adm/product/view/' . $prodId);
    }

    private function produtcDeactivate(){
        try{
            include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
            $manager = new ProductsManager();
            $manager->activateDeactivateProduct($this->entityValue);

            $this->productList();
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function productAttributeAdd($post){
        include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
        $manager = new ProductsManager();
        $manager->addProductAttribute($this->entityValue, $post['attribute']);

        header('Location: ' . SITE_URL . 'adm/product/view/' . $this->entityValue);
    }

    private function productAttributeRemove(){
        include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
        $manager = new ProductsManager();
        $manager->removeProductAttribute($this->entityValue, $this->extraValue);

        header('Location: ' . SITE_URL . 'adm/product/view/' . $this->entityValue);
    }

    /****************************************************************
     * CATEGORIES ACTIONS
     */
    private function categoryAdd(){
        try{
            include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
            include_once(SITE_FOLDER . 'Entities/Category.php');

            $this->pageInclude = $this->pageInclude . "-add";
            $this->viewItems = array();

            $categoriesManager = new CategoriesManager();

            $categories = $categoriesManager->getAllCategories();

            $this->viewItems['categories'] = $categories;
            $this->viewItems['formaction'] = SITE_URL . 'adm/category/save';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function categoryView(){
        try{
            include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
            include_once(SITE_FOLDER . 'Entities/Category.php');

            $this->pageInclude = $this->pageInclude . "-view";
            $this->viewItems = array();

            $categoriesManager = new CategoriesManager();
            $categories = $categoriesManager->getAllCategories();
            $category = $categoriesManager->getCategoryById($this->entityValue);

            $this->viewItems['categories'] = $categories;
            $this->viewItems['category'] = $category;
            $this->viewItems['formaction'] = SITE_URL . 'adm/category/save';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function categoryList(){
        try{
            include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
            include_once(SITE_FOLDER . 'Entities/Category.php');

            $this->pageInclude = $this->pageInclude . "-list";
            $this->viewItems = array();
            $this->categoriesList = "";

            $categoriesManager = new CategoriesManager();
            $categories = $categoriesManager->getAllCategoriesStructure();

            consoleLog($categories);

            $this->viewItems['viewLink'] = SITE_URL . 'adm/category/view/';

            $this->buildCategoryList("", $categories);
            $this->viewItems['categories'] = $categories;
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function categorySave($post){
        try{
            include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
            include_once(SITE_FOLDER . 'Entities/Category.php');
            include_once(SITE_FOLDER . 'Utils/FileUtils.php');

            $fileName = "";
            if(count($_FILES) > 0 && $_FILES['image']['size'] > 1){
                $fileName = 'category_' . time() . '.jpg';
                $fileUtils = new FileUtils();
                $fileUtils->upload($_FILES['image'], CATEGORIES_IMAGES_FOLDER, $fileName);
            }

            $manager = new CategoriesManager();
            $category = new Category();
            $category->id = $post['id'];

            if($category->id > 0){
                $category = $manager->getCategoryById($category->id);
            }

            $category->name = $post['name'];
            $category->description = $post['description'];
            $category->parentCategoryId = $post['category'];

            if($fileName != ""){
                $category->image = $fileName;
            }

            $manager->saveCategory($category);

            $this->categoryList();
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function categoryActive(){
        try{
            include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
            include_once(SITE_FOLDER . 'Entities/Category.php');
            include_once(SITE_FOLDER . 'Utils/FileUtils.php');
            $manager = new CategoriesManager();
            $category = new Category();
            $category->id = $this->entityValue;
            if($category->id > 0){
                $category = $manager->getCategoryById($category->id);
            }
            if($category->isActive == 1){
                $category->isActive = 0;
            }else{
                $category->isActive = 1;
            }
            $manager->saveCategory($category);
            header('Location: ' . SITE_URL . 'adm/category');
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function buildCategoryList($parentCategoryName, $categories){
        foreach($categories as $category){
            $name = $parentCategoryName . " - " . $category->name;
            $html = '<tr>';
            $html .= '<td>' . $parentCategoryName . ' - ' . $category->name . '</td>';
            $html .= '<td>' . $category->description . '</td>';
            $html .= '<td style="text-align: center;"><a href="' . $this->viewItems['viewLink'] . $category->id . '">[View]</a></td>';
            $html .= '</tr>';

            $this->categoriesList .= $html;
            $this->buildCategoryList($name, $category->subcategories);
        }
    }

    /****************************************************************
     * ATTRIBUTES ACTIONS
     */
    private function attributeAdd(){
        try{
            $this->pageInclude = $this->pageInclude . "-add";
            $this->viewItems = array();
            $this->viewItems['formaction'] = SITE_URL . 'adm/attribute/save';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function attributeView(){
        try{
            include_once(SITE_FOLDER . 'Managers/AttributesManager.php');
            include_once(SITE_FOLDER . 'Entities/Attribute.php');

            $this->pageInclude = $this->pageInclude . "-view";
            $this->viewItems = array();

            $attributesManager = new AttributesManager();
            $attribute = $attributesManager->getAttributeById($this->entityValue);

            $this->viewItems['attribute'] = $attribute;
            $this->viewItems['formaction'] = SITE_URL . 'adm/attribute/save';
            $this->viewItems['formaction-add'] = SITE_URL . 'adm/attribute-option/add/';
            $this->viewItems['formaction-remove'] = SITE_URL . 'adm/attribute-option/remove/' . $attribute->id . '/';
            $this->viewItems['formaction-view'] = SITE_URL . 'adm/attribute-option/view/';
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function attributeList(){
        try{
            include_once(SITE_FOLDER . 'Managers/AttributesManager.php');
            include_once(SITE_FOLDER . 'Entities/Attribute.php');

            $this->pageInclude = $this->pageInclude . "-list";
            $this->viewItems = array();

            $manager = new AttributesManager();
            $attributes = $manager->getAllAttributes();

            $this->viewItems['viewLink'] = SITE_URL . 'adm/attribute/view/';
            $this->viewItems['attributes'] = $attributes;
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function attributeSave($post){
        try{
            include_once(SITE_FOLDER . 'Managers/AttributesManager.php');
            include_once(SITE_FOLDER . 'Entities/Attribute.php');

            $attribute = new Attribute();
            $attribute->id = $post['id'];
            $attribute->name = $post['name'];
            $attribute->observation = $post['observation'];
            $attribute->isRequired = $post['isrequired'];

            $manager = new AttributesManager();
            $manager->saveAttribute($attribute);

            header('Location: ' . SITE_URL . 'adm/attribute/');
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function attributeOptionAdd($post){
        try{
            include_once(SITE_FOLDER . 'Managers/AttributesManager.php');
            include_once(SITE_FOLDER . 'Entities/AttributeOption.php');

            $attribute = new AttributeOption();
            $attribute->id = $post['id'];
            $attribute->attributeId = $post['attributeid'];
            $attribute->name = $post['name'];
            $attribute->observation = $post['observation'];
            $attribute->priceChange = $post['pricechange'];

            $manager = new AttributesManager();
            $manager->addAttributeOption($attribute);

            header('Location: ' . SITE_URL . 'adm/attribute/view/' . $post['attributeid']);
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function attributeOptionRemove(){
        try{
            include_once(SITE_FOLDER . 'Managers/AttributesManager.php');
            include_once(SITE_FOLDER . 'Entities/AttributeOption.php');

            $manager = new AttributesManager();
            $option = $manager->getAttributeOption($this->extraValue);
            $manager->removeAttributeOption($option->id);

            header('Location: ' . SITE_URL . 'adm/attribute/view/' . $option->attributeId);
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function attributeOptionView($post = null){
        try{
            include_once(SITE_FOLDER . 'Managers/AttributesManager.php');
            include_once(SITE_FOLDER . 'Entities/AttributeOption.php');

            $manager = new AttributesManager();

            if(isset($post['id']) && isset($post['name']) && isset($post['pricechange']) && isset($post['observation'])){
                $option = $manager->getAttributeOption($post['id']);
                $option->name = $post['name'];
                $option->priceChange = $post['pricechange'];
                $option->observation = $post['observation'];
                $manager->updateAttributeOption($option);

                header('Location: ' . SITE_URL . 'adm/attribute/view/' . $option->attributeId);
            }else{
                $option = $manager->getAttributeOption($this->entityValue);
                $this->viewItems['option'] = $option;
                $this->viewItems['formaction-edit'] = SITE_URL . 'adm/attribute-option/view/' . $option->id;
                $this->pageInclude .= '-view';
            }
        }catch (Exception $e){
            echo($e->getMessage());
        }
    }

    private function addProductInfo($post){
        try{
            include_once(SITE_FOLDER . 'Managers/QuickInfosManager.php');
            include_once(SITE_FOLDER . 'Entities/QuickInfo.php');

            if(isset($post['name']) && isset($post['description'])){
                $manager = new QuickInfosManager();
                $info = new QuickInfo();
                $info->description = $post['description'];
                $info->name = $post['name'];
                $info->productId = $this->entityValue;

                $manager->saveQuickInfo($info);
            }

            header('Location: ' . SITE_URL . 'adm/product/view/' . $this->entityValue);
        }catch (Exception $e){

        }
    }

    private function removeProductInfo($post){
        try{
            include_once(SITE_FOLDER . 'Managers/QuickInfosManager.php');
            include_once(SITE_FOLDER . 'Entities/QuickInfo.php');

            $manager = new QuickInfosManager();
            $manager->removeAttributeOption($this->extraValue);

            header('Location: ' . SITE_URL . 'adm/product/view/' . $this->entityValue);
        }catch (Exception $e){

        }
    }
}