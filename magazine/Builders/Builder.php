<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 08/10/14
 * Time: 00:04
 */

include_once(SITE_FOLDER . 'Config.php');

abstract class Builder {

    public $pageName = SITE_NAME;
    public $pageStylesheets = "";
    public $pageScripts = "";
    public $analytics = "";
    public $facebookPixel = "";

    public abstract function loadData($get = null, $post = null);

    public abstract function build();
} 