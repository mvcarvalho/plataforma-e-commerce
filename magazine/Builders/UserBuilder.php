<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 03/11/14
 * Time: 21:54
 */

include_once(SITE_FOLDER . 'Managers/UsersManager.php');
include_once(SITE_FOLDER . 'Managers/SalesManager.php');

class UserBuilder extends Builder{

    public $pageItems;
    public $pageInclude;
    public $entityValue;
    public $extraValue;
    public $action;

    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME;
        $this->pageItems = array();
        $this->setAction($get);
        $this->setEntityValue($get);
        $this->setExtraValue($get);
        $this->executeAction($get, $post);
    }

    public function build(){
        include_once(SITE_FOLDER . 'Pages/' . $this->pageInclude . '.php');
    }

    private function setAction($get){
        if(isset($get['param1'])){
            switch($get['param1']){
                case 'login':
                    $this->pageInclude = 'User-login';
                    $this->action = $this->pageInclude;
                    break;

                case 'logout':
                    $this->action = 'User-logout';
                    $this->pageInclude = 'User-login';
                    break;

                case 'register':
                case 'profile':
                    $this->pageInclude = 'User-register';
                    $this->action = 'User-register';
                    break;

                case 'history':
                    $this->pageInclude = 'User-history';
                    $this->action = 'User-history';
                    break;

                case 'edit':
                    $this->pageInclude = 'User';
                    $this->action = $this->pageInclude . '-edit';
                    break;

                case 'reset':
                    $this->pageInclude = 'User';
                    $this->action = $this->pageInclude . '-reset';
                    break;

                case 'reset-email':
                    $this->pageInclude = 'User';
                    $this->action = $this->pageInclude . '-reset-email';
                    break;

                case 'home':
                    $this->pageInclude = 'User';
                    $this->action = $this->pageInclude . '-home';
                    break;

                case 'pay-sale':
                    $this->action = 'User-pay-sale';
                    break;

                case 'ticket-list':
                    $this->action = 'User-ticket-list';
                    break;

                case 'ticket-view':
                    $this->action = 'User-ticket-view';
                    break;

                case 'ticket-add':
                    $this->action = 'User-ticket-add';
                    break;

                default:
                    $this->pageInclude = 'User';
                    $this->action = $this->pageInclude . '-home';
                    break;
            }
        }else{
            $this->pageInclude = 'User';
            $this->action = $this->pageInclude . '-home';
        }
    }

    private function setEntityValue($get){
        if(isset($get['param2'])){
            $this->entityValue = $get['param2'];
        }else{
            $this->entityValue = 0;
        }
    }

    private function setExtraValue($get){
        if(isset($get['param3'])){
            $this->extraValue = $get['param3'];
        }else{
            $this->extraValue = 0;
        }
    }

    private function executeAction($get, $post){
        switch($this->action){
            case 'User-login':
                $this->userLogin($post);
                break;

            case 'User-logout':
                $this->userLogout();
                break;

            case 'User-register':
                $this->userRegister($post);
                break;

            case 'User-history':
                $this->userHistory();
                break;

            case 'User-reset':
                $this->userReset($post);
                break;

            case 'User-reset-email':
                $this->userResetEmail($post);
                break;

            case 'User-pay-sale':
                $this->paySale();
                break;

            case 'User-ticket-list':
                $this->userTicketList();
                break;

            case 'User-ticket-view':
                $this->userTicketView($post);
                break;

            case 'User-ticket-add':
                $this->userTicketAdd($post);
                break;

            default:
                header('Location: ' . SITE_URL . 'user' . DS . 'login');
                break;
        }
    }

    private function userLogin($post){
        if(isset($post['email']) && isset($post['password'])){
            $manager = new UsersManager();
            $user = $manager->login($post['email'], $post['password']);
            if($user == false){
                $this->pageItems['error'] = 'Usuário ou senha incorretos. Verifique e tente novamente.';
            }else{
                $temp = SessionManager::getTempAction();
                if($temp != ""){
                    $temp['success'] = true;
                    SessionManager::setTempAction($temp);
                    header("Location: " . $temp['url']);
                }else{
                    header("Location: " . SITE_URL);
                }
            }
        }else{
            $this->pageItems['error'] = 'Informe o e-mail e a senha.';
        }
    }

    private function userLogout(){
        $manager = new UsersManager();
        $manager->logout();
        header("Location: " . SITE_URL);
    }

    private function userRegister($post){

        $userId = SessionManager::getUserId();
        $manager = new UsersManager();

        if($userId > 0){
            $user = $manager->getUsersById($userId);

            if(count($post) > 2){
                //$user->email = $post['email'];
                $user->name = $post['username'];
                $user->cpf = $post['cpf'];
                $user->address = $post['address'];
                $user->cep = $post['cep'];
                $user->city = $post['city'];
                $user->state = $post['state'];
                $user->phone = $post['phone'];
                $manager->saveUserData($user);

                if($post['password'] != $user->password){
                    $manager->changePassword($user->id, md5($post['password']));
                }
            }

            $this->pageItems['user'] = $user;

        }else if(count($post) > 2){
            //Quando vem da página de login, os dados vem com 2 POSTs, mais do que isso é interno.
            $user = $manager->register($post['email'], $post['password'], $post['username']);

            if($user != false){
                $user->address = $post['address'];
                $user->cep = $post['cep'];
                $user->cpf = $post['cpf'];
                $user->city = $post['city'];
                $user->state = $post['state'];
                $user->phone = $post['phone'];
                $manager->saveUserData($user);

                $temp = SessionManager::getTempAction();
                if($temp != ""){
                    $temp['success'] = true;
                    SessionManager::setTempAction($temp);
                    header("Location: " . $temp['url']);
                }else{
                    header("Location: " . SITE_URL);
                }
            }else{
                $this->pageItems['error'] = 'Este e-mail já está cadastrado. Digite os dados corretamente.';
                header('Location: ' . SITE_URL . 'user' . DS . 'login');
            }
        }else{
            if(isset($post['email'])){
                $user = $manager->getUserByEmail($post['email']);

                if($user->id > 0){
                    $this->pageInclude = 'User-login';
                    $this->pageItems['error'] = 'Este e-mail já está cadastrado. Acesse sua conta aqui.';
                }else{
                    $this->pageItems['email'] = $post['email'];
                }
            }else{
                $this->pageInclude = 'User-login';
            }
        }
    }

    private function userHistory(){
        $userId = SessionManager::getUserId();

        if($userId > 0){
            $manager = new SalesManager();
            $sales = $manager->getSales($userId);
            $this->pageItems['sales'] = $sales;

        }else{
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    private function userReset($post){
        if(isset($post['password']) && isset($post['password-confirm']) && strlen($this->entityValue) > 3){
            if($post['password'] == $post['password-confirm']){
                $manager = new UsersManager();
                $user = $manager->getUserByEmail(decrypt($this->entityValue));
                if($user->id > 0){
                    $manager->changePassword($user->id, $post['password']);
                }
                header('Location: ' . SITE_URL . 'user/login');
            }
        }else{
            $this->pageInclude = "User-reset";
        }
    }

    private function userResetEmail($post){
        if(isset($post['email'])){

            $manager = new UsersManager();
            $user = $manager->getUserByEmail($post['email']);

            if($user->id > 0){
                $htmlBody = file_get_contents(SITE_FOLDER . 'emails' . DS . 'template.html');
                $htmlMessage = file_get_contents(SITE_FOLDER . 'emails' . DS . 'message_template.html');
                $htmlFooter = file_get_contents(SITE_FOLDER . 'emails' . DS . 'footer_template.html');
                $htmlSiteFooter = file_get_contents(SITE_FOLDER . 'emails' . DS . 'site_footer_template.html');

                $htmlFooter = str_replace('{UNSUBSCRIBE_URL}', SITE_URL, $htmlFooter);

                $htmlSiteFooter = str_replace('{SITE_NAME}', SITE_NAME, $htmlSiteFooter);
                $htmlSiteFooter = str_replace('{SITE_ADDRESS_1}', 'Guarulhos', $htmlSiteFooter);
                $htmlSiteFooter = str_replace('{SITE_ADDRESS_2}', 'São Paulo - Brasil', $htmlSiteFooter);
                $htmlSiteFooter = str_replace('{SITE_URL}', SITE_URL, $htmlSiteFooter);
                $htmlSiteFooter = str_replace('{CONTACT_EMAIL}', 'contato@totalfast.com.br', $htmlSiteFooter);

                $htmlBody = str_replace('{EMAIL_WELCOME_MESSAGE}', $post['welcomemessage'], $htmlBody);
                $htmlBody = str_replace('{SITE_NAME}', SITE_NAME, $htmlBody);
                $htmlBody = str_replace('{SITE_URL}', SITE_URL, $htmlBody);
                $htmlBody = str_replace('{SITE_LOGO_URL}', SITE_URL . 'Images/top-logo.png', $htmlBody);
                $htmlBody = str_replace('{SITE_ABOUT_URL}', SITE_URL . 'page/about', $htmlBody);
                $htmlBody = str_replace('{SITE_PRIVACY_URL}', SITE_URL . 'page/privacy', $htmlBody);
                $htmlBody = str_replace('{SITE_SECURITY_URL}', SITE_URL . 'page/security', $htmlBody);
                $htmlBody = str_replace('{FOOTER_HTML}', $htmlFooter, $htmlBody);
                $htmlBody = str_replace('{SITE_FOOTER_HTML}', $htmlSiteFooter, $htmlBody);
                $htmlBody = str_replace('{MAIN_PRODUCT_HTML}', '', $htmlBody);

                $htmlMessage = str_replace('{MSG}', '<br><h3>Para redefinir sua senha, acesse o link abaixo:</h3><br> <a href="'.SITE_URL.'user/reset/'.encrypt($user->email).'">'.SITE_URL.'user/reset/'.encrypt($user->email).'</a>', $htmlMessage);

                $htmlBody = str_replace('{PRODUCTS_HTML}', $htmlMessage, $htmlBody);

                //Email cópia para pessoa q cadastrou
                EmailManager::sendSimpleEmail(
                    SITE_NAME, //Quem está enviando
                    SITE_CONTACT_MAIL, //De qual e-mail
                    SITE_CONTACT_MAIL, //Para qual e-mail responder
                    $user->name . "<" . $user->email . ">", //Para quem está enviando
                    "Redefinição de senha - " . SITE_NAME . " - " . $user->name, //Assunto do e-mail
                    $htmlBody //Conteúdo do e-mail
                );

                //echo($htmlBody);

                $this->pageItems['msg'] = 'Siga as instruções enviadas ao seu e-mail.';
                $this->pageInclude = "User-reset-mail";
            }else{
                $this->pageItems['msg'] = 'Não encontramos o e-mail digitado. Verifique e tente novamente.';
                $this->pageInclude = "User-reset-mail";
            }
        }else{
            $this->pageInclude = "User-reset-mail";
        }
    }

    private function paySale(){
        $userId = SessionManager::getUserId();
        if($userId > 0){
            $userManager = new UsersManager();
            $user = $userManager->getUsersById($userId);

            $saleManager = new SalesManager();
            $sale = $saleManager->getSaleById($this->entityValue);

            if($sale->id > 0){
                $this->pageItems['sale'] = $sale;
                $this->pageItems['user'] = $user;
                $this->pageInclude = "Sale-new-pay";
            }else{
                header('Location: ' . SITE_URL . 'user/history');
            }
        }else{
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    private function userTicketList(){
        $userId = SessionManager::getUserId();
        if($userId > 0){
            $userManager = new UsersManager();
            $tickets = $userManager->getUserTickets($userId);
            $this->pageItems['tickets'] = $tickets;
            $this->pageInclude = "User-ticket-list";
        }else{
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    private function userTicketView($post){
        $userId = SessionManager::getUserId();
        if($userId > 0){

            $userManager = new UsersManager();
            $ticket = $userManager->getUserTicketById($userId, $this->entityValue);

            if(isset($post['message']) && isset($post['status'])){
                if($post['status'] == 2){
                    $ticket->status = 2;

                }else if($post['status'] == 1){
                    $ticket->status = 1;
                }

                $userManager->addUserTicketMessage($ticket->id, $post['message']);
                $userManager->updateUserTicketById($ticket);
                header("Location: " . SITE_URL . 'user' . DS . 'ticket-view' . DS . $ticket->id);

            }else{
                $user = $userManager->getUsersById($userId);
                $this->pageItems['ticket'] = $ticket;
                $this->pageItems['user'] = $user;
                $this->pageInclude = "User-ticket-view";
            }
        }else{
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    private function userTicketAdd($post){
        $userId = SessionManager::getUserId();
        if($userId > 0){
            $userManager = new UsersManager();

            if(isset($post['subject']) && isset($post['message']) && isset($post['saleid']) && strlen($post['saleid']) > 0){
                $saleId = $post['saleid'];
                $ticket = $userManager->createUserTicket($userId, $post['subject'], $saleId);

                if($ticket->id > 0){
                    $userManager->addUserTicketMessage($ticket->id, $post['message']);
                }

                header("Location: " . SITE_URL . 'user' . DS . 'ticket-list');
            }else{
                $this->pageInclude = "User-ticket-add";
                $this->pageItems['msg'] = 'Preencha todos os campos.';
            }
        }else{
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }
}