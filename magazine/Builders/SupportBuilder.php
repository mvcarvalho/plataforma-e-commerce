<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 30/12/14
 * Time: 16:45
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Utils/ACL.php');
include_once(SITE_FOLDER . 'Utils/StringUtils.php');
include_once(SITE_FOLDER . 'Utils/GeneralUtils.php');
include_once(SITE_FOLDER . 'Utils/FileUtils.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');
include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'Managers/SupportManager.php');
include_once(SITE_FOLDER . 'Managers/UsersManager.php');

class SupportBuilder extends Builder{

    private $action = "";
    private $pageInclude = "";
    private $viewItems = array();
    private $entityValue;
    private $extraValue;

    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME . " - Suporte";

        //Carrega o parâmetro da página de administração
        $this->setPage($get);

        //Carrega o parâmetro de ação
        $this->setAction($get);

        if(SessionManager::getAdminName() === ""){
            if($this->action != 'user-auth' && $this->action != 'user-login'){
                header('Location: ' . SITE_URL . 'support/user/login');
            }else{
                if($this->action == 'user-login'){
                    $this->userLogin();
                }else if($this->action == 'user-auth'){
                    $this->userAuth($post);
                }
            }
        }else{
            //Carrega o parâmetro do valor da entidade
            $this->setEntityValue($get);

            //Carrega o parâmetro do valor extra da URL
            $this->setExtraValue($get);

            $this->executeAction($get, $post);
        }
    }

    public function build(){
        include_once(SITE_FOLDER . 'Pages/Support/' . $this->pageInclude . '.php');
    }

    private function setPage($get){
        if(isset($get['param1'])){
            switch($get['param1']){
                case 'user':
                    $this->pageInclude = 'user';
                    break;

                case 'ticket':
                    $this->pageInclude = 'ticket';
                    break;
            }
        }else{
            $this->pageInclude = 'dashboard';
        }
    }

    private function setAction($get){
        if(isset($get['param2'])){
            switch($get['param2']){
                case 'auth':
                    $this->action = $this->pageInclude . '-auth';
                    break;

                case 'login':
                    $this->action = $this->pageInclude . '-login';
                    break;

                case 'logout':
                    $this->action = $this->pageInclude . '-logout';
                    break;

                case 'save':
                    $this->action = $this->pageInclude . '-save';
                    break;

                case 'add':
                    $this->action = $this->pageInclude . '-add';
                    break;

                case 'remove':
                    $this->action = $this->pageInclude . '-remove';
                    break;

                case 'view':
                    $this->action = $this->pageInclude . '-view';
                    break;

                case 'createdefault':
                    $this->pageInclude .= '-createdefault';
                    $this->action = $this->pageInclude;
                    break;

                default:
                    $this->action = $this->pageInclude . '-list';
                    break;
            }
        }else{
            $this->action = $this->pageInclude . '-list';
        }
    }

    private function setEntityValue($get){
        if(isset($get['param3'])){
            $this->entityValue = $get['param3'];
        }else{
            $this->entityValue = 0;
        }
    }

    private function setExtraValue($get){
        if(isset($get['param4'])){
            $this->extraValue = $get['param4'];
        }else{
            $this->extraValue = 0;
        }
    }

    private function executeAction($get, $post){
        switch($this->action){

            case 'user-logout':
                $this->userLogout();
                break;

            case 'user-login':
                $this->userLogin();
                break;

            case 'ticket-list':
                $this->ticketList();
                break;

            case 'ticket-view':
                $this->ticketView($post);
                break;

            default:
                break;
        }
    }

    /****************************************************************
     * USER ACTIONS
     */
    private function userLogin(){
        $this->pageInclude .= '-login';
        $this->viewItems['login_action'] = SITE_URL . 'support/user/auth';
    }

    private function userAuth($post){
        $acl = new ACL();

        if($acl->isRightLogin($post['login'], $post['password'])){
            SessionManager::setAdminName($post['login']);
            SessionManager::setAdminId($acl->getUserId($post['login']));
            header('Location: ' . SITE_URL . 'support/dashboard');
        }else{
            $this->pageInclude .= '-login';
            $this->viewItems['login_action'] = SITE_URL . 'support/user/auth';
        }
    }

    private function userLogout(){
        SessionManager::setAdminName("");
        SessionManager::setAdminId(0);
        $this->pageInclude = 'user-login';
        $this->viewItems['login_action'] = SITE_URL . 'support/user/auth';

    }

    /****************************************************************
     * TICKET ACTIONS
     */
    private function ticketList(){
        $responsibleId = SessionManager::getAdminId();
        $manager = new SupportManager();
        $tickets = $manager->getTickets($responsibleId);
        $this->viewItems['tickets'] = $tickets;
        $this->pageInclude = 'ticket-list';
    }

    private function ticketView($post){
        $manager = new SupportManager();
        $userManager = new UsersManager();
        $ticket = $manager->getTicketById($this->entityValue);
        $user = $userManager->getUsersById($ticket->userId);

        if(isset($post['message']) && isset($post['status'])){
            if($post['status'] == 2){
                $ticket->status = 2;

            }else if($post['status'] == 1){
                $ticket->status = 1;
            }

            $manager->addTicketMessage($ticket->id, $post['message']);
            $manager->updateTicket($ticket);
            header("Location: " . SITE_URL . 'support' . DS . 'ticket' . DS . 'view' . DS . $ticket->id);

        }else{
            $this->viewItems['ticket'] = $ticket;
            $this->viewItems['user'] = $user;
            $this->pageInclude = "ticket-view";
        }

        return $ticket;
    }
} 