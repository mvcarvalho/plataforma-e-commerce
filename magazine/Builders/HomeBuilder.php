<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 08/10/14
 * Time: 00:05
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');
include_once(SITE_FOLDER . 'Entities/Product.php');
include_once(SITE_FOLDER . 'Entities/Category.php');
include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
include_once(SITE_FOLDER . 'Managers/CategoriesManager.php');
include_once(SITE_FOLDER . 'Managers/BannersManager.php');

class HomeBuilder extends Builder{

    private $viewItems = array();
    private $menuHtml = "";
    public $entityValue;

    //Instacia variáveis e realiza consultas no banco.
    //Os dados serão utilizados na construção da página,
    //através do método build.
    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME . " - Home";

        SessionManager::setTempAction("");

        //Carrega o parâmetro do valor da entidade
        $this->setEntityValue($get);

        $productsManager = new ProductsManager();
        $categoryManager = new CategoriesManager();
        $bannerManager = new BannersManager();
        $this->viewItems['main-banners'] = $bannerManager->getBanners('main');
        $this->viewItems['seccondary-banners'] = $bannerManager->getBanners('seccondary');
        $this->viewItems['product-banners'] = $bannerManager->getBanners('product');
        $this->viewItems['stripe-banners'] = $bannerManager->getBanners('stripe');

        if($this->entityValue > 0){
            $this->viewItems['products'] = $productsManager->getRandomProductsByCategory($this->entityValue, true, true);
            $this->viewItems['category'] = $categoryManager->getCategoryById($this->entityValue);

            if($this->viewItems['category']->parentCategoryId > 0){
                SessionManager::setTempAction(array('category' => $this->viewItems['category']->parentCategoryId));
            }else{
                SessionManager::setTempAction(array('category' => $this->entityValue));
            }
        }else{
            $this->viewItems['products'] = $productsManager->getRandomProducts(true);
        }
    }

    public function build()
    {
        include_once("Pages/Home.php");
    }

    private function setEntityValue($get){
        if(isset($get['builder'])){
            $this->entityValue = $get['builder'];
        }else{
            $this->entityValue = 0;
        }
    }
}