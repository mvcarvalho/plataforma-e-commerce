<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 08/10/14
 * Time: 00:39
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');
include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
include_once(SITE_FOLDER . 'Managers/CartsManager.php');
include_once(SITE_FOLDER . 'Managers/QuickInfosManager.php');
include_once(SITE_FOLDER . 'Managers/FeaturesManager.php');

class ProductBuilder extends Builder{

    public $pageItems;
    public $pageInclude;
    public $entityValue;
    public $extraValue;
    public $action;

    public function loadData($get = null, $post = null){
        $this->pageItems = array();

        //Carrega o parâmetro do valor da entidade
        $this->setEntityValue($get);

        //Carrega o parâmetro de ação
        $this->setAction($get);

        //Carrega o parâmetro do valor extra da URL
        $this->setExtraValue($get);

        $this->executeAction($get, $post);

        $this->pageName = SITE_NAME . " - " . $this->pageItems['product']->name;
    }

    public function build(){
        include_once(SITE_FOLDER . 'Pages/' . $this->pageInclude . '.php');
    }

    private function setAction($get){
        if(isset($get['param2'])){
            switch($get['param2']){
                case 'add':
                    $this->pageInclude = 'Product';
                    $this->action = $this->pageInclude . '-add';
                    break;

                case 'remove':
                    $this->pageInclude = 'Product';
                    $this->action = $this->pageInclude . '-remove';
                    break;

                default:
                    $this->pageInclude = 'Product';
                    $this->action = $this->pageInclude . '-view';
                    break;
            }
        }else{
            $this->pageInclude = 'Product';
            $this->action = $this->pageInclude . '-view';
        }
    }

    private function setEntityValue($get){
        if(isset($get['param1'])){
            $this->entityValue = $get['param1'];
        }else{
            $this->entityValue = 0;
        }
    }

    private function setExtraValue($get){
        if(isset($get['param3'])){
            $this->extraValue = $get['param3'];
        }else{
            $this->extraValue = 1;
        }
    }

    private function executeAction($get, $post){
        switch($this->action){
            case 'Product-remove':
                $this->productRemove($post);
                break;

            case 'Product-view':
                $this->productView();
                break;

            default:
                $this->productView();
                break;
        }
    }

    private function productView(){
        $manager = new ProductsManager();
        $infoManager = new QuickInfosManager();
        $featuresManager = new FeaturesManager();
        $product = $manager->getProductById($this->entityValue);

        if($product->id <= 0){
            $product = new Product();
        }else{
            $product->views += 1;
            $manager->saveProduct($product);
        }

        $infos = $infoManager->getQuickInfoByProductId($product->id);
        $features = $featuresManager->getFeaturesByProductId($product->id);

        $this->pageItems['product'] = $product;
        $this->pageItems['infos'] = $infos;
        $this->pageItems['features'] = $features;
    }

    //Remove o produto do carrinho
    private function productRemove($post){
        $manager = new ProductsManager();
        $product = $manager->getProductById($this->entityValue);

        if($product->id <= 0){
            $product = new Product();
        }else{
            $manager = new CartsManager();
            $manager->removeCartItem($product->id);
            //Implementar remoção de opções
        }

        $this->productView();
    }
}