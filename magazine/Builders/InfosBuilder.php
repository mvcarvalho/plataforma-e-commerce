<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 08/10/14
 * Time: 00:05
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');
include_once(SITE_FOLDER . 'Managers/EmailManager.php');

class InfosBuilder extends Builder{

    public $entityValue;
    public $pageInclude;
    public $viewItems;

    //Instacia variáveis e realiza consultas no banco.
    //Os dados serão utilizados na construção da página,
    //através do método build.
    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME;

        //Carrega o parâmetro do valor da entidade
        $this->setEntityValue($get);

        $this->checkContactMailSend($post);

        if($this->entityValue == "contract"){
            $this->pageInclude = 'Contract.php';

        }else if($this->entityValue == "privacy"){
            $this->pageInclude = 'Privacy.php';

        }else if($this->entityValue == "security"){
            $this->pageInclude = 'Security.php';

        }else if($this->entityValue == "contact"){
            $this->pageInclude = 'Contact.php';

        }else{
            $this->pageInclude = '';
        }
    }

    public function build()
    {
        if($this->pageInclude != ''){
            include_once("Pages/" . $this->pageInclude);
        }
    }

    private function setEntityValue($get){
        if(isset($get['param1'])){
            $this->entityValue = $get['param1'];
        }else{
            $this->entityValue = 0;
        }
    }

    private function checkContactMailSend($post){
        if(isset($post['message']) && isset($post['email']) && isset($post['name'])){
            
			//Email para contato@totalfast.com.br
            EmailManager::sendSimpleEmail(
				$post['name'], //Quem está enviando
				SITE_CONTACT_MAIL, //De qual e-mail
				$post['email'], //Para qual e-mail responder
				SITE_CONTACT_MAIL, //Para quem está enviando
				"Contato " . SITE_NAME . " - " . $post['name'], //Assunto do e-mail
				$post['message'] //Conteúdo do e-mail
			);
			
			//Email cópia para pessoa q cadastrou
			EmailManager::sendSimpleEmail(
				SITE_NAME, //Quem está enviando
				SITE_CONTACT_MAIL, //De qual e-mail
				SITE_CONTACT_MAIL, //Para qual e-mail responder
				$post['name'] . "<" . $post['email'] . ">", //Para quem está enviando
				"Contato " . SITE_NAME . " - " . $post['name'], //Assunto do e-mail
				$post['message'] //Conteúdo do e-mail
			);
            
			$this->viewItems = array('message'=>'E-mail enviado com sucesso. Em breve responderemos.');
        }
    }
}