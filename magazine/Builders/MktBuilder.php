<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 12/10/14
 * Time: 12:26
 */

include_once(SITE_FOLDER . 'Config.php');
include_once(SITE_FOLDER . 'Utils/ACL.php');
include_once(SITE_FOLDER . 'Utils/StringUtils.php');
include_once(SITE_FOLDER . 'Utils/GeneralUtils.php');
include_once(SITE_FOLDER . 'Utils/FileUtils.php');
include_once(SITE_FOLDER . 'Builders/Builder.php');
include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'Managers/ProductsManager.php');
include_once(SITE_FOLDER . 'Managers/BannersManager.php');
include_once(SITE_FOLDER . 'Managers/NewsletterManager.php');

class MktBuilder extends Builder{

    private $action = "";
    private $pageInclude = "";
    private $viewItems = array();
    private $entityValue;
    private $extraValue;

    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME . " - Marketing";

        //Carrega o parâmetro da página de administração
        $this->setPage($get);

//        var_dump("Set Action-> ".$post);
        //Carrega o parâmetro de ação
        $this->setAction($get);

        if(SessionManager::getAdminName() === ""){
            if($this->action != 'user-auth' && $this->action != 'user-login'){
                header('Location: ' . SITE_URL . 'mkt/user/login');
            }else{
                if($this->action == 'user-login'){
                    $this->userLogin();
                }else if($this->action == 'user-auth'){
                    $this->userAuth($post);
                }
            }
        }else{
            //Carrega o parâmetro do valor da entidade
            $this->setEntityValue($get);

            //Carrega o parâmetro do valor extra da URL
            $this->setExtraValue($get);

            $this->executeAction($get, $post);

        }
    }

    public function build(){
        include_once(SITE_FOLDER . 'Pages/Mkt/' . $this->pageInclude . '.php');
    }

    private function setPage($get){
        if(isset($get['param1'])){
            switch($get['param1']){
                case 'user':
                    $this->pageInclude = 'user';
                    break;

                case 'email':
                    $this->pageInclude = 'email';
                    break;

                case 'banner':
                    $this->pageInclude = 'banner';
                    break;

                case 'newsletter':
                    $this->pageInclude = 'newsletter';
                    break;
            }
        }else{
            $this->pageInclude = 'dashboard';
        }
    }

    private function setAction($get){
        if(isset($get['param2'])){
            switch($get['param2']){
                case 'auth':
                    $this->action = $this->pageInclude . '-auth';
                    break;

                case 'login':
                    $this->action = $this->pageInclude . '-login';
                    break;

                case 'logout':
                    $this->action = $this->pageInclude . '-logout';
                    break;

                case 'save':
                    $this->action = $this->pageInclude . '-save';
                    break;

                case 'add':
                    $this->action = $this->pageInclude . '-add';
                    break;

                case 'remove':
                    $this->action = $this->pageInclude . '-remove';
                    break;

                case 'view':
                    $this->action = $this->pageInclude . '-view';
                    break;

                case 'createdefault':
                    $this->pageInclude .= '-createdefault';
                    $this->action = $this->pageInclude;
                    break;

                default:
                    $this->action = $this->pageInclude . '-list';
                    break;
            }
        }else{
            $this->action = $this->pageInclude . '-list';
        }
    }

    private function setEntityValue($get){
        if(isset($get['param3'])){
            $this->entityValue = $get['param3'];
        }else{
            $this->entityValue = 0;
        }
    }

    private function setExtraValue($get){
        if(isset($get['param4'])){
            $this->extraValue = $get['param4'];
        }else{
            $this->extraValue = 0;
        }
    }

    private function executeAction($get, $post){
        switch($this->action){

            case 'user-logout':
                $this->userLogout();
                break;

            case 'user-login':
                $this->userLogin();
                break;

            case 'email-list':
                $this->listEmails();
                break;

            case 'email-createdefault':
                $this->createEmailDefault($post);
                break;

            case 'banner-list':
                $this->listBanners();
                break;

            case 'banner-add':
                $this->bannerAdd($post);
                break;

            case 'banner-remove':
                $this->bannerRemove();
                break;

            case 'newsletter-add':

                $this->newsletterAdd($post);
                break;

            default:
                break;
        }
    }

    /****************************************************************
     * USER ACTIONS
     */
    private function userLogin(){
        $this->pageInclude .= '-login';
        $this->viewItems['login_action'] = SITE_URL . 'mkt/user/auth';
    }

    private function userAuth($post){
        $acl = new ACL();

        if($acl->isRightLogin($post['login'], $post['password'])){
            SessionManager::setAdminName($post['login']);
            SessionManager::setAdminId($acl->getUserId($post['login']));
            header('Location: ' . SITE_URL . 'mkt/dashboard');
        }else{
            $this->pageInclude .= '-login';
            $this->viewItems['login_action'] = SITE_URL . 'mkt/user/auth';
        }
    }

    private function userLogout(){
        SessionManager::setAdminName("");
        SessionManager::setAdminId(0);
        $this->pageInclude = 'user-login';
        $this->viewItems['login_action'] = SITE_URL . 'mkt/user/auth';

    }

    /****************************************************************
     * EMAIL ACTIONS
     */
    private function listEmails(){
        $files = scandir(SITE_FOLDER . 'emails/generated/', true);
        $emails = array();
        foreach($files as $file){
            if(strlen($file) > 5){
                $emails[] = $file;
            }
        }

        $this->pageInclude .= '-list';
        $this->viewItems['emails'] = $emails;
    }

    private function createEmailDefault($post){
        if(count($post) > 1){
            $products = array();
            for($x = 1; $x <= 10; $x++){
                if($post['productid' . $x] != ""){
                    $products[] = array(
                        $post['productid' . $x],
                        //$post['productdescription' . $x]
                    );
                }
            }

            if(count($products) > 0){

                $manager = new ProductsManager();

                $htmlBody = file_get_contents(SITE_FOLDER . 'emails' . DS . 'template.html');
                $htmlProductsTemplate = file_get_contents(SITE_FOLDER . 'emails' . DS . 'two_columns_product_template.html');
                $htmlProducts = "";

                $left = true;
                $tempTemplate = "";
                foreach($products as $product){
                    $product = $manager->getProductById($product[0]);
                    if($product->id > 0){
                        if($left){
                            $tempTemplate = $htmlProductsTemplate;
                            $tempTemplate = str_replace('{PRODUCT_LEFT_NAME}', $product->shortDescription, $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_LEFT_URL}', SITE_URL . 'produto/' . $product->id . '/' . StringUtils::clean($product->name), $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_LEFT_IMAGE_URL}', SITE_URL . 'Images/Products/' . $product->image, $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_LEFT_PRICE}', GeneralUtils::getFormattedValue($product->offerPrice), $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_LEFT_PRICE_DIVIDED}', GeneralUtils::getFormattedValue($product->offerPrice/10), $tempTemplate);
                            $left = false;
                        }else{
                            $tempTemplate = str_replace('{PRODUCT_RIGHT_NAME}', $product->shortDescription, $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_RIGHT_URL}', SITE_URL . 'produto/' . $product->id . '/' . StringUtils::clean($product->name), $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_RIGHT_IMAGE_URL}', SITE_URL . 'Images/Products/' . $product->image, $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_RIGHT_PRICE}', GeneralUtils::getFormattedValue($product->offerPrice), $tempTemplate);
                            $tempTemplate = str_replace('{PRODUCT_RIGHT_PRICE_DIVIDED}', GeneralUtils::getFormattedValue($product->offerPrice/10), $tempTemplate);
                            $htmlProducts .= $tempTemplate;
                            $left = true;
                        }
                    }
                }

                $htmlBody = str_replace('{SITE_NAME}', SITE_NAME, $htmlBody);
                $htmlBody = str_replace('{PRODUCTS_HTML}', $htmlProducts, $htmlBody);

                $pageName = "email_" . date('Y_m_d_H_i_s') . ".html";
                $file = fopen(SITE_FOLDER . "emails/generated/" . $pageName, "w");
                fwrite($file, $htmlBody);
                fclose($file);

                $this->viewItems['emailhtml'] = SITE_URL . 'emails/generated/' . $pageName;
            }
        }
        $this->viewItems['formaction'] = SITE_URL . 'mkt/email/createdefault';
    }

    /****************************************************************
     * BANNERS ACTIONS
     */

    public function listBanners(){
        $manager = new BannersManager();
        $banners = $manager->getBanners();
        $this->pageInclude .= '-list';
        $this->viewItems['banners'] = $banners;
    }

    public function bannerAdd($post){
        if(isset($post['link']) && isset($post['order']) && isset($post['type'])){

            $fileName = "";
            if(count($_FILES) > 0 && $_FILES['image']['size'] > 1){
                $fileName = 'banner_' . time() . '.jpg';
                $fileUtils = new FileUtils();
                $fileUtils->upload($_FILES['image'], BANNERS_IMAGES_FOLDER, $fileName);
            }

            $manager = new BannersManager();
            $banner = new Banner();
            $banner->image = $fileName;
            $banner->url = $post['link'];
            $banner->order = $post['order'];
            $banner->type = $post['type'];
            $manager->saveBanner($banner);
        }

        header('Location: ' . SITE_URL . 'mkt/banner/list');
    }

    public function bannerRemove(){
        $manager = new BannersManager();
        $banner = $manager->getBannerById($this->entityValue);

        if($banner->id > 0){
            unlink(BANNERS_IMAGES_FOLDER . $banner->image);
            $manager->deleteBannerById($banner->id);
        }

        header('Location: ' . SITE_URL . 'mkt/banner/list');
    }

    public function newsletterAdd($post) {

    }
}