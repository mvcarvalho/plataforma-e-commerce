<?php
/**
 * Created by PhpStorm.
 * User: rubens
 * Date: 28/01/15
 * Time: 21:04
 */

include_once(SITE_FOLDER . 'Managers/NewsletterManager.php');

class NewslettersBuilder extends NewsletterManager {

    public $pageItems;
    public $pageInclude;
    public $entityValue;
    public $extraValue;
    public $action;

    public function loadData($get = null, $post = null){
        $this->pageName = SITE_NAME;
        $this->pageItems = array();
        $this->setAction($get);
        $this->executeAction($get, $post);
    }

    public function build(){
    }

    private function setAction($get){
        if(isset($get['param1'])){
            switch($get['param1']){
                case 'add':
                    $this->pageInclude = 'news-add';
                    $this->action = $this->pageInclude;
                    break;

                case 'already':
                    $this->pageInclude = 'news-already';
                    $this->action = $this->pageInclude;
                    break;

                default:
                    $this->pageInclude = 'User';
                    $this->action = $this->pageInclude . '-home';
                    break;
            }
        }else{
            $this->pageInclude = 'User';
            $this->action = $this->pageInclude . '-home';
        }
    }

    private function executeAction($get, $post){
        switch($this->action){
            case 'news-add':
                $this->addNews($post);
                break;

            case 'news-already':
                $this->newsAlreadyRegistered();
                break;

            default:
                header('Location: ' . SITE_URL);
                break;
        }
    }

    private function addNews($post){
        if(isset($post['email'])){
            $news = new Newsletter();
            $news->email = $post['email'];
            if($post['sexo'] != -1){
                $news->sexo = $post['sexo'];
            }

            $news->interest = array();

            $keys = array_keys($post);
            $i = 0;
            foreach($post as $value) {
                consoleLog(" -- KEY ".$keys[$i]);
                if(strripos($keys[$i],"CAT") > -1) {
                    consoleLog("Valor " . $value);

                    $interest = new Interest(0, $value);
                    $news->interest[] = $interest;
                }
                $i++;
            }

            $manager = new NewsletterManager();
            $manager->saveNewsletter($news);

            SessionManager::setNewsletter(false);
        }

        header('Location: ' . SITE_URL);
    }

    private function newsAlreadyRegistered(){
        SessionManager::setNewsletter(false);
    }
}