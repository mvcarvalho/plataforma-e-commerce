<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 26/10/14
 * Time: 16:02
 */

include_once(SITE_FOLDER . 'Managers/CartsManager.php');
include_once(SITE_FOLDER . 'Managers/SalesManager.php');
include_once(SITE_FOLDER . 'Managers/SessionManager.php');
include_once(SITE_FOLDER . 'Managers/UsersManager.php');
include_once(SITE_FOLDER . 'Managers/PaymentManager.php');
include_once(SITE_FOLDER . 'Managers/FeaturesManager.php');
include_once(SITE_FOLDER . 'Managers/CouponsManager.php');
include_once(SITE_FOLDER . 'Libs/moip/autoload.inc.php');
include_once(SITE_FOLDER . 'DAOs/AttributeOptionDAO.php');

class CartBuilder extends Builder
{

    public $pageItems;
    public $pageInclude;
    public $entityValue;
    public $extraValue;
    public $action;

    public function loadData($get = null, $post = null)
    {
        $this->pageName = SITE_NAME . " - Carrinho";
        $this->pageInclude = 'Cart';

        $this->setAction($get);
        $this->setEntityValue($get);
        $this->setExtraValue($get);
        $this->executeAction($get, $post);
    }

    public function build()
    {
        include_once(SITE_FOLDER . 'Pages/' . $this->pageInclude . '.php');
    }

    private function setAction($get)
    {
        if (isset($get['param1'])) {
            switch ($get['param1']) {
                case 'add-quantity':
                    $this->action = $this->pageInclude . '-add-quantity';
                    break;

                case 'set-coupon':
                    $this->action = $this->pageInclude . '-set-coupon';
                    break;

                case 'remove-quantity':
                    $this->action = $this->pageInclude . '-remove-quantity';
                    break;

                case 'add':
                    $this->action = $this->pageInclude . '-add';
                    break;

                case 'remove':
                    $this->action = $this->pageInclude . '-remove';
                    break;

                case 'checkout':
                    $this->action = $this->pageInclude . '-checkout';
                    break;

                case 'finalize':
                    $this->action = $this->pageInclude . '-finalize';
                    break;

                default:
                    $this->pageInclude = 'Cart';
                    $this->action = $this->pageInclude . '-view';
                    break;
            }
        } else {
            $this->pageInclude = 'Cart';
            $this->action = $this->pageInclude . '-view';
        }
    }

    private function setEntityValue($get)
    {
        if (isset($get['param2'])) {
            $this->entityValue = $get['param2'];
        } else {
            $this->entityValue = 0;
        }
    }

    private function setExtraValue($get)
    {
        if (isset($get['param3'])) {
            $this->extraValue = $get['param3'];
        } else {
            $this->extraValue = 1;
        }
    }

    private function executeAction($get, $post)
    {
        switch ($this->action) {
            // PRODUCTS
            case 'Cart-remove-quantity':
                $this->setItemQuantity($post, -1);
                break;

            case 'Cart-add-quantity':
                $this->setItemQuantity($post, 1);
                break;

            case 'Cart-add':
                $this->addItem($post);
                break;

            case 'Cart-set-coupon':
                $this->setCupom($post);
                break;

            case 'Cart-remove':
                $this->removeItem();
                break;

            case 'Cart-view':
                $this->cartView();
                break;

            case 'Cart-checkout':
                $this->cartCheckout();
                break;

            case 'Cart-finalize':
                $this->finalizeBuy($post);
                break;

            default:
                $this->cartView();
                break;
        }
    }

    //Define o cupom do carrinho
    private function setCupom($post)
    {
        $userId = SessionManager::getUserId();
        if ($userId > 0) {
            $manager = new CartsManager();
            $couponManager = new CouponsManager();
            $cart = $manager->getCart($userId);

            if($cart->id > 0 && (!isset($cart->coupon) || $cart->coupon == "")){
                $this->pageItems['cart'] = $cart;

                if(isset($post["coupon"]) && $post["coupon"] != ""){
                    $coupon = $couponManager->getCouponByKey($post["coupon"]);
                    $total = 0.0;
                    foreach ($cart->items as $item) {
                        $total += $item->price * $item->quantity;
                    }

                    if($coupon->id > 0 && $coupon->validity > time() && $total > $coupon->minValue){
                        $cart->coupon = $coupon->key;
                        $manager->updateCart($cart);

                        header("Location: " . SITE_URL . 'cart');
                    }else{
                        $this->pageItems['coupon_msg'] = 'Cupom inválido.';
                    }
                }else{
                    $this->pageItems['coupon_msg'] = 'Digite um cupom.';
                }
            }else{
                $this->pageItems['coupon_msg'] = 'O carrinho já possui um cupom.';
            }

        } else {
            $temp = array(
                'url' => SITE_URL . 'cart/add/' . $this->entityValue . '/1',
                'post' => $post
            );
            SessionManager::setTempAction($temp);
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    //Adiciona o produto no carrinho
    private function addItem($post)
    {
        $userId = SessionManager::getUserId();
        if ($userId > 0) {
            $temp = SessionManager::getTempAction();
            if (isset($temp['post'])) {
                $post = $temp['post'];
                SessionManager::setTempAction("");
            }

            $manager = new ProductsManager();
            $product = $manager->getProductById($this->entityValue);

            if ($product->id <= 0) {
                $product = new Product();

            } else {
                $manager = new CartsManager();
                $featuresManager = new FeaturesManager();
                $cart = $manager->getCart($userId);

                $options = array();

                foreach ($product->attributes as $attribute) {
                    if (count($attribute->options) > 0) {
                        $attributeOptionId = $post['select-' . $attribute->id];
                        $optionDAO = new AttributeOptionDAO();
                        $option = $optionDAO->selectById($attributeOptionId);
                        $options[] = $option;
                        $product->offerPrice += $option->priceChange;
                        //$manager->addCartItemOption($cartItemId, $attributeOptionId, $option->priceChange);
                    }
                }

                $features = $featuresManager->getFeaturesByProductId($product->id);
                $featureOptions = "";
                $count = 0;
                foreach ($features as $feature) {
                    $count++;
                    $featureOptions .= $post['feature-' . $feature->id];
                    if ($count < count($features)) {
                        $featureOptions .= " - ";
                    }
                }

                $cartItemId = $manager->addCartItem($cart->id, $product->id, $this->extraValue, $product->offerPrice, $featureOptions);

                if ($cartItemId > 0) {
                    foreach ($options as $option) {
                        $manager->addCartItemOption($cartItemId, $option->id, $option->priceChange);
                    }
                }
            }

            header("Location: " . SITE_URL . 'cart/fb/p');
        } else {
            $temp = array(
                'url' => SITE_URL . 'cart/add/' . $this->entityValue . '/1',
                'post' => $post
            );
            SessionManager::setTempAction($temp);
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    private function removeItem()
    {
        $manager = new CartsManager();
        $manager->removeCartItemOption($this->entityValue);
        $manager->removeCartItem($this->entityValue);
        header("Location: " . SITE_URL . 'cart');
    }

    private function setItemQuantity($post, $quantity)
    {
        $manager = new CartsManager();
        $item = $manager->getCartItem($this->entityValue);
        $item->quantity += $quantity;
        $manager->updateCartItemQuantity($item->id, $item->quantity);

        header("Location: " . SITE_URL . 'cart');
    }

    private function cartView()
    {
        $userId = SessionManager::getUserId();

        if ($userId > 0) {
            $manager = new CartsManager();
            $couponManager = new CouponsManager();
            $cart = $manager->getCart($userId);

            $total = 0.0;
            $deliveryCost = 0.0;
            foreach ($cart->items as $item) {
                $total += $item->price * $item->quantity;
                if ($item->product->deliveryCost > $deliveryCost) {
                    $deliveryCost = $item->product->deliveryCost;
                }
            }

            if(isset($cart->coupon) && $cart->coupon != "") {
                $coupon = $couponManager->getCouponByKey($cart->coupon);
                if($coupon->offType == 1){
                    $this->pageItems['coupon_off'] = $coupon->off;
                    $this->pageItems['coupon'] = $cart->coupon;
                }
                if($coupon->offType == 2){
                    $this->pageItems['coupon_off'] = ($total / 100) * $coupon->off;
                    $this->pageItems['coupon'] = $cart->coupon;
                }
            }

            $this->pageItems['cart'] = $cart;
            $this->pageItems['total'] = $total;
            $this->pageItems['deliveryCost'] = $deliveryCost;

            if ($this->entityValue === 'p') {
                $this->pageItems['pixel'] = true;
            }
        } else {
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    private function cartCheckout()
    {
        $userId = SessionManager::getUserId();
        if ($userId > 0) {
            $userManager = new UsersManager();
            $user = $userManager->getUsersById($userId);

            $manager = new CartsManager();
            $cart = $manager->getCart($userId);

            if (count($cart->items) > 0) {

                $final = 0;
                $total = 0;
                $deliveryCost = 0;
                foreach ($cart->items as $item) {
                    if ($item->product->deliveryCost > $deliveryCost) {
                        $deliveryCost = $item->product->deliveryCost;
                    }
                    $total += (int)$item->quantity * (float)$item->price;
                }

                $couponManager = new CouponsManager();
                if(isset($cart->coupon) && $cart->coupon != "") {
                    $coupon = $couponManager->getCouponByKey($cart->coupon);
                    if($coupon->offType == 1){
                        $final -= $coupon->off;
                        $this->pageItems['coupon_off'] = $coupon->off;
                        $this->pageItems['coupon'] = $cart->coupon;
                    }
                    if($coupon->offType == 2){
                        $final -= ($total / 100) * $coupon->off;
                        $this->pageItems['coupon_off'] = ($total / 100) * $coupon->off;
                        $this->pageItems['coupon'] = $cart->coupon;
                    }
                }

                $final += $total + $deliveryCost;

                if (GATEWAY_INTEGRATION == 'moip') {
                    $moip = new Moip();
                    $moip->setEnvironment('test');
                    $moip->setCredential(array(
                        'key' => MOIP_KEY,
                        'token' => MOIP_TOKEN
                    ));

                    $result = $moip->queryParcel(MOIP_LOGIN, MAX_INSTALLMENTS, MOIP_RATE, $final);
                    $this->pageItems['installments'] = $result['installment'];
                }

                $this->pageItems['cart'] = $cart;
                $this->pageItems['total'] = $total;
                $this->pageItems['final'] = $final;
                $this->pageItems['deliveryCost'] = $deliveryCost;
                $this->pageItems['user'] = $user;
                $this->pageInclude .= "-checkout";
            } else {
                header("Location: " . SITE_URL . 'cart');
            }
        } else {
            header("Location: " . SITE_URL . 'user' . DS . 'login');
        }
    }

    private function finalizeBuy($post)
    {

    }
}