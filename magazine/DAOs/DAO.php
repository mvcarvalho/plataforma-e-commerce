<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:58
 */

include_once(SITE_FOLDER . 'Config.php');

class DAO {

    public  $conn;

    public function openConnection(){
//        consoleLog("Opening database connection.", "DAO");
        $this->conn =  new mysqli(DATABASE_URL, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);

        if($this->conn->errno){
            consoleLog("Error opening database connection. " . $this->conn->error, "DAO");
        }else{
//            consoleLog("Database connection opened.", "DAO");
            $this->conn->set_charset('utf8');
        }

        return $this->conn;
    }

    public function closeConnection(){
        $this->conn->close();
//        consoleLog("Database connection closed.", "DAO");
    }
} 