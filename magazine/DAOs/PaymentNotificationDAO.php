<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 19/11/14
 * Time: 19:32
 */

include_once(SITE_FOLDER . 'Entities/PaymentNotification.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class PaymentNotificationDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO payment_notifications VALUES (NULL,?,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE payment_notifications SET gateway_name = ?, gateway_id = ?, payment_method = ?, value = ?, notification_date = ?, status = ? WHERE payment_id = ?";
    private $SQL_SELECT = "SELECT * FROM payment_notifications WHERE gateway_id = ";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(PaymentNotification $paymentNotification){
        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("sssids",
            $paymentNotification->gatewayId,
            $paymentNotification->gatewayName,
            $paymentNotification->paymentMethod,
            $paymentNotification->date,
            $paymentNotification->value,
            $paymentNotification->status
        );

        $sta->execute();

        if($sta->insert_id != 0){
            $paymentNotification->id = $sta->insert_id;
        }

        $sta->close();
        return $paymentNotification;
    }

    public function update(PaymentNotification $paymentNotification){
        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("sssid",
            $paymentNotification->gatewayId,
            $paymentNotification->gatewayName,
            $paymentNotification->paymentMethod,
            $paymentNotification->date,
            $paymentNotification->value,
            $paymentNotification->status,
            $paymentNotification->id
        );

        $sta->execute();
        $sta->close();
    }

    public function selectByGatewayId($gatewaayId){
        $resultSet = $this->conn->query($this->SQL_SELECT . $gatewaayId);

        $payment = new PaymentNotification();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $payment->setFromResultSet($row);
        }

        $resultSet->free();
        return $payment;
    }
} 