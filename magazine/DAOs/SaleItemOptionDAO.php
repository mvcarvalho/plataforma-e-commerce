<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:13
 */

include_once(SITE_FOLDER . 'Entities/SaleItemOption.php');
include_once(SITE_FOLDER . 'DAOs/AttributeOptionDAO.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class SaleItemOptionDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO sales_items_options VALUES(null,?,?,?)";
    private $SQL_UPDATE = "UPDATE sales_items_options SET sale_item_id = ?, attribute_id = ?, price_change = ? WHERE sale_item_option_id = ?";
    private $SQL_SELECT = "SELECT * FROM sales_items_options";

    private $TAG = "Sale Item Option";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(SaleItemOption $saleItemOption){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iid",
            $saleItemOption->saleItemId,
            $saleItemOption->attributeId,
            $saleItemOption->priceChange
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $saleItemOption->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
        return $saleItemOption;
    }

    public function update(SaleItemOption $saleItemOption){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("iidi",
            $saleItemOption->saleItemId,
            $saleItemOption->attributeId,
            $saleItemOption->priceChange,
            $saleItemOption->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function select(){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $saleItemsOptions = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $saleItemOption = new SaleItemOption();
            $saleItemOption->setFromResultSet($row);
            $saleItemsOptions[] = $saleItemOption;
        }

        $resultSet->free();
        return $saleItemsOptions;
    }

    public function selectByItemId($itemId = 0){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE sale_item_id = ' . $itemId);

        $attributeOptionDAO = new AttributeOptionDAO();

        $saleItemOptions = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $saleItemOption = new SaleItemOption();
            $saleItemOption->setFromResultSet($row);
            $saleItemOption->attribute = $attributeOptionDAO->selectById($saleItemOption->attributeId);
            $saleItemOptions[] = $saleItemOption;
        }

        $resultSet->free();
        return $saleItemOptions;
    }
} 