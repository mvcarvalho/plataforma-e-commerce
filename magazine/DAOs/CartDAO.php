<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:11
 */

include_once(SITE_FOLDER . 'Entities/Cart.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class CartDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO carts VALUES(null,?,?,?)";
    private $SQL_UPDATE = "UPDATE carts SET user_id = ?, creation_date = ?, cupom = ? WHERE cart_id = ?";
    private $SQL_SELECT = "SELECT * FROM carts";
    private $SQL_DELETE = "DELETE FROM carts";

    private $TAG = "Cart";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Cart $cart){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iis",
            $cart->userId,
            $cart->creationDate,
            $cart->coupon
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $cart->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
    }

    public function update(Cart $cart){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("iisi",
            $cart->userId,
            $cart->creationDate,
            $cart->coupon,
            $cart->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function delete($userId = 0){
        consoleLog("Deleting.", $this->TAG);

        $cart = $this->selectByUserId($userId);
        if($cart->id > 0){

            $cartItemDAO = new CartItemDAO();
            $cartItemDAO->deleteByCartId($cart->id);

            $sta = $this->conn->prepare($this->SQL_DELETE . ' WHERE user_id = ?');
            $sta->bind_param("i",
                $userId
            );

            $sta->execute();

            if($sta->error){
                consoleLog("Delete error. SQL error: " . $sta->error, $this->TAG);
            }else{
                consoleLog("Delete success.", $this->TAG);
            }
        }

        $sta->close();
    }

    public function select(){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $carts = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $cart = new Cart();
            $cart->setFromResultSet($row);
            $carts[] = $cart;
        }

        $resultSet->free();
        return $carts;
    }

    public function selectByUserId($userId = 0){
        consoleLog("Selecting by user id.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE user_id = ' . $userId . ' ORDER BY cart_id DESC');

        $cart = new Cart();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            include_once(SITE_FOLDER . 'DAOs/CartItemDAO.php');
            $cartItemDAO = new CartItemDAO();

            $cart->setFromResultSet($row);
            $cart->items = $cartItemDAO->selectByCartId($cart->id);
        }

        $resultSet->free();
        return $cart;
    }
} 