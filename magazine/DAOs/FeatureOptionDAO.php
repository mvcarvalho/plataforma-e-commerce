<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 26/09/14
 * Time: 00:09
 */

include_once(SITE_FOLDER . 'Entities/FeatureOption.php');
//include_once(SITE_FOLDER . 'DAOs/FeatureOptionDAO.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class FeatureOptionDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO products_features_options VALUES(NULL,?,?)";
    private $SQL_UPDATE = "UPDATE products_features_options SET name = ? WHERE feature_option_id = ?";
    private $SQL_SELECT = "SELECT * FROM products_features_options";
    private $SQL_DELETE = "DELETE FROM products_features_options";

    private $TAG = "Feature Option";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(FeatureOption $feature){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("is",
            $feature->featureId,
            $feature->name
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $feature->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
        return $feature;
    }

    public function update(FeatureOption $feature){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("si",
            $feature->name,
            $feature->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function delete(FeatureOption $feature){
        consoleLog("Deleting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_DELETE . " WHERE feature_option_id = ?");
        $sta->bind_param("i",
            $feature->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Delete error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Delete success.", $this->TAG);
        }

        $sta->close();
    }

    public function deleteByFeatureId($featureId = 0){
        consoleLog("Deleting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_DELETE . " WHERE product_feature_id = ?");
        $sta->bind_param("i",
            $featureId
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Delete error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Delete success.", $this->TAG);
        }

        $sta->close();
    }

    public function selectByFeatureId($productId = 0){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE product_feature_id = " . $productId);

        $features = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $feature = new FeatureOption();
            $feature->setFromResultSet($row);
            $features[] = $feature;
        }

        $resultSet->free();
        return $features;
    }
} 