<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 23:49
 */

include_once(SITE_FOLDER . 'Entities/Category.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class CategoryDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO categories VALUES (NULL,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE categories SET parent_category_id = ?, name = ?, description = ?, image = ?, is_active = ? WHERE category_id = ?";
    private $SQL_SELECT = "SELECT * FROM categories";

    private $TAG = "Category";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Category $category){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT) or consoleLog($this->conn->error);
        $sta->bind_param("isssi",
            $category->parentCategoryId,
            $category->name,
            $category->description,
            $category->image,
            $category->isActive
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
    }

    public function update(Category $category){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE) or consoleLog($this->conn->error);
        $sta->bind_param("isssii",
            $category->parentCategoryId,
            $category->name,
            $category->description,
            $category->image,
            $category->isActive,
            $category->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function select($isActive = false){
        consoleLog("Selecting all.", $this->TAG);

        if($isActive){
            $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE is_active = 1') or consoleLog($this->conn->error);
        }else{
            $resultSet = $this->conn->query($this->SQL_SELECT) or consoleLog($this->conn->error);
        }

        $categories = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $category = new Category();
            $category->setFromResultSet($row);
            $categories[] = $category;
        }

        $resultSet->free();
        return $categories;
    }

    public function selectPage($page, $count, $isActive = false){
        consoleLog("Selecting page " . $page .".", $this->TAG);

        $offset = $page * $count;
        $active = '';
        if($isActive){
            $active = ' WHERE is_active = 1 ';
        }
        $resultSet = $this->conn->query($this->SQL_SELECT . $active . " LIMIT " . $offset . ", " . $count) or consoleLog($this->conn->error);

        $categories = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $category = new Category();
            $category->setFromResultSet($row);
            $categories[] = $category;
        }

        $resultSet->free();
        return $categories;
    }

    public function selectById($categoryId){
        consoleLog("Selecting id " . $categoryId . ".", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE category_id = " . $categoryId) or consoleLog($this->conn->error);

        $category = new Category();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $category->setFromResultSet($row);
        }

        $resultSet->free();
        return $category;
    }

    public function selectByParentId($categoryId, $isActive = false){
        consoleLog("Selecting by parent id " . $categoryId . ".", $this->TAG);

        $active = '';
        if($isActive){
            $active = ' AND is_active = 1 ';
        }
        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE parent_category_id = " . $categoryId . $active) or consoleLog($this->conn->error);

        $categories = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $category = new Category();
            $category->setFromResultSet($row);
            $categories[] = $category;
        }

        $resultSet->free();
        return $categories;
    }

    public function selectSubCategories($categoryId, $isActive = false){
        $category = $this->selectById($categoryId);

        $categories = $this->selectByParentId($categoryId);
        foreach($categories as $c){
            $c = $this->selectSubCategories($c->id, $isActive);
            $category->subcategories[] = $c;
        }

        return $category;
    }
} 