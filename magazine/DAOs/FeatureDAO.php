<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 26/09/14
 * Time: 00:09
 */

include_once(SITE_FOLDER . 'Entities/Feature.php');
include_once(SITE_FOLDER . 'DAOs/FeatureOptionDAO.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class FeatureDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO products_features VALUES(NULL,?,?)";
    private $SQL_UPDATE = "UPDATE products_features SET feature_name = ? WHERE product_feature_id = ?";
    private $SQL_SELECT = "SELECT * FROM products_features";
    private $SQL_DELETE = "DELETE FROM products_features";

    private $TAG = "Feature";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Feature $feature){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("is",
            $feature->productId,
            $feature->name
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $feature->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
        return $feature;
    }

    public function update(Feature $feature){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("si",
            $feature->name,
            $feature->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function delete(Feature $feature){
        consoleLog("Deleting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_DELETE . " WHERE product_feature_id = ?");
        $sta->bind_param("i",
            $feature->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Delete error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Delete success.", $this->TAG);
        }

        $sta->close();
    }

    public function selectById($featureId = 0){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE product_feature_id = " . $featureId);

        $feature = new Feature();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $feature->setFromResultSet($row);
        }

        $resultSet->free();
        return $feature;
    }

    public function selectByProductId($productId = 0){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE product_id = " . $productId);

        $features = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $feature = new Feature();
            $feature->setFromResultSet($row);
            $features[] = $feature;
        }

        $resultSet->free();
        return $features;
    }
} 