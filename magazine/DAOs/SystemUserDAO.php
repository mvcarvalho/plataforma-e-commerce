<?php
/**
 * Created by PhpStorm.
 * User: mateu_000
 * Date: 21/01/15
 * Time: 22:24
 */

include_once(SITE_FOLDER . 'Entities/SystemUser.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class SystemUserDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO system_users VALUES (NULL,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE system_users SET login = ?, name = ?, is_active = ? WHERE system_user_id = ?";
    private $SQL_UPDATE_PASSWORD = "UPDATE system_users SET password = ? WHERE system_user_id = ?";
    private $SQL_SELECT = "SELECT * FROM system_users";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(SystemUser $user){
        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("sssi",
            $user->login,
            $user->password,
            $user->name,
            $user->isActive
        );

        $sta->execute();

        if($sta->insert_id === 0){
            $sta->close();
            return false;

        }else{
            $user->id = $sta->insert_id;
            $sta->close();
            return $user;
        }
    }

    public function update(SystemUser $user){
        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("ssii",
            $user->login,
            $user->name,
            $user->isActive,
            $user->id
        );
        $sta->execute();
        $sta->close();
    }

    public function updateUserPassword(SystemUser $user){
        $sta = $this->conn->prepare($this->SQL_UPDATE_PASSWORD);
        $sta->bind_param("si",
            $user->password,
            $user->id
        );
        $sta->execute();
        $sta->close();
    }

    public function select(){
        $resultSet = $this->conn->query($this->SQL_SELECT);

        $users = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user = new SystemUser();
            $user->setFromResultSet($row);
            $users[] = $user;
        }

        $resultSet->free();
        return $users;
    }

    public function selectUserByLoginAndPassword($login, $password){
        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE login = '" . $login . "' AND password = '" . $password . "'");
        $user = new SystemUser();

        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user->setFromResultSet($row);
        }

        $resultSet->free();
        return $user;
    }

    public function selectUserById($userId){
        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE system_user_id = " . $userId);
        $user = new SystemUser();

        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user->setFromResultSet($row);
        }

        $resultSet->free();
        return $user;
    }
} 