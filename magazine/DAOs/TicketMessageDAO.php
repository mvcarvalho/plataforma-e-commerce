<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 30/12/14
 * Time: 11:36
 */

include_once(SITE_FOLDER . 'DAOs/DAO.php');
include_once(SITE_FOLDER . 'Entities/TicketMessage.php');

class TicketMessageDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO tickets_messages VALUES(null,?,?,?,?)";
    private $SQL_SELECT = "SELECT * FROM tickets_messages";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(TicketMessage $ticket){
        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iisi",
            $ticket->isFromUser,
            $ticket->ticketId,
            $ticket->message,
            $ticket->date
        );

        $sta->execute();

        if($sta->insert_id != 0){
            $ticket->id = $sta->insert_id;
        }

        $sta->close();
        return $ticket;
    }

    public function selectByTicket($ticketId){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE ticket_id = ' . $ticketId );
        $tickets = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $ticket = new TicketMessage();
            $ticket->setFromResultSet($row);
            $tickets[] = $ticket;
        }

        return $tickets;
    }

} 