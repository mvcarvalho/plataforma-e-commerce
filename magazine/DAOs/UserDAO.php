<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 23:32
 */

include_once(SITE_FOLDER . 'Entities/User.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class UserDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO users VALUES (NULL,?,?,?,?,?,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE users SET name = ?, cpf = ?, email = ?, phone = ?, address = ?, city = ?, state = ?, cep = ? WHERE user_id = ?";
    private $SQL_UPDATE_PASSWORD = "UPDATE users SET password = ? WHERE user_id = ?";
    private $SQL_SELECT = "SELECT * FROM users";

    private $TAG = "User";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(User $user){
        consoleLog("Inserting.", $this->TAG);

        $user->creation = time();

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("sssssssssi",
            $user->name,
            $user->cpf,
            $user->email,
            $user->phone,
            $user->password,
            $user->address,
            $user->city,
            $user->state,
            $user->cep,
            $user->creation
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
            $sta->close();
            return false;

        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
            $user->id = $sta->insert_id;
            $sta->close();
            return $user;
        }
    }

    public function update(User $user){
        consoleLog("Updating id " . $user->id . ".", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("ssssssssi",
            $user->name,
            $user->cpf,
            $user->email,
            $user->phone,
            $user->address,
            $user->city,
            $user->state,
            $user->cep,
            $user->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function updatePassword(User $user){
        consoleLog("Updating password id " . $user->id . ".", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE_PASSWORD);
        $sta->bind_param("si",
            $user->password,
            $user->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
            $sta->close();
            return false;
        }else{
            consoleLog("Update success.", $this->TAG);
            $sta->close();
            return true;
        }
    }

    public function select(){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $users = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user = new User();
            $user->setFromResultSet($row);
            $users[] = $user;
        }

        $resultSet->free();
        return $users;
    }

    public function selectPage($page, $count){
        consoleLog("Selecting page " . $page .".", $this->TAG);

        $offset = $page * $count;
        $resultSet = $this->conn->query($this->SQL_SELECT . " LIMIT " . $offset . ", " . $count);

        $users = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user = new User();
            $user->setFromResultSet($row);
            $users[] = $user;
        }

        $resultSet->free();
        return $users;
    }

    public function selectUserByEmail($email){
        consoleLog("Selecting by email.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE email = '" . $email . "'") or trigger_error($this->conn->error."[erro]");;
        $user = new User();

        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user->setFromResultSet($row);
        }

        $resultSet->free();
        return $user;
    }

    public function selectUserByEmailAndPassword($email, $password){
        consoleLog("Selecting by email and password.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE email = '" . $email . "' AND password = '" . $password . "'");
        $user = new User();

        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user->setFromResultSet($row);
        }

        $resultSet->free();
        return $user;
    }

    public function selectUserById($userId){
        consoleLog("Selecting by id.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE user_id = " . $userId);
        $user = new User();

        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $user->setFromResultSet($row);
            consoleLog("Selected:", $this->TAG);
            consoleLog($user, $this->TAG);
        }

        $resultSet->free();
        return $user;
    }
} 