<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:12
 */

include_once(SITE_FOLDER . 'Entities/Sale.php');
include_once(SITE_FOLDER . 'Entities/User.php');
include_once(SITE_FOLDER . 'DAOs/SaleItemDAO.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class SaleDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO sales VALUES(null,?,?,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE sales SET status = ?, external_token = ?, payment_request_string = ? WHERE sale_id = ?";
    private $SQL_UPDATE_TRACKING_CODE = "UPDATE sales SET status = ?, tracking_code = ? WHERE sale_id = ?";
    private $SQL_SELECT = "SELECT * FROM sales";
    private $SQL_DELETE = "DELETE FROM sales";
    private $SQL_SELECT_COUNT = "SELECT COUNT(*) FROM sales";
    private $SQL_SELECT_WITH_USERS = "SELECT * FROM sales a, users b WHERE a.user_id = b.user_id";

    private $TAG = "Sale";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Sale $sale){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iidisss",
            $sale->userId,
            $sale->date,
            $sale->value,
            $sale->status,
            $sale->externalToken,
            $sale->trackingCode,
            $sale->paymentRequestString
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $sale->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
        return $sale;
    }

    public function update(Sale $sale){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("issi",
            $sale->status,
            $sale->externalToken,
            $sale->paymentRequestString,
            $sale->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function updateTrackingCode(Sale $sale){
        consoleLog("Updating tracking code.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE_TRACKING_CODE);
        $sta->bind_param("isi",
            $sale->status,
            $sale->trackingCode,
            $sale->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function select($page = 0){
        consoleLog("Selecting page " . $page . ".", $this->TAG);

        $first = $page * 20;

        //$resultSet = $this->conn->query($this->SQL_SELECT_WITH_USERS . ' LIMIT ' . $first . ', 20');
        $resultSet = $this->conn->query($this->SQL_SELECT_WITH_USERS);

        $sales = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $sale = new Sale();
            $sale->setFromResultSet($row);
            $sale->user = new User();
            $sale->user->setFromResultSet($row);
            $sales[] = $sale;
        }

        $resultSet->free();
        return $sales;
    }

    /**
     *
     *
     */
    public function selectByTime($startTime, $endTime, $status = -1, $orderBy = null){
        consoleLog("Selecting by time.", $this->TAG);
        $sSQL = $this->SQL_SELECT_WITH_USERS . " AND date >= " . $startTime . " AND date <= " . $endTime;
        if($status != -1){
            $sSQL .= ' AND status = ' . $status;
        }

        if($orderBy == null) {

            $sSQL .= ' ORDER BY date DESC';
        } else {

            $sSQL .= ' ORDER By '.$orderBy;
        }

        $resultSet = $this->conn->query($sSQL);
        $saleItemDAO = new SaleItemDAO();
        $sales = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $sale = new Sale();
            $sale->setFromResultSet($row);
            $sale->user = new User();
            $sale->user->setFromResultSet($row);
            $sale->items = $saleItemDAO->selectBySaleId($sale->id);
            $sales[] = $sale;
        }

        $resultSet->free();
        return $sales;
    }

    public function selectNumberOfPages(){
        consoleLog("Selecting number of pages.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT_COUNT);

        $total = 0;
        if($row = $resultSet->fetch_array(MYSQLI_BOTH)){
            $total = $resultSet[0];
        }

        $resultSet->free();
        return ceil($total / 20);
    }

    public function deleteById($saleId = 0){
        consoleLog("Deleting by id.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_DELETE  . ' WHERE sale_id = ?');
        $sta->bind_param("i",
            $saleId
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Delete error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Delete success.", $this->TAG);
        }

        $sta->close();
    }

    public function selectById($saleId = 0){
        consoleLog("Selecting by id.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE sale_id = ' . $saleId);

        $sale = new Sale();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            include_once(SITE_FOLDER . 'DAOs/SaleItemDAO.php');
            $saleItemDAO = new SaleItemDAO();

            $sale->setFromResultSet($row);
            $sale->items = $saleItemDAO->selectBySaleId($sale->id);
        }

        $resultSet->free();
        return $sale;
    }

    public function selectByTrackingCode($code = ""){
        consoleLog("Selecting by id.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE external_token = "' . $code . '"');

        $sale = new Sale();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            include_once(SITE_FOLDER . 'DAOs/SaleItemDAO.php');
            $saleItemDAO = new SaleItemDAO();

            $sale->setFromResultSet($row);
            $sale->items = $saleItemDAO->selectBySaleId($sale->id);
        }

        $resultSet->free();
        return $sale;
    }

    public function selectByUserId($userId = 0){
        consoleLog("Selecting by user id.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE user_id = ' . $userId . ' ORDER BY sale_id DESC');

        $sales = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            include_once(SITE_FOLDER . 'DAOs/SaleItemDAO.php');
            $saleItemDAO = new SaleItemDAO();

            $sale = new Sale();
            $sale->setFromResultSet($row);
            $sale->items = $saleItemDAO->selectBySaleId($sale->id);
            $sales[] = $sale;
        }

        $resultSet->free();
        return $sales;
    }
} 