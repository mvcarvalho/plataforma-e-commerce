<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:12
 */

include_once(SITE_FOLDER . 'Entities/CartItem.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class CartItemDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO carts_items VALUES(null,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE carts_items SET cart_id = ?, product_id = ?, price = ?, quantity = ?, options = ? WHERE cart_item_id = ?";
    private $SQL_SELECT = "SELECT * FROM carts_items";
    private $SQL_DELETE = "DELETE FROM carts_items";

    private $TAG = "Cart Item";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(CartItem $cartItem){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iidis",
            $cartItem->cartId,
            $cartItem->productId,
            $cartItem->price,
            $cartItem->quantity,
            $cartItem->optionsText
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $cartItem->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
    }

    public function update(CartItem $cartItem){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("iidisi",
            $cartItem->cartId,
            $cartItem->productId,
            $cartItem->price,
            $cartItem->quantity,
            $cartItem->optionsText,
            $cartItem->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function delete($cartItemId){
        consoleLog("Deleting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_DELETE . ' WHERE cart_item_id = ?');
        $sta->bind_param("i",
            $cartItemId
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function deleteByCartId($cartId){
        consoleLog("Deleting by cartId.", $this->TAG);

        $cartItemOptionDAO = new CartItemOptionDAO();

        $items = $this->selectByCartId($cartId);
        foreach($items as $item){
            $cartItemOptionDAO->deleteByItemId($item->id);
        }

        $sta = $this->conn->prepare($this->SQL_DELETE . ' WHERE cart_id = ?');
        $sta->bind_param("i",
            $cartId
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function select(){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $cartItems = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $cartItem = new CartItem();
            $cartItem->setFromResultSet($row);
            $cartItems[] = $cartItem;
        }

        $resultSet->free();
        return $cartItems;
    }

    public function selectByCartId($cartId = 0){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE cart_id = ' . $cartId);

        include_once(SITE_FOLDER . 'DAOs/CartItemOptionDAO.php');
        include_once(SITE_FOLDER . 'DAOs/ProductDAO.php');
        $cartItemOptionDAO = new CartItemOptionDAO();
        $productDAO = new ProductDAO();

        $cartItems = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $cartItem = new CartItem();
            $cartItem->setFromResultSet($row);

            $cartItem->options = $cartItemOptionDAO->selectByItemId($cartItem->id);
            $cartItem->product = $productDAO->selectById($cartItem->productId);

            $cartItems[] = $cartItem;
        }

        $resultSet->free();
        return $cartItems;
    }

    public function selectById($cartItemId = 0){
        consoleLog("Selecting by id" . $cartItemId . ".", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE cart_item_id = ' . $cartItemId);

        include_once(SITE_FOLDER . 'DAOs/CartItemOptionDAO.php');
        include_once(SITE_FOLDER . 'DAOs/ProductDAO.php');
        $cartItemOptionDAO = new CartItemOptionDAO();
        $productDAO = new ProductDAO();

        $cartItem = new CartItem();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $cartItem->setFromResultSet($row);

            $cartItem->options = $cartItemOptionDAO->selectByItemId($cartItem->id);
            $cartItem->product = $productDAO->selectById($cartItem->productId);
        }

        $resultSet->free();
        return $cartItem;
    }

    public function selectByCartAndProductId($cartId = 0, $productId = 0){
        consoleLog("Selecting by cart id " . $cartId . ' and product id ' . $productId . ".", $this->TAG);

        include_once(SITE_FOLDER . 'DAOs/CartItemOptionDAO.php');
        include_once(SITE_FOLDER . 'DAOs/ProductDAO.php');

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE cart_id = ' . $cartId . ' AND product_id = ' . $productId);
        $cartItemOptionDAO = new CartItemOptionDAO();
        $productDAO = new ProductDAO();

        $cartItem = new CartItem();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $cartItem->setFromResultSet($row);

            $cartItem->options = $cartItemOptionDAO->selectByItemId($cartItem->id);
            $cartItem->product = $productDAO->selectById($cartItem->productId);
        }

        $resultSet->free();
        return $cartItem;
    }
} 