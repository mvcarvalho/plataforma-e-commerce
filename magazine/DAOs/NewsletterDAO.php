<?php
/**
 * Created by PhpStorm.
 * User: rubens
 * Date: 19/01/15
 * Time: 23:11
 */

include_once(SITE_FOLDER . 'Entities/Newsletter.php');
include_once(SITE_FOLDER . 'Entities/Interest.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class NewsletterDAO extends DAO {

    private $SQL_INSERT = "INSERT INTO newsletter VALUES(NULL,?,?)";
    private $SQL_UPDATE = "UPDATE newsletter SET email = ?, sexo = ? WHERE newsletter_id = ?";
    private $SQL_SELECT = "SELECT * FROM newsletter";

    private $SQL_SELECT_EMAIL = "SELECT * FROM newsletter WHERE email = ?";

    private $SQL_SELECT_INTEREST_NEWSLETTER = "SELECT * FROM newsletter_has_categories WHERE newsletter_id = ?";
    private $SQL_INSERT_INTEREST = "INSERT INTO newsletter_has_categories VALUES(?,?)";
    private $SQL_SELECT_INTEREST_CATEGORY = "SELECT * FROM newsletter_has_categories WHERE CATEGORY_id = ?";

    private $TAG = "Newsletter";
    private $TAG_NEWSLETTER = "Newsletter_has_category";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Newsletter $news) {
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("ss",
            $news->email,
            $news->sexo
        );

        $sta->execute();
        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
            $news->newsletter_id = $sta->insert_id;

            foreach($news->interest as $a) {

                $a->newsletter_id = $news->newsletter_id;
                $this->insertInterest($a);
            }
        }

        $sta->close();
        return $news;
    }

    public function update(Newsletter $news){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("ssi",
            $news->email,
            $news->sexo,
            $news->newsletter_id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }


    /**
     *
     * @param $interest Array(category_id, newsletter_id)
     */
    public function insertInterest(Interest $interest) {
        consoleLog("Inserting interest ", $this->TAG_NEWSLETTER);

        $sta = $this->conn->prepare($this->SQL_INSERT_INTEREST);
        $sta->bind_param("ii",
            $interest->newsletter_id,
            $interest->category_id
        );

        $sta->execute();
        if($this->conn->error){

            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG_NEWSLETTER);
            return false;
        }else{

            consoleLog("Insert success with category_id " . $interest->category_id . ".", $this->TAG_NEWSLETTER);
        }

        $sta->close();
        return true;
    }

    public function select(){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $news = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $new = new Newsletter();
            $new->setFromResultSet($row);
            $news[] = $new;
        }


        $resultSet->free();
        return $news;
    }

    public function selectCategories($arrayCategories){
        consoleLog("Selecting Categories.", $this->TAG);

        $select = "SELECT news.*, cat.name as cat_name FROM newsletter news,newsletter_has_categories news_cat,categories cat WHERE ";

        $i = 0;

        $select .= "(";
        foreach($arrayCategories as $value) {
            consoleLog(" -- INDICE ".$i);
            consoleLog("Valor " . $value);
            $select .= "cat.category_id = ".$value;

            if($arrayCategories[$i+1] != null)
                $select .= " OR ";

            $i++;
        }
        $select .= ")";
        $select .= " AND news.newsletter_id = news_cat.newsletter_id AND news_cat.category_id = cat.category_id GROUP By news.email";

        $resultSet = $this->conn->query($select);

        $news = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $new = new Newsletter();
            $new->setFromResultSet($row);
            $new->interest = $row['cat_name'];
            $news[] = $new;
        }

        $resultSet->free();
        return $news;
    }

    public function getNewsWithEmail($email) {
        $select = "SELECT * FROM newsletter WHERE email = '".$email."'";

        $resultSet = $this->conn->query($select);

        $new = new Newsletter();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)) {

//            $new = new Newsletter();
            $new->setFromResultSet($row);
        }

        consoleLog("RUBENS ".$new->email);
        return $new;
    }
} 