<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 13/11/14
 * Time: 20:37
 */

include_once(SITE_FOLDER . 'Entities/QuickInfo.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class QuickInfoDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO products_quick_infos VALUES(NULL,?,?,?)";
    private $SQL_UPDATE = "UPDATE products_quick_infos SET product_id = ?, name = ?, description = ? WHERE quick_info_id = ?";
    private $SQL_SELECT = "SELECT * FROM products_quick_infos";
    private $SQL_DELETE = "DELETE FROM products_quick_infos";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(QuickInfo $quickInfo){

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iss",
            $quickInfo->productId,
            $quickInfo->name,
            $quickInfo->description
        );

        $sta->execute();

        if($sta->insert_id != 0){
            $quickInfo->id = $sta->insert_id;
        }

        $sta->close();
        return $quickInfo;
    }

    public function update(QuickInfo $quickInfo){

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("issi",
            $quickInfo->productId,
            $quickInfo->name,
            $quickInfo->description,
            $quickInfo->id
        );

        $sta->execute();
        $sta->close();
    }

    public function delete($quickInfoId){
        $sta = $this->conn->prepare($this->SQL_DELETE . ' WHERE quick_info_id = ' . $quickInfoId);
        $sta->execute();
        $sta->close();
    }

    public function selectByProductId($productId){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE product_id = ' . $productId);

        $infos = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $info = new QuickInfo();
            $info->setFromResultSet($row);
            $infos[] = $info;
        }

        $resultSet->free();
        return $infos;
    }

    public function selectById($infoId){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE quick_info_id = ' . $infoId);

        $info = new QuickInfo();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $info->setFromResultSet($row);
        }

        $resultSet->free();
        return $info;
    }
} 