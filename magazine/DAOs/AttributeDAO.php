<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 26/09/14
 * Time: 00:09
 */

include_once(SITE_FOLDER . 'Entities/Attribute.php');
include_once(SITE_FOLDER . 'DAOs/AttributeOptionDAO.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class AttributeDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO attributes VALUES(NULL,?,?,?)";
    private $SQL_UPDATE = "UPDATE attributes SET name = ?, observation = ?, is_required = ? WHERE attribute_id = ?";
    private $SQL_SELECT = "SELECT * FROM attributes";

    private $TAG = "Attribute";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Attribute $attribute){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("ssi",
            $attribute->name,
            $attribute->observation,
            $attribute->isRequired
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $attribute->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
        return $attribute;
    }

    public function update(Attribute $attribute){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("ssii",
            $attribute->name,
            $attribute->observation,
            $attribute->isRequired,
            $attribute->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function select(){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $attributes = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $attribute = new Attribute();
            $attribute->setFromResultSet($row);
            $attributes[] = $attribute;
        }

        $resultSet->free();
        return $attributes;
    }

    public function selectById($attributeId){
        consoleLog("Selecting by id.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE attribute_id = " . $attributeId);
        $attribute = new Attribute();

        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $attributeOptionDAO = new AttributeOptionDAO();
            $attribute->setFromResultSet($row);
            $attribute->options = $attributeOptionDAO->selectByAttributeId($attribute->id);
        }

        $resultSet->free();
        return $attribute;
    }
} 