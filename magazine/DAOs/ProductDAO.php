<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 23:48
 */

include_once(SITE_FOLDER . 'Entities/Product.php');
include_once(SITE_FOLDER . 'DAOs/AttributeDAO.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class ProductDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO products VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE products SET category_id = ?, name = ?, description = ?, rules = ?, normal_price = ?, offer_price = ?, validity = ?, sales = ?, views = ?, image = ?, is_active = ?, delivery_days = ?, delivery_cost = ?, short_description = ?, promotional_text = ?, technical_data = ?, items_included = ? WHERE product_id = ?";
    private $SQL_SELECT = "SELECT * FROM products";

    private $SQL_INSERT_ATTRIBUTE = "INSERT INTO products_attributes VALUES(?,?,?)";
    private $SQL_DELETE_ATTRIBUTE = "DELETE FROM products_attributes WHERE product_id = ? AND attribute_id = ?";
    private $SQL_UPDATE_ATTRIBUTE = "UPDATE products_attributes SET quantity = ? WHERE product_id = ? AND attribute_id = ?";
    private $SQL_SELECT_ATTRIBUTE = "SELECT * FROM products_attributes";

    private $SQL_INSERT_IMAGE = "INSERT INTO products_images VALUES(?,?)";
    private $SQL_DELETE_IMAGE = "DELETE FROM products_images WHERE product_id = ? AND image = ?";
    private $SQL_SELECT_IMAGE = "SELECT * FROM products_images";

    private $TAG = "Product";
    private $TAG_ATTRIBUTE = "Product Attribute";
    private $TAG_IMAGE = "Product Image";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Product $product){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);

        $sta->bind_param("isssddiiisisdssss",
            $product->categoryId,
            $product->name,
            $product->description,
            $product->rules,
            $product->normalPrice,
            $product->offerPrice,
            $product->validity,
            $product->sales,
            $product->views,
            $product->image,
            $product->isActive,
            $product->deliveryDays,
            $product->deliveryCost,
            $product->shortDescription,
            $product->promotionalText,
            $product->technicalData,
            $product->itemsIncluded
        );

        var_dump($sta->execute());
//        $sta->execute();
        var_dump($this->conn->error);
        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
            $product->id = $sta->insert_id;
        }

        $sta->close();
        return $product;
    }

    public function update(Product $product){
        consoleLog("Updating.", $this->TAG);

        if($product->image == "" || empty($product->image)){
            $tmpProduct = $this->selectById($product->id);
            $product->image = $tmpProduct->image;
        }

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("isssddsiisisdssssi",
            $product->categoryId,
            $product->name,
            $product->description,
            $product->rules,
            $product->normalPrice,
            $product->offerPrice,
            $product->validity,
            $product->sales,
            $product->views,
            $product->image,
            $product->isActive,
            $product->deliveryDays,
            $product->deliveryCost,
            $product->shortDescription,
            $product->promotionalText,
            $product->technicalData,
            $product->itemsIncluded,
            $product->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function selectAll($active = false){
        consoleLog("Selecting all.", $this->TAG);

        if($active){
            $resultSet = $this->conn->query($this->SQL_SELECT . " WhERE is_active = 1 ORDER BY name");
        }else{
            $resultSet = $this->conn->query($this->SQL_SELECT . ' ORDER BY name');
        }

        $products = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $product = new Product();
            $product->setFromResultSet($row);
            $product->attributes = $this->selectAttributes($product->id);
            $product->extraImages = $this->selectImages($product->id);
            $products[] = $product;
        }

        $resultSet->free();
        return $products;
    }

    public function selectBySearch($search, $active = false){
        consoleLog("Selecting all.", $this->TAG);

        $search = mysqli_real_escape_string($this->conn, $search);

        if($active){
            $sSQL = $this->SQL_SELECT . " WhERE name like '%" . $search . "%' and is_active = 1 ORDER BY name";
        }else{
            $sSQL = $this->SQL_SELECT . " WhERE name like '%" . $search . "%' ORDER BY name";
        }

        $resultSet = $this->conn->query($sSQL);

        $products = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $product = new Product();
            $product->setFromResultSet($row);
            $product->attributes = $this->selectAttributes($product->id);
            $product->extraImages = $this->selectImages($product->id);
            $products[] = $product;
        }

        $resultSet->free();
        return $products;
    }

    public function selectRandom($active = false){
        consoleLog("Selecting all random.", $this->TAG);

        if($active){
            $resultSet = $this->conn->query($this->SQL_SELECT . " WhERE is_active = 1 ORDER BY RAND() LIMIT 16");
        }else{
            $resultSet = $this->conn->query($this->SQL_SELECT . ' ORDER BY name');
        }

        $products = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $product = new Product();
            $product->setFromResultSet($row);
            $product->attributes = $this->selectAttributes($product->id);
            $product->extraImages = $this->selectImages($product->id);
            $products[] = $product;
        }

        $resultSet->free();
        return $products;
    }

    public function selectById($productId){
        consoleLog("Selecting id " .$productId . ".", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE product_id = " . $productId);

        $product = new Product();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $product->setFromResultSet($row);
            $product->attributes = $this->selectAttributes($product->id);
            $product->extraImages = $this->selectImages($product->id);
        }

        $resultSet->free();
        return $product;
    }

    public function selectByCategoryId($categoryId, $actives = false, $random = false){
        consoleLog("Selecting category id " .$categoryId . ".", $this->TAG);

        $query = $this->SQL_SELECT . " WHERE category_id = " . $categoryId;

        if($actives){
            $query .= " AND is_active = 1";
        }

        if($random){
            $query .= " ORDER BY RAND()";
        }else{
            $query .= " ORDER BY name";
        }

        $resultSet = $this->conn->query($query);

        $products = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $product = new Product();
            $product->setFromResultSet($row);
            $product->attributes = $this->selectAttributes($product->id);
            $product->extraImages = $this->selectImages($product->id);
            $products[] = $product;
        }

        $resultSet->free();
        return $products;
    }

    public function insertProductAttribute($productId, $attributeId, $quantity){
        consoleLog("Inserting.", $this->TAG_ATTRIBUTE);

        $sta = $this->conn->prepare($this->SQL_INSERT_ATTRIBUTE);
        $sta->bind_param("iii",
            $productId,
            $attributeId,
            $quantity
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG_ATTRIBUTE);
        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG_ATTRIBUTE);
        }

        $sta->close();
    }

    public function updateProductAttribute($productId, $attributeId, $quantity){
        consoleLog("Updating.", $this->TAG_ATTRIBUTE);

        $sta = $this->conn->prepare($this->SQL_UPDATE_ATTRIBUTE);
        $sta->bind_param("iii",
            $quantity,
            $productId,
            $attributeId
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG_ATTRIBUTE);
        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG_ATTRIBUTE);
        }

        $sta->close();
    }

    public function deleteProductAttribute($productId, $attributeId){
        consoleLog("Deleting.", $this->TAG_ATTRIBUTE);

        $sta = $this->conn->prepare($this->SQL_DELETE_ATTRIBUTE);
        $sta->bind_param("ii",
            $productId,
            $attributeId
        );

        $sta->execute();

        if($sta->affected_rows === 0){
            consoleLog("Delete error. SQL error: " . $this->conn->error, $this->TAG_ATTRIBUTE);
        }else{
            consoleLog("Delete success on " . $sta->affected_rows . " row(s).", $this->TAG_ATTRIBUTE);
        }

        $sta->close();
    }

    public function selectAttributes($productId){
        consoleLog("Selecting all.", $this->TAG_ATTRIBUTE);

        $resultSet = $this->conn->query($this->SQL_SELECT_ATTRIBUTE . " WHERE product_id = " . $productId);
        $attributes = array();
        $attributeDAO = new AttributeDAO();

        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $attribute = $attributeDAO->selectById($row['attribute_id']);
            $attribute->quantity = $row['quantity'];
            $attributes[] = $attribute;
        }

        $resultSet->free();
        return $attributes;
    }

    public function insertProductImage($productId, $imageFile){
        $sta = $this->conn->prepare($this->SQL_INSERT_IMAGE);
        $sta->bind_param("is",
            $productId,
            $imageFile
        );

        $sta->execute();
        $sta->close();
    }

    public function deleteProductImage($productId, $imageFile){
        $sta = $this->conn->prepare($this->SQL_DELETE_IMAGE);
        $sta->bind_param("is",
            $productId,
            $imageFile
        );

        $sta->execute();
        $sta->close();
    }

    public function selectImages($productId){
        $resultSet = $this->conn->query($this->SQL_SELECT_IMAGE . " WHERE product_id = " . $productId);
        $images = array();

        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $images[] = $row['image'];
        }

        $resultSet->free();
        return $images;
    }

    public function selectProductIdByImage($image){
        $resultSet = $this->conn->query($this->SQL_SELECT_IMAGE . " WHERE image = '" . $image . "'");

        $id = 0;
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $id = $row['product_id'];
        }

        $resultSet->free();
        return $id;
    }
} 