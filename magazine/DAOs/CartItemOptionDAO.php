<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:12
 */

include_once(SITE_FOLDER . 'Entities/CartItemOption.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class CartItemOptionDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO carts_items_options VALUES(null,?,?,?)";
    private $SQL_UPDATE = "UPDATE carts_items_options SET cart_item_id = ?, attribute_option_id = ?, price_change = ? WHERE cart_item_option_id = ?";
    private $SQL_SELECT = "SELECT * FROM carts_items_options";
    private $SQL_DELETE = "DELETE FROM carts_items_options";

    private $TAG = "Cart Item Option";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(CartItemOption $cartItemOption){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iid",
            $cartItemOption->cartItemId,
            $cartItemOption->attributeId,
            $cartItemOption->priceChange
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
    }

    public function update(CartItemOption $cartItemOption){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("iidi",
            $cartItemOption->cartItemId,
            $cartItemOption->attributeId,
            $cartItemOption->priceChange,
            $cartItemOption->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function deleteByItemId($itemId){
        consoleLog("Deleting by item ID.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_DELETE . ' WHERE cart_item_id = ?');
        $sta->bind_param("i",
            $itemId
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Delete error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Delete success.", $this->TAG);
        }

        $sta->close();
    }

    public function select(){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $cartItemOptions = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $cartItemOption = new CartItemOption();
            $cartItemOption->setFromResultSet($row);
            $cartItemOptions[] = $cartItemOption;
        }

        $resultSet->free();
        return $cartItemOptions;
    }

    public function selectByItemId($itemId = 0){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE cart_item_id = ' . $itemId);

        $cartItemOptions = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $cartItemOption = new CartItemOption();
            $attributeOptionDAO = new AttributeOptionDAO();

            $cartItemOption->setFromResultSet($row);
            $cartItemOption->attribute = $attributeOptionDAO->selectById($cartItemOption->attributeId);

            $cartItemOptions[] = $cartItemOption;
        }

        $resultSet->free();
        return $cartItemOptions;
    }
} 