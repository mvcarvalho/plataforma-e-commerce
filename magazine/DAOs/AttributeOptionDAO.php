<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 26/09/14
 * Time: 00:14
 */

include_once(SITE_FOLDER . 'Entities/AttributeOption.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class AttributeOptionDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO attributes_options VALUES(NULL,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE attributes_options SET attribute_id = ?, name = ?, price_change = ?, observation = ? WHERE option_id = ?";
    private $SQL_SELECT = "SELECT * FROM attributes_options";
    private $SQL_DELETE = "DELETE FROM attributes_options";

    private $TAG = "Attribute Option";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(AttributeOption $attributeOption){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("isds",
            $attributeOption->attributeId,
            $attributeOption->name,
            $attributeOption->priceChange,
            $attributeOption->observation
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
    }

    public function update(AttributeOption $attributeOption){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("isdsi",
            $attributeOption->attributeId,
            $attributeOption->name,
            $attributeOption->priceChange,
            $attributeOption->observation,
            $attributeOption->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function delete($attributeOptionId){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_DELETE . ' WHERE option_id = ?');
        $sta->bind_param("i",
            $attributeOptionId
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function select(AttributeOption $attributeOption){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $attributes = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $attribute = new AttributeOption();
            $attribute->setFromResultSet($row);
            $attributes[] = $attribute;
        }

        $resultSet->free();
        return $attributes;
    }

    public function selectByAttributeId($attributeId){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE attribute_id = " . $attributeId);

        $attributes = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $attribute = new AttributeOption();
            $attribute->setFromResultSet($row);
            $attributes[] = $attribute;
        }

        $resultSet->free();
        return $attributes;
    }

    public function selectById($optionId){
        consoleLog("Selecting.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . " WHERE option_id = " . $optionId);

        $attribute = new AttributeOption();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $attribute->setFromResultSet($row);
        }

        $resultSet->free();
        return $attribute;
    }
} 