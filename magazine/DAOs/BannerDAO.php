<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:12
 */

include_once(SITE_FOLDER . 'Entities/Banner.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class BannerDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO banners VALUES(null,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE banners SET banner_image = ?, banner_url = ?, banner_type = ?, banner_order = ? WHERE banner_id = ?";
    private $SQL_SELECT = "SELECT * FROM banners";
    private $SQL_DELETE = "DELETE FROM banners";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Banner $banner){
        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("sssi",
            $banner->image,
            $banner->url,
            $banner->type,
            $banner->order
        );
        $sta->execute();
        if($sta->insert_id > 0){
            $banner->id = $sta->insert_id;
        }
        $sta->close();
    }

    public function update(Banner $banner){
        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("sssii",
            $banner->image,
            $banner->url,
            $banner->type,
            $banner->order,
            $banner->id
        );
        $sta->execute();
        $sta->close();
    }

    public function deleteById($bannerId){
        $sta = $this->conn->prepare($this->SQL_DELETE . ' WHERE banner_id = ?');
        $sta->bind_param("i",
            $bannerId
        );
        $sta->execute();
        $sta->close();
    }

    public function select(){
        $resultSet = $this->conn->query($this->SQL_SELECT);
        $banners = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $banner = new Banner();
            $banner->setFromResultSet($row);
            $banners[] = $banner;
        }
        $resultSet->free();
        return $banners;
    }

    public function selectByType($type){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE banner_type = "' . $type . '" order by banner_order');
        $banners = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $banner = new Banner();
            $banner->setFromResultSet($row);
            $banners[] = $banner;
        }
        $resultSet->free();
        return $banners;
    }

    public function selectById($id){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE banner_id = ' . $id);
        $banner = new Banner();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $banner->setFromResultSet($row);
        }
        $resultSet->free();
        return $banner;
    }
} 