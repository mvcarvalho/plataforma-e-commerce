<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 30/12/14
 * Time: 11:11
 */

include_once(SITE_FOLDER . 'DAOs/DAO.php');
include_once(SITE_FOLDER . 'DAOs/TicketMessageDAO.php');
include_once(SITE_FOLDER . 'Entities/Ticket.php');

class TicketDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO tickets VALUES(null,?,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE tickets SET status = ?, responsible_id = ? WHERE ticket_id = ?";
    private $SQL_SELECT = "SELECT * FROM tickets";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Ticket $ticket){
        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iisiii",
            $ticket->date,
            $ticket->status,
            $ticket->subject,
            $ticket->saleId,
            $ticket->responsibleId,
            $ticket->userId
        );

        $sta->execute();

        if($sta->insert_id != 0){
            $ticket->id = $sta->insert_id;
        }

        $sta->close();
        return $ticket;
    }

    public function update(Ticket $ticket){
        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("iii",
            $ticket->status,
            $ticket->responsibleId,
            $ticket->id
        );

        $sta->execute();
        $sta->close();
    }

    public function selectByResponsible($responsibleId = 0){
        if($responsibleId != 0){
            $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE responsible_id IN (0, ' . $responsibleId . ')' );
        }else{
            $resultSet = $this->conn->query($this->SQL_SELECT);
        }

        $tickets = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $ticket = new Ticket();
            $ticket->setFromResultSet($row);
            $tickets[] = $ticket;
        }

        return $tickets;
    }

    public function selectByUser($userId){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE user_id = ' . $userId );
        $tickets = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $ticket = new Ticket();
            $ticket->setFromResultSet($row);
            $tickets[] = $ticket;
        }

        return $tickets;
    }

    public function selectById($id){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE ticket_id = ' . $id );
        $ticket = new Ticket();
        $messagesDAO = new TicketMessageDAO();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $ticket->setFromResultSet($row);
            $ticket->messages = $messagesDAO->selectByTicket($ticket->id);
        }
        return $ticket;
    }
} 