<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:12
 */

include_once(SITE_FOLDER . 'Entities/Coupon.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class CouponDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO coupons VALUES(null,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE coupons SET coupon_key = ?, coupon_off = ?, coupon_off_type = ?, coupon_min_value = ?, coupon_validity = ? WHERE coupon_id = ?";
    private $SQL_SELECT = "SELECT * FROM coupons";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(Coupon $coupon){
        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("sdidi",
            $coupon->key,
            $coupon->off,
            $coupon->offType,
            $coupon->minValue,
            $coupon->validity
        );
        $sta->execute();
        if($sta->insert_id > 0){
            $coupon->id = $sta->insert_id;
        }
        $sta->close();
    }

    public function update(Coupon $coupon){
        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("sdidii",
            $coupon->key,
            $coupon->off,
            $coupon->offType,
            $coupon->minValue,
            $coupon->validity,
            $coupon->id
        );
        $sta->execute();
        $sta->close();
    }

    public function select(){
        $resultSet = $this->conn->query($this->SQL_SELECT);
        $coupons = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $coupon = new Coupon();
            $coupon->setFromResultSet($row);
            $coupons[] = $coupon;
        }
        $resultSet->free();
        return $coupons;
    }

    public function selectById($id){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE coupon_id = ' . $id);
        $coupon = new Coupon();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $coupon->setFromResultSet($row);
        }
        $resultSet->free();
        return $coupon;
    }

    public function selectByKey($key){
        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE coupon_key = "' . $key . '"');
        $coupon = new Coupon();
        if($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $coupon->setFromResultSet($row);
        }
        $resultSet->free();
        return $coupon;
    }
} 