<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 05/10/14
 * Time: 20:12
 */

include_once(SITE_FOLDER . 'Entities/SaleItem.php');
include_once(SITE_FOLDER . 'DAOs/DAO.php');

class SaleItemDAO extends DAO{

    private $SQL_INSERT = "INSERT INTO sales_items VALUES(null,?,?,?,?,?)";
    private $SQL_UPDATE = "UPDATE sales_items SET sale_id = ?, product_id = ?, price = ?, quantity = ?, options = ? WHERE sale_item_id = ?";
    private $SQL_SELECT = "SELECT * FROM sales_items";

    private $TAG = "Sale Item";

    public function __construct(){
        $this->openConnection();
    }

    public function __destruct(){
        $this->closeConnection();
    }

    public function insert(SaleItem $saleItem){
        consoleLog("Inserting.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_INSERT);
        $sta->bind_param("iidis",
            $saleItem->saleId,
            $saleItem->productId,
            $saleItem->price,
            $saleItem->quantity,
            $saleItem->optionsText
        );

        $sta->execute();

        if($sta->insert_id === 0){
            consoleLog("Insert error. SQL error: " . $this->conn->error, $this->TAG);
        }else{
            $saleItem->id = $sta->insert_id;
            consoleLog("Insert success with id " . $sta->insert_id . ".", $this->TAG);
        }

        $sta->close();
        return $saleItem;
    }

    public function update(SaleItem $saleItem){
        consoleLog("Updating.", $this->TAG);

        $sta = $this->conn->prepare($this->SQL_UPDATE);
        $sta->bind_param("iidisi",
            $saleItem->saleId,
            $saleItem->productId,
            $saleItem->price,
            $saleItem->quantity,
            $saleItem->optionsText,
            $saleItem->id
        );

        $sta->execute();

        if($sta->error){
            consoleLog("Update error. SQL error: " . $sta->error, $this->TAG);
        }else{
            consoleLog("Update success.", $this->TAG);
        }

        $sta->close();
    }

    public function select(SaleItem $saleItem){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT);

        $saleItems = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $saleItem = new SaleItem();
            $saleItem->setFromResultSet($row);
            $saleItems[] = $saleItem;
        }

        $resultSet->free();
        return $saleItems;
    }

    public function selectBySaleId($saleId = 0){
        consoleLog("Selecting all.", $this->TAG);

        $resultSet = $this->conn->query($this->SQL_SELECT . ' WHERE sale_id = ' . $saleId);

        include_once(SITE_FOLDER . 'DAOs/SaleItemOptionDAO.php');
        include_once(SITE_FOLDER . 'DAOs/ProductDAO.php');
        $saleItemOptionDAO = new SaleItemOptionDAO();
        $productDAO = new ProductDAO();

        $saleItems = array();
        while($row = $resultSet->fetch_array(MYSQLI_ASSOC)){
            $saleItem = new SaleItem();
            $saleItem->setFromResultSet($row);

            $saleItem->options = $saleItemOptionDAO->selectByItemId($saleItem->id);
            $saleItem->product = $productDAO->selectById($saleItem->productId);

            foreach($saleItem->options as $option){
                $saleItem->product->name .= ' - ' . $option->attribute->name;
            }

            $saleItems[] = $saleItem;
        }

        $resultSet->free();
        return $saleItems;
    }
} 