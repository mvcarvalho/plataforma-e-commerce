<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 13/11/14
 * Time: 20:33
 */

class QuickInfo {

    public $id;
    public $productId;
    public $name;
    public $description;

    public function __construct(){
        $this->id = 0;
        $this->productId = 0;
        $this->name = "";
        $this->description = "";
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['quick_info_id'];
            $this->productId = $resultSet['product_id'];
            $this->name = $resultSet['name'];
            $this->description = $resultSet['description'];
        }catch (Exception $e){

        }
    }
} 