<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 19/11/14
 * Time: 19:27
 */

class PaymentNotification {

    public $id;
    public $gatewayName;
    public $gatewayId;
    public $paymentMethod;
    public $value;
    public $date;
    public $status;

    public function __construct(){
        $this->id = 0;
        $this->gatewayName = '';
        $this->gatewayId = '';
        $this->paymentMethod = '';
        $this->value = 0;
        $this->date = 0;
        $this->status = '';
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['payment_id'];
            $this->gatewayName = $resultSet['gateway_name'];
            $this->gatewayId = $resultSet['gateway_id'];
            $this->paymentMethod = $resultSet['payment_method'];
            $this->value = $resultSet['value'];
            $this->date = $resultSet['notification_date'];
            $this->status = $resultSet['status'];
        }catch (Exception $e){
        }
    }
} 