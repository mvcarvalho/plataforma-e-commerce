<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:26
 */

include_once(SITE_FOLDER . 'Entities/AttributeOption.php');

class CartItemOption {

    public $id;
    public $cartItemId;
    public $attributeId;
    public $priceChange;
    public $attribute;

    public function __construct(){
        $this->id = 0;
        $this->cartItemId = 0;
        $this->attributeId = 0;
        $this->priceChange = 0.0;
        $this->attribute = new AttributeOption();
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["cart_item_option_id"];
            $this->cartItemId = $resultSet["cart_item_id"];
            $this->attributeId = $resultSet["attribute_option_id"];
            $this->priceChange = $resultSet["price_change"];
        }
    }
} 