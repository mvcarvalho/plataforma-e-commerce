<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:53
 */

include_once(SITE_FOLDER . 'Entities/AttributeOption.php');

class SaleItemOption {

    public $id;
    public $saleItemId;
    public $attributeId;
    public $priceChange;
    public $attribute;

    public function __construct(){
        $this->id = 0;
        $this->saleItemId = 0;
        $this->attributeId = 0;
        $this->priceChange = 0.0;
        $this->attribute = new AttributeOption();
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["sale_item_option_id"];
            $this->saleItemId = $resultSet["sale_item_id"];
            $this->attributeId = $resultSet["attribute_id"];
            $this->priceChange = $resultSet["price_change"];
        }
    }
}