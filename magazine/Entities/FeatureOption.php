<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 02/12/14
 * Time: 21:47
 */

class FeatureOption {

    public $id;
    public $featureId;
    public $name;

    public function __construct(){
        $this->id = 0;
        $this->featureId = 0;
        $this->name = 0;
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['feature_option_id'];
            $this->featureId = $resultSet['product_feature_id'];
            $this->name = $resultSet['name'];
        }catch (Exception $e){

        }
    }
} 