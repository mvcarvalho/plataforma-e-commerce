<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:52
 */

class Sale {

    public $id;
    public $userId;
    public $date;
    public $value;
    public $status;
    public $items;
    public $externalToken;
    public $trackingCode;
    public $paymentRequestString;
    public $user;

    public function __construct(){
        $this->id = 0;
        $this->userId = 0;
        $this->date = time();
        $this->value = 0.0;
        $this->status = "";
        $this->items = array();
        $this->externalToken = "";
        $this->trackingCode = "";
        $this->paymentRequestString = "";
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["sale_id"];
            $this->userId = $resultSet["user_id"];
            $this->date = date('Y-m-d', $resultSet["date"]);
            $this->value = $resultSet["value"];
            $this->status = $resultSet["status"];
            $this->externalToken = $resultSet["external_token"];
            $this->trackingCode = $resultSet["tracking_code"];
            $this->paymentRequestString = $resultSet["payment_request_string"];
        }
    }
} 