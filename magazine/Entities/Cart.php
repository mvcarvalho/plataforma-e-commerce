<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:25
 */

class Cart {

    public $id;
    public $userId;
    public $creationDate;
    public $coupon;
    public $items;

    public function __construct(){
        $this->id = 0;
        $this->userId = 0;
        $this->creationDate = time();
        $this->coupon = "";
        $this->items = array();
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["cart_id"];
            $this->userId = $resultSet["user_id"];
            $this->creationDate = $resultSet["creation_date"];
            $this->coupon = $resultSet["cupom"];
        }
    }
} 