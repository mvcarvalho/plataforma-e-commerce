<?php

class Coupon {
    public $id;
    public $key;
    public $off;
    public $offType;
    public $minValue;
    public $validity;

    public function __construct(){
        $this->id = 0;
        $this->key = "";
        $this->off = 0.0;
        $this->offType = 0;
        $this->minValue = 0.0;
        $this->validity = time();
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['coupon_id'];
            $this->key = $resultSet['coupon_key'];
            $this->off = $resultSet['coupon_off'];
            $this->offType = $resultSet['coupon_off_type'];
            $this->minValue = $resultSet['coupon_min_value'];
            $this->validity = $resultSet['coupon_validity'];
        }catch (Exception $e){}
    }
}