<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:05
 */

class Product {

    public $id;
    public $categoryId;
    public $name;
    public $description;
    public $rules;
    public $normalPrice;
    public $offerPrice;
    public $validity;
    public $sales;
    public $views;
    public $image;
    public $isActive;
    public $extraImages;
    public $attributes;
    public $deliveryDays;
    public $deliveryCost;
    public $shortDescription;
    public $promotionalText;
    public $technicalData;
    public $itemsIncluded;

    public function __construct(){
        $this->id = 0;
        $this->categoryId = 0;
        $this->description = "";
        $this->rules = "";
        $this->normalPrice = 0.0;
        $this->offerPrice = 0.0;
        $this->validity = date('Y-m-d');
        $this->sales = 0;
        $this->views = 0;
        $this->image = "";
        $this->isActive = false;
        $this->name = "";
        $this->attributes = array();
        $this->extraImages = array();
        $this->deliveryCost = 0;
        $this->deliveryDays = 0;
        $this->shortDescription = "";
        $this->promotionalText = "";
        $this->technicalData = "";
        $this->itemsIncluded = "";
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["product_id"];
            $this->categoryId = $resultSet["category_id"];
            $this->description = $resultSet["description"];
            $this->rules = $resultSet["rules"];
            $this->normalPrice = $resultSet["normal_price"];
            $this->offerPrice = $resultSet["offer_price"];
            $this->validity = date('Y-m-d', strtotime($resultSet["validity"]));
            $this->sales = $resultSet["sales"];
            $this->views = $resultSet["views"];
            $this->image = $resultSet["image"];
            $this->name = $resultSet["name"];
            $this->isActive = $resultSet["is_active"];
            $this->deliveryCost = $resultSet["delivery_cost"];
            $this->deliveryDays = $resultSet["delivery_days"];
            $this->shortDescription = $resultSet["short_description"];
            $this->promotionalText = $resultSet["promotional_text"];
            $this->technicalData = $resultSet["technical_data"];
            $this->itemsIncluded = $resultSet["items_included"];

            consoleLog($this);
        }
    }
}