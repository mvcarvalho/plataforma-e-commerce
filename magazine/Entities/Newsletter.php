<?php
/**
 * Created by PhpStorm.
 * User: rubens
 * Date: 19/01/15
 * Time: 22:57
 */



class Newsletter {

    public $newsletter_id;
    public $email;
    public $sexo;


    /**
     *
     * Se o objeto que for inserido não possuir newsletter_id deverá ser enviado 0
     *
     * @var array(
     *  Interest(newsletter_id, category_id),
     *  Interest(newsletter_id, category_id),
     *  Interest(newsletter_id, category_id),
     *              .
     *              .
     *              .
     *              .
     * )
     *
     * Ex:  $t = new Newsletter();

            $arr = array();
            $arr[] = new Interest($t->newsletter_id,15);
            $arr[] = new Interest($t->newsletter_id,16);
            $arr[] = new Interest($t->newsletter_id,17);
            $arr[] = new Interest($t->newsletter_id,18);

            $t->email = "rubens.machion@gmail2.com";
            $t->sexo = "M";
            $t->interest = $arr;

            NewsletterManager->saveNewsletter($t);
     *
     *
     */
    public $interest;

    public function __construct(){
        $this->newsletter_id = 0;
        $this->email = "";
        $this->sexo = "";
        $this->interest = array();
    }

    /**
     *
     * @param $resultSet
     *
     * @param $interest
     *
     */
    public function setFromResultSet($resultSet, $interest = null){
        if($resultSet != null) {
            $this->newsletter_id = $resultSet['newsletter_id'];
            $this->email = $resultSet['email'];
            $this->sexo = $resultSet['sexo'];
            $this->interest = $interest != null ? $interest : null;
//            consoleLog($this);
        }
    }
} 