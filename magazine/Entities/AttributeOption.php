<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:06
 */

class AttributeOption {
    public $id;
    public $attributeId;
    public $name;
    public $priceChange;
    public $observation;

    public function __construct(){
        $this->id = 0;
        $this->attributeId = 0;
        $this->name = "";
        $this->priceChange = 0.0;
        $this->observation = 0;
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["option_id"];
            $this->attributeId = $resultSet["attribute_id"];
            $this->name = $resultSet["name"];
            $this->priceChange = $resultSet["price_change"];
            $this->observation = $resultSet["observation"];
        }
    }
} 