<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:05
 */

class Category {

    public $id;
    public $parentCategoryId;
    public $name;
    public $description;
    public $image;
    public $subcategories;
    public $isActive;

    public function __construct(){
        $this->id = 0;
        $this->parentCategoryId = 0;
        $this->name = "";
        $this->description = "";
        $this->image = "";
        $this->subcategories = array();
        $this->isActive = 1;
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["category_id"];
            $this->parentCategoryId = $resultSet["parent_category_id"];
            $this->name = $resultSet["name"];
            $this->description = $resultSet["description"];
            $this->image = $resultSet["image"];
            $this->isActive = $resultSet["is_active"];
        }
    }
}