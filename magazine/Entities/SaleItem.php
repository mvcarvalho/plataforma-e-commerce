<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:53
 */

class SaleItem {

    public $id;
    public $saleId;
    public $productId;
    public $price;
    public $quantity;
    public $optionsText;
    public $options;
    public $product;

    public function __construct(){
        $this->id = 0;
        $this->saleId = 0;
        $this->productId = 0;
        $this->price = 0.0;
        $this->quantity = 0;
        $this->optionsText = "";
        $this->options = array();
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["sale_item_id"];
            $this->saleId = $resultSet["sale_id"];
            $this->productId = $resultSet["product_id"];
            $this->price = $resultSet["price"];
            $this->quantity = $resultSet["quantity"];
            $this->optionsText = $resultSet["options"];
        }
    }
} 