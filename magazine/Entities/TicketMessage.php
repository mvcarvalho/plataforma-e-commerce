<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 30/12/14
 * Time: 11:04
 */

class TicketMessage {

    public $id;
    public $ticketId;
    public $isFromUser;
    public $message;
    public $date;

    public function __construct(){
        $this->id = 0;
        $this->ticketId = 0;
        $this->isFromUser = false;
        $this->message = "";
        $this->date = time();
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['ticket_message_id'];
            $this->ticketId = $resultSet['ticket_id'];
            $this->isFromUser = $resultSet['is_from_user'];
            $this->message = $resultSet['message'];
            $this->date = date('Y-m-d', $resultSet['date']);
        }catch (Exception $e){}
    }
}