<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 30/12/14
 * Time: 10:57
 */

class Ticket {

    public $id;
    public $date;
    public $status;
    public $subject;
    public $saleId;
    public $responsibleId;
    public $userId;
    public $messages;

    public function __construct(){
        $this->id = 0;
        $this->date = time();
        $this->status = 0;
        $this->subject = "";
        $this->saleId = 0;
        $this->responsibleId = 0;
        $this->userId = 0;
        $this->messages = array();
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['ticket_id'];
            $this->date = date('Y-m-d', $resultSet['date']);
            $this->status = $resultSet['status'];
            $this->subject = $resultSet['subject'];
            $this->saleId = $resultSet['sale_id'];
            $this->responsibleId = $resultSet['responsible_id'];
            $this->userId = $resultSet['user_id'];
        }catch (Exception $e){}
    }
} 