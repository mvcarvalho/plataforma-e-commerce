<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 02/12/14
 * Time: 21:44
 */

class Feature {

    public $id;
    public $productId;
    public $name;
    public $options;
    public $optionsText;

    public function __construct(){
        $this->id = 0;
        $this->productId = 0;
        $this->name = "";
        $this->options = array();
        $this->optionsText = "";
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['product_feature_id'];
            $this->productId = $resultSet['product_id'];
            $this->name = $resultSet['feature_name'];
        }catch (Exception $e){

        }
    }
} 