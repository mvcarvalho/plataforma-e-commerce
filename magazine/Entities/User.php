<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:05
 */

class User {

    public $id;
    public $name;
    public $cpf;
    public $email;
    public $phone;
    public $password;
    public $address;
    public $city;
    public $state;
    public $cep;
    public $creation;

    public function __construct(){
        $this->id = 0;
        $this->address = "";
        $this->cep = "";
        $this->cpf = "";
        $this->city = "";
        $this->email = "";
        $this->name = "";
        $this->password = "";
        $this->phone = "";
        $this->state = "";
        $this->creation = time();
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["user_id"];
            $this->address = $resultSet["address"];
            $this->cep = $resultSet["cep"];
            $this->cpf = $resultSet["cpf"];
            $this->city = $resultSet["city"];
            $this->email = $resultSet["email"];
            $this->name = $resultSet["name"];
            $this->password = $resultSet["password"];
            $this->phone = $resultSet["phone"];
            $this->state = $resultSet["state"];
            $this->creation = $resultSet["user_creation"];
        }
    }
} 