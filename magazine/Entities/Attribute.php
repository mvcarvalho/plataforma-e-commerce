<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 25/09/14
 * Time: 22:05
 */

class Attribute {

    public $id;
    public $name;
    public $observation;
    public $isRequired;
    public $options;
    public $quantity; //Utilizado nos produtos para diferenciar estoques

    public function __construct(){
        $this->id = 0;
        $this->name = "";
        $this->observation = "";
        $this->isRequired = false;
        $this->options = array();
        $this->quantity = 0;
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["attribute_id"];
            $this->name = $resultSet["name"];
            $this->observation = $resultSet["observation"];
            $this->isRequired = $resultSet["is_required"];
        }
    }
} 