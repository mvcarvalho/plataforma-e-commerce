<?php
/**
 * Created by PhpStorm.
 * User: mateu_000
 * Date: 21/01/15
 * Time: 22:22
 */

class SystemUser {

    public $id;
    public $login;
    public $password;
    public $name;
    public $isActive;

    public function __construct(){
        $this->id = 0;
        $this->login = "";
        $this->password = "";
        $this->name = "";
        $this->isActive = 0;
    }

    public function setFromResultSet($resultSet){
        if($resultSet != null){
            $this->id = $resultSet["system_user_id"];
            $this->login = $resultSet["login"];
            $this->password = $resultSet["password"];
            $this->name = $resultSet["name"];
            $this->isActive = $resultSet["is_active"];
        }
    }
} 