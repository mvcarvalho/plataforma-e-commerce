<?php
/**
 * Created by PhpStorm.
 * User: Mateus
 * Date: 11/12/14
 * Time: 20:31
 */

class Banner {

    public $id;
    public $image;
    public $url;
    public $type;
    public $order;

    public function __construct(){
        $this->id = 0;
        $this->image = "";
        $this->url = "";
        $this->type = "";
        $this->order = 0;
    }

    public function setFromResultSet($resultSet){
        try{
            $this->id = $resultSet['banner_id'];
            $this->image = $resultSet['banner_image'];
            $this->url = $resultSet['banner_url'];
            $this->type = $resultSet['banner_type'];
            $this->order = $resultSet['banner_order'];
        }catch (Exception $e){}
    }
} 