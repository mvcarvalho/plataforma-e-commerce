<?php
/**
 * Created by PhpStorm.
 * User: rubens
 * Date: 21/01/15
 * Time: 23:18
 */
/** Representa a tabela newsletter_has_categories */


class Interest {

    public $newsletter_id;
    public $category_id;

    public function __construct($newsletter_id = 0, $category_id = 0){
        $this->newsletter_id = $newsletter_id;
        $this->category_id = $category_id;
    }
    
    public function setFromResultSet($resultSet){
        if($resultSet != null) {
            $this->newsletter_id = $resultSet['newsletter_id'];
            $this->category_id = $resultSet['category_id'];
            consoleLog($this);
        }
    }

} 