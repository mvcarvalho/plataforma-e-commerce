-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 08-Abr-2015 às 05:10
-- Versão do servidor: 5.6.14
-- versão do PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `magazine`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `attributes`
--

CREATE TABLE IF NOT EXISTS `attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `observation` varchar(128) NOT NULL,
  `is_required` tinyint(4) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `attributes_options`
--

CREATE TABLE IF NOT EXISTS `attributes_options` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `price_change` double NOT NULL,
  `observation` text NOT NULL,
  PRIMARY KEY (`option_id`),
  KEY `attributeid` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_image` varchar(256) NOT NULL,
  `banner_url` varchar(256) NOT NULL,
  `banner_type` varchar(32) NOT NULL,
  `banner_order` int(11) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `creation_date` int(11) NOT NULL,
  `cupom` varchar(128) NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `userid` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `carts_items`
--

CREATE TABLE IF NOT EXISTS `carts_items` (
  `cart_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `options` varchar(2048) NOT NULL,
  PRIMARY KEY (`cart_item_id`),
  KEY `cartid` (`cart_id`),
  KEY `productid` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `carts_items_options`
--

CREATE TABLE IF NOT EXISTS `carts_items_options` (
  `cart_item_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_item_id` int(11) NOT NULL,
  `attribute_option_id` int(11) NOT NULL,
  `price_change` double NOT NULL,
  PRIMARY KEY (`cart_item_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(1024) NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_key` varchar(128) NOT NULL,
  `coupon_off` double NOT NULL,
  `coupon_off_type` int(11) NOT NULL,
  `coupon_min_value` double NOT NULL,
  `coupon_validity` int(11) NOT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `coupon_key` (`coupon_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `sexo` char(1) DEFAULT NULL,
  PRIMARY KEY (`newsletter_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletter_has_categories`
--

CREATE TABLE IF NOT EXISTS `newsletter_has_categories` (
  `newsletter_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  KEY `fk_newsletter_has_categories_categories1_idx` (`category_id`),
  KEY `fk_newsletter_has_categories_newsletter_idx` (`newsletter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `payment_notifications`
--

CREATE TABLE IF NOT EXISTS `payment_notifications` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_name` varchar(128) NOT NULL,
  `gateway_id` varchar(128) NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `notification_date` int(11) NOT NULL,
  `value` double NOT NULL,
  `status` varchar(128) NOT NULL,
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `gateway_id` (`gateway_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `description` longtext NOT NULL,
  `rules` longtext NOT NULL,
  `normal_price` double NOT NULL,
  `offer_price` double NOT NULL,
  `validity` datetime NOT NULL,
  `sales` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `image` varchar(1024) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `delivery_days` varchar(32) NOT NULL,
  `delivery_cost` double NOT NULL DEFAULT '0',
  `short_description` varchar(512) NOT NULL,
  `promotional_text` varchar(500) DEFAULT NULL,
  `technical_data` longtext,
  `items_included` longtext,
  PRIMARY KEY (`product_id`),
  KEY `FK_SUBCATEGORY` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=205 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products_attributes`
--

CREATE TABLE IF NOT EXISTS `products_attributes` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  KEY `productid` (`product_id`),
  KEY `attributeid` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products_features`
--

CREATE TABLE IF NOT EXISTS `products_features` (
  `product_feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `feature_name` varchar(32) NOT NULL,
  PRIMARY KEY (`product_feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products_features_options`
--

CREATE TABLE IF NOT EXISTS `products_features_options` (
  `feature_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_feature_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`feature_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products_images`
--

CREATE TABLE IF NOT EXISTS `products_images` (
  `product_id` int(11) NOT NULL,
  `image` varchar(1024) NOT NULL,
  KEY `productid` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products_quick_infos`
--

CREATE TABLE IF NOT EXISTS `products_quick_infos` (
  `quick_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`quick_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `value` double NOT NULL,
  `status` int(11) NOT NULL,
  `external_token` varchar(256) NOT NULL DEFAULT '',
  `tracking_code` varchar(128) NOT NULL DEFAULT '-',
  `payment_request_string` text NOT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `userid` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sales_items`
--

CREATE TABLE IF NOT EXISTS `sales_items` (
  `sale_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `options` varchar(2048) NOT NULL,
  PRIMARY KEY (`sale_item_id`),
  KEY `saleid` (`sale_id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sales_items_options`
--

CREATE TABLE IF NOT EXISTS `sales_items_options` (
  `sale_item_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_item_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `price_change` double NOT NULL,
  PRIMARY KEY (`sale_item_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `system_users`
--

CREATE TABLE IF NOT EXISTS `system_users` (
  `system_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`system_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `subject` varchar(256) NOT NULL,
  `sale_id` varchar(64) NOT NULL,
  `responsible_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tickets_messages`
--

CREATE TABLE IF NOT EXISTS `tickets_messages` (
  `ticket_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_from_user` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`ticket_message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `cpf` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(14) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `address` varchar(128) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `user_creation` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


INSERT INTO `system_users` (`system_user_id`, `login`, `password`, `name`, `is_active`) VALUES
(1, 'adminMagazine', '0f359740bd1cda994f8b55330c86d845', 'Adminstrador', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;